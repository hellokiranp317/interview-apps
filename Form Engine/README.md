# ProSe eForm Filing - Forms Engine Application

# About this project
ProSe eForm filing - Forms Engine Application
   
# Environment Setup
Generate the Client Code from API Definition
### Download OpenAPI CLI Jar

- The v5.0.0 jar can be downloaded from here: [openapi-generator-cli](https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.0.0/openapi-generator-cli-5.0.0.jar). 



### Set npm config variable
#### For Linux
- Create a variable named `NPM_CONFIG_OPENAPI_GENERATOR_JAR` and it's value as the path of the openapi-generator-cli jar that was downloaded in the previous step.
``` bash
export NPM_CONFIG_OPENAPI_GENERATOR_JAR=/tmp/openapi-generator-cli-5.0.0.jar
```
- Create a variable named `NPM_CONFIG_SWAGGER_DEFINITION` and it's value as the path of the swagger definition file
``` bash
export NPM_CONFIG_SWAGGER_DEFINITION=/tmp/interview-api.yaml
```  

#### For Windows
- Refer this [link](https://www.architectryan.com/2018/08/31/how-to-change-environment-variables-on-windows-10) for setting environment variables 



### Check npm config
Run:
```bash
npm config list
```
the output will look something like this
```bash
; cli configs
; cli configs
metrics-registry = "https://registry.npmjs.org/"
scope = ""
user-agent = "npm/6.14.10 node/v14.15.4 darwin x64"

; environment configs
openapi-generator-jar = "/tmp/openapi-generator-cli-5.0.0.jar"
swagger-definition = "/tmp/interview-api.yaml"

; node bin location = /usr/local/bin/node
; cwd = /Users/visnu.ravichandran/Workspace/Projects/utah-prose-filing/Bitbucket/formsengine
; HOME = /Users/visnu.ravichandran
; "npm config ls -l" to show all defaults.
```

### Generate Client
For Linux

```bash
NPM_CONFIG_SCRIPT_SHELL=bash npm run generate OR
npm run generate
```
For Windows

```bash
NPM_CONFIG_SCRIPT_SHELL=bash npm run generate:windows OR
npm run generate:windows
```
OR Direct way: 
``` 
java -jar <openapi-jar-location> generate -i <openapi-yaml-location> -l typescript-angular -o <location-to-generate-client> --additional-properties <angular-version>,<service-suffix>

Eg:
java -jar /tmp/openapi-generator-cli-5.0.0.jar generate -i /tmp/api.yaml -l typescript-angular -o src/app/client/ --additional-properties ngVersion=11,serviceSuffix=ApiService
```

# To run Angular...
## Development server

Run `npm run proxy` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Added service URL in proxy-config.json, this should avoid CORS issue

## Testing

Run `npm run test` to test all unit tests with karma and jasmine

> Unit Testing Documentation can be found [here](src/test/unit-testing.md)

## Building the project

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

