import { environment } from 'src/environments/environment';

import { Configuration } from "src/app/client";

// Mock Open API Configuration
const mockAPIConfiguration = (): Configuration => new Configuration({
    basePath: environment.apiUrl,
    credentials: {
      Bearer: 'somerandomtoken'
    }
});

export { mockAPIConfiguration };