# Unit Testing Documentation

> This documentation contains some prerequisites for testing a component/service, how to mock objects/functions and some testing scenarios for components and services

- Frameworks Used 
    - `Karma` as test runner
    - `Jasmine` for writing unit test cases

# Prerequisites

- Each component/service acts as a separate module while testing it, so we need to add dependency modules required by that component/service in `TestBed.configureTestingModule()`, which should be placed inside `beforeEach()`

- If a component has be tested, then it should be declared inside `declarations` array in `TestBed.configureTestingModule()`
    
```javascript 
beforeEach(async () => {
    await TestBed.configureTestingModule({
        declarations: [ SampleComponent ],
    })
    .compileComponents();
});
```

- If a component uses another component which is inside a different module, then that respective module should be added inside `imports` array in `TestBed.configureTestingModule()`

    > Eg: `HeaderComponent` is used inside `InterviewDashboardComponent`, but it is present in `SharedModule`, in that case `SharedModule` should be added to `imports` array while testing `InterviewDashboardComponent`
    
```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [ SharedModule ],
    })
    .compileComponents();
});
```

- Since, we are using OpenAPI Codegeneration Module, we should include `ApiModule` inside `imports` array in `TestBed.configureTestingModule()`
```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [ ApiModule ],
    })
    .compileComponents();
});
```

- We should mock the API Configuration while running the test, so we can provide a `mockAPIConfiguration` for the Configuration provider
    - `mockAPIConfiguration` can be imported from `src/test/test.utils.ts`
   
```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
        providers: [ 
            {
                provide: Configuration,
                useFactory: mockAPIConfiguration,
                multi: false,
            }
        ]
    })
    .compileComponents();
});
```

- If `HttpClient` is used inside a component/service, then we should include `HttpClientTestingModule` inside `imports` array in `TestBed.configureTestingModule()`
    
```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [ HttpClientTestingModule ],
    })
    .compileComponents();
});
```

- If `Router` is used inside a component/service, then we should include `RouterTestingModule` inside `imports` array in `TestBed.configureTestingModule()`
    
```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
        imports: [ RouterTestingModule ],
    })
    .compileComponents();
});
```

# Mocking

## HttpClient

- If a service uses `HttpClient` to make API requests, we should mock and test it using `HttpTestingController`
    - We should create a variable for `HttpTestingController` and inject it into `TestBed`
    - We should run `httpMock.verify()` in `afterEach()` to make sure that there are no outstanding requests
    - We can expect a requestUrl by running `httpMock.expectOne(requestUrl)`
    - We can expect the request method by accessing `req.request.method`, where `req` is a return value of `httpMock.expectOne(requestUrl)`
    - We can provide dummy values as responses using `flush`

```javascript
describe('SampleService', () => {
    let service: SampleService;
    let httpMock: HttpTestingController;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule ],
        });
        service = TestBed.inject(SampleService);
        httpMock = TestBed.inject(HttpTestingController);
    });
    afterEach(() => {
        httpMock.verify();
    });
    it('testing a function that makes a API call using HttpClient', () => {
        const requestUrl: string = API_ROUTES.AUTHENTICATION;
        service.functionUsesHttpClient().subscribe(response => {
            expect(response.length).toBe(2);
            expect(response).toEqual(dummyResponse);
        });
        const req = httpMock.expectOne(requestUrl);
        expect(req.request.method).toBe("GET");
        req.flush(mockResponse);
    });
});
```

> To learn more about mocking HttpClient, click [here](https://medium.com/netscape/testing-with-the-angular-httpclient-api-648203820712)

## Router

- If a service/component uses `Router` to navigate to different routes, we can mock and test it using the following way
    - We should create a variable for `Router` and inject it into `TestBed`
    - We can spyOn `navigate` function in `Router` and expect it

```javascript
describe('SampleService', () => {
    let service: SampleService;
    let router:  Router;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ RouterTestingModule ],
        });

        service = TestBed.inject(SampleService);
        router = TestBed.inject(Router);
    });
    it('testing a function that navigates to a different route using Router', () => {
        const navigateSpy = spyOn(router, 'navigate');
        expect(navigateSpy).toHaveBeenCalled();
        expect(navigateSpy).toHaveBeenCalledWith(['/someRoute']);
    });
});
```

## ActivatedRoute

- If a component uses `ActivatedRoute` to fetch params, we can mock it using the following way

```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [YourComponent],
    imports: [],
    providers: [
        {
            provide: ActivatedRoute, 
            useValue: {
                params: Observable.of({ id: 'test' })
            }
        }
    ]
    })
    .compileComponents();
});
```


## CustomService

- If we want to mock and test a custom service `CustomService` in a component, we can do that by providing a mockValue to `CustomService` provider using `useValue`

```javascript
beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [YourComponent],
    providers: [
        {
            provide: CustomService,
            useValue: {
                someFunction: () => of(mockResponse) 
            }
        }
    ]
    })
    .compileComponents();
});
```

# Testing
> Examples of some scenarios can be found [here](https://github.com/juristr/angular-testing-recipes)

## Component testing scenarios

- If any `@Input` property is used in a component
- If any `@Output` property is used in a component
- Variables whose value will be assigned in a asynchronous way
- If the component contains forms, then form validations need to be tested
- Functions that make a service call
- Logical functions
   
> Detailed Guide for testing components can be found [here](https://angular.io/guide/testing-components-basics)

## Service testing scenarios

- All functions that make an API request to an endpoint
- If any observable is present, test whether the value is set property

> Detailed Guide for testing services can be found [here](https://angular.io/guide/testing-services)


