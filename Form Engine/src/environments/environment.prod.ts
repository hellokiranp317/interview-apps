export const environment = {
  production: true,
  apiUrl: "/InterviewAPI",
  myCaseUrl: "/MyCaseWEB",
  odrUrl: "/OnlineDisputeResolutionWEB",
  adobeSignHostUrl: "https://secure.na3.echosign.com",
};
