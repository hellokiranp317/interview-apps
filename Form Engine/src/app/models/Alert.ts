export interface Alert {
    type: string;
    message: string;
    dismissable?: boolean;
    toBeDismissedIn?: number;
} 