export interface AuthenticateResponse{ 
    success?: boolean,
    accessToken?: string,
    refreshToken?: string,
    data?: {
        redirectUrl?: string
        userFullName?: string
    }
}