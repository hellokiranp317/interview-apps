// Angular Modules
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtHelperService, JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';

// Application Modules
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

// Components
import { AppComponent } from './app.component';
import {
  AemFormComponent,
  AssignTagsModalComponent,
  AuditHistoryTableComponent,
  BuilderComponent,
  CreateGuidedInterviewModalComponent,
  DataTableComponent,
  FormioBuilderRendererComponent,
  FormWizardStepperComponent,
  GeneralFormComponent,
  LoginFormComponent,
  PublishFormComponent,
  PublishInterviewModalComponent,
  SharedFormModalComponent,
  TemplateInfoModalComponent,
  UploadFormComponent
} from './components';

// Pages
import {
  FormsComponent, FormsDashboardComponent, FormsEngineDashboardComponent, LoginComponent
} from './pages';

// Client Services
import { ApiModule, Configuration } from './client';

// Utils
import { JwtInterceptor } from './core/helpers';
import { getAPIConfiguration } from './core/utils';


@NgModule({
  declarations: [
    AppComponent,
    FormsDashboardComponent,
    FormsEngineDashboardComponent,
    DataTableComponent,
    FormsComponent,
    BuilderComponent,
    AuditHistoryTableComponent,
    AssignTagsModalComponent,
    SharedFormModalComponent,
    FormWizardStepperComponent,
    LoginComponent,
    LoginFormComponent,
    UploadFormComponent,
    GeneralFormComponent,
    AemFormComponent,
    PublishFormComponent,
    PublishInterviewModalComponent,
    FormioBuilderRendererComponent,
    TemplateInfoModalComponent,
    CreateGuidedInterviewModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ApiModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(), 
    NgSelectModule,
    JwtModule,
    FontAwesomeModule
  ],
  providers: [
    {
      provide: Configuration,
      useFactory: getAPIConfiguration,
      multi: false,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
      JwtHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
