import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';

import { ApiModule, Configuration, FormEngineForm } from 'src/app/client';
import { SharedModule } from 'src/app/shared/shared.module';
import { mockAPIConfiguration } from 'src/test/test.utils';
import mockFormsResponseData from 'src/test/data/formsResponse.json';
import { DataTableComponent } from './data-table.component';
import { DATA_TABLE, FORM_ENGINE, FRONT_END_ROUTES } from 'src/app/core/constants';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;
  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ApiModule, SharedModule, HttpClientTestingModule, RouterTestingModule , NgxDatatableModule ],
      declarations: [ DataTableComponent ],
      providers: [ 
        {
          provide: Configuration,
          useFactory: mockAPIConfiguration,
          multi: false,
        }
      ]
    })
    TestBed.configureTestingModule ( {
      providers: [
        { provide: Router, useValue: mockRouter},
      ]
    } )
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    component.formsData = mockFormsResponseData.data;
    component.filterData = {
      formName: null, 
      caseType: null, 
      roles: null, 
      status: null, 
      statusDate: null, 
    };
    component.dropDownFilterData = {
      status : new Set('New')
    };
    component.columnsType = FORM_ENGINE.TABLE_TYPES.CASE_TYPE_FORMS;
    component.sortOrder = [DATA_TABLE.SORT.DATE_ASC];
    component.rows = mockFormsResponseData.data;
    component.dataParams = DATA_TABLE.DATA_PARAMS;
    component.selected = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('initializeDataTable and setColumn method is called on ngOnInit', () => {
      spyOn(component, 'initializeDataTable').and.callThrough();
      component.ngOnInit();
      expect(component.initializeDataTable).toHaveBeenCalled();
      spyOn(component, 'setColumn').and.callThrough();
      component.ngOnInit();
      expect(component.setColumn).toHaveBeenCalled();
    });
  });

  describe('ngAfterViewInit', () => {
    it('setColumn method is called on ngAfterViewInit', () => {
      spyOn(component, 'setColumn').and.callThrough();
      component.ngAfterViewInit();
      expect(component.setColumn).toHaveBeenCalled();
    });
  });

  describe('setColumn Method', () => {
    it('setColumn method is called on ngAfterViewInit', () => {
      spyOn(component, 'setColumn').and.callThrough();
      component.ngAfterViewInit();
      expect(component.setColumn).toHaveBeenCalled();
      expect(component.columns.length).toBe(6);
      component.columnsType = FORM_ENGINE.TABLE_TYPES.AUDIT_HISTORY;
      component.ngAfterViewInit();
      expect(component.columns.length).toBe(3);
    });
  });


  describe('onLimitChange Method', () => {
    it('sets the data', () => { 
      component.onLimitChange('50');
      expect(component.dataParams.pageSize).toEqual(50);
    });
  });

  describe('onSelect Method', () => {
    it('onSelect method is called with ', () => {
      const selectedForm =   {
        formId: 34944,
        caseType: "Debt Collection",
        tag: "PLAINTIFF",
        roles: 2133,
        status: "New",
        statusDate: "2021-06-25",
        formName: "Ex Parte Motion for Hearing to Identify Judgment Debtor's Property"
      }
      component.onSelect({selected: [selectedForm]});
      expect(component.selectedCount).toBe(1);
    });
  });

  describe('clearFilter clears filter, sort and selection', () => {
    it('clears filter, sort and selection', () => {
      component.clearFilter();
      expect(component.selected.length).toEqual(0);
      const filter = {formName: null, caseType: null, roles: null, status: null, statusDate: null};
      expect(component.columnFilterValues).toEqual(filter);
    });
  });

  describe('setPage method', () => {
    it('setPage sets offset', () => {
      component.setPage({offset: 10});
      expect(component.dataParams.pageNum).toEqual(10);
    });
  });

  describe('redirectToFormsPage method', () => {
    it('Redirect to forms page', () => {
      const currentRow: FormEngineForm = {
        formId: 34944,
        caseTypes: ["Debt Collection"],
        roles: ["PLAINTIFF"],
        formTemplateId: "2133",
        statusDate: "2021-06-25",
        formName: "Ex Parte Motion for Hearing to Identify Judgment Debtor's Property",
        status: 'IN PROGRESS'
      }
      component.redirectToFormsPage(currentRow);
      expect(mockRouter.navigate).toHaveBeenCalledWith([FRONT_END_ROUTES.FORMS, currentRow.formId]);
    });
  });
});
