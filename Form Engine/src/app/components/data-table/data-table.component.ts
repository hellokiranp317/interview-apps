import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { 
  faFileAlt as farFileAlt, 
  faFileEdit as farFileEdit, 
  faFilePdf as farFilePdf, 
  faFile as farFile } from '@fortawesome/pro-regular-svg-icons';
import { 
  faFileAlt as fasFileAlt, 
  faFileEdit as fasFileEdit, 
  faFilePdf as fasFilePdf, 
  faFile as fasFile } from '@fortawesome/pro-solid-svg-icons';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { FormEngineForm, FormEngineFormAllStatus, FormEngineFormStatus } from 'src/app/client';
import { DATA_TABLE, FORM_ENGINE, TABLE } from 'src/app/core/constants';
import { Columns, DropdownFilterdata, FilterData, PageLimit, SortOrder } from 'src/app/core/models';
import { TableModalEmitter } from 'src/app/core/models/TableModalEmitter';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() formsData: FormEngineForm[];
  @Input() filterData: FilterData;
  @Input() dropDownFilterData: DropdownFilterdata;
  @Input() sortOrder: SortOrder[];
  @Input() columnsType: string;

  @Output() openModalEmitter: EventEmitter<TableModalEmitter> = new EventEmitter();
  @Output() redirectToNextStepEmitter: EventEmitter<any> = new EventEmitter();
  @Output() redirectToEditInterviewEmitter: EventEmitter<any> = new EventEmitter();

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('checkboxTemplate') checkboxTemplate: TemplateRef<any>;
  @ViewChild('headerTemplate') headerTemplate: TemplateRef<any>;
  @ViewChild('headerDropdownTemplate') headerDropdownTemplate: TemplateRef<any>;
  @ViewChild('clearHeaderTemplate') clearHeaderTemplate: TemplateRef<any>;
  @ViewChild('tableOptionsTemplate') tableOptionsTemplate: TemplateRef<any>;
  @ViewChild('dateTemplate') dateTemplate: TemplateRef<any>;
  @ViewChild('clickableBodyTemplate') clickableBodyTemplate: TemplateRef<any>;
  @ViewChild('formTextBodyTemplate') formTextBodyTemplate: TemplateRef<any>;
  @ViewChild('tagTemplate') tagTemplate: TemplateRef<any>;
  @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
  @ViewChild('dateBodyTemplate') dateBodyTemplate: TemplateRef<any>;
  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('statusTemplateWithTypeTemplate') statusTemplateWithTypeTemplate: TemplateRef<any>;
  @ViewChild('statusTemplateWithTypeDropdownTemplate') statusTemplateWithTypeDropdownTemplate: TemplateRef<any>;

  @ViewChildren('filter') filter: { _results: any[]; };

  readonly TABLE = TABLE;
  readonly MODAL_TYPES = FORM_ENGINE.MODAL_TYPES;
  readonly TABLE_TYPES = FORM_ENGINE.TABLE_TYPES;
  readonly STATUS_ICONS = FORM_ENGINE.AUDIT_HISTORY_MODAL.STATUS_ICONS;
  readonly SHARED_FORM = FORM_ENGINE.SHARED_FORM;
  readonly FORM_STATUS = FormEngineFormStatus;
  readonly FORM_ENGINE_STATUS = FORM_ENGINE.STATUS;
  readonly FORMS_TABLE = FORM_ENGINE.FORMS_TABLE;
  readonly PUBLISH_STATUS_TYPES = FORM_ENGINE.PUBLISH_STATUS_TYPES;
  readonly FONTAWESOME_ICONS = {
    PENDING: {
      BASIC_PDF: farFilePdf,
      FILLABLE_FORM: farFileEdit,
      CONTENT_FORM: farFile,
      GUIDED_INTERVIEW: farFileAlt,
    }, 
    PUBLISHED: {
      BASIC_PDF: fasFilePdf,
      FILLABLE_FORM: fasFileEdit,
      CONTENT_FORM: fasFile,
      GUIDED_INTERVIEW: fasFileAlt,
    }
  }
  readonly FORMS_STATUS_WITH_TYPE = FORM_ENGINE.FORMS_STATUS_WITH_TYPE;
  readonly FORMS_STATUS_WITH_TYPE_KEYS = FORM_ENGINE.FORMS_STATUS_WITH_TYPE_KEYS;

  defaultSortOrder: SortOrder[];
  tempRows: FormEngineForm[];
  rows: any[];
  totalCount: number = 0;
  dataParams: { pageNum: number; pageSize: number; } = DATA_TABLE.DATA_PARAMS;
  SelectionType: typeof SelectionType = SelectionType;
  selected: FormEngineForm[] = [];
  pageLimitOptions: PageLimit[] = DATA_TABLE.PAGE_LIMIT;
  selectedCount: number;
  columnFilterValues: FilterData;
  columns: Columns[];

  constructor(private router: Router, private element: ElementRef) { }

  ngOnInit(): void {
    this.initializeDataTable()
    this.setColumn();
  }

  ngAfterViewInit() {
    this.setColumn();
  }

  /**
   * Method to initialize data table
   */
  initializeDataTable() {
    if (this.formsData) {
      this.rows = this.formsData;
      this.totalCount = this.rows.length;
      this.tempRows = [...this.rows];
    }
    this.dataParams.pageNum = 1;
    this.dataParams.pageSize = TABLE.PAGE_SIZE;
    this.defaultSortOrder = this.sortOrder;
    this.columnFilterValues = this.filterData;
  }

  /**
   * function for pushing the selected rows to array
   * @param selected -selected row 
   * @return void
   * @memberof DatatableComponent
   */
  onSelect({ selected }): void {
    this.selected?.splice(0, this.selected.length);
    if (selected) this.selected?.push(...selected);
    this.selectedCount = this.selected.length;
  }

  /**
   * function to handle no.of rows per page in the table
   * @param {number} limit 
   * @return void
   * @memberof DatatableComponent
   */
  onLimitChange(limit: string): void {
    this.dataParams.pageSize = parseInt(limit);
  }

  /**
   * function to clear filter, sort and selection
   * @return void
   * @memberof DatatableComponent
   */
  clearFilter(): void {
    this.rows = this.tempRows;
    this.columnFilterValues = this.filterData;
    this.filter._results.forEach(element => {
      element.nativeElement.value = '';
    });
    this.selected.length = 0;
    this.defaultSortOrder = this.sortOrder;
  }


  /**
   * Update the table based on the filters
   * @param event 
   * @param {string} prop 
   * @return void
   * @memberof DatatableComponent
   */
  updateFilter(event: any, prop: string): void {
    const val = event.target.value.toLowerCase();
    this.columnFilterValues[prop] = val;
    this.rows = this.tempRows;
    this.applyFilterToDataTable();
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /**
   * Applies the filtered data to the Data Table
   *
   * @memberof DatatableComponent
   */
  applyFilterToDataTable(): void {
    if (this.columnFilterValues) {
      let columnFilterValues = Object.entries(this.columnFilterValues);
      for (let [field, value] of columnFilterValues) {
        const filteredData = this.rows.filter((row) => {
          if (row[field] instanceof Array) {
            const matches = row[field].map(e => e.toLocaleLowerCase()).filter(element => {
              if (element.indexOf(value) !== -1) {
                return true;
              }
            });
            return matches.length > 0;
          }
          else {
            if (value && field === FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.STATUS && ((this.columnsType === this.TABLE_TYPES.CASE_TYPE_FORMS) || (this.columnsType === this.TABLE_TYPES.ALL_FORMS))) {
              const key = this.FORMS_STATUS_WITH_TYPE[value].key;
              const keyValue = this.FORMS_STATUS_WITH_TYPE[value].value;
              return (value && !row.statusOfTypes) ? false : ((row.statusOfTypes.hasOwnProperty(key) && row.statusOfTypes[key] === keyValue) || !value);
            }
            return (value && !row[field]) ? false : (row[field]?.toLowerCase().indexOf(value) !== -1 || !value);
          }
        });
        this.rows = filteredData;
      }
    }
  }

  /**
   * function for handling pagination
   * @param pageInfo 
   * @return void
   * @memberof DatatableComponent
   */
  setPage(pageInfo: { offset: number }): void {
    this.dataParams.pageNum = pageInfo.offset;
  }

  /**
   * Clears out the date filter
   *
   * @memberof DocumentsTableComponent
   */
  clearDateFilter(prop: string) {
    const dateFilter = this.element.nativeElement.querySelector('.' + prop);
    dateFilter.value = '';
    this.updateFilter({ target: { value: '' } }, prop);
  }

  /**
   * funtion to set the columns of the datatable 
   * @memberof DatatableComponent
   * @returns void
   */
  setColumn(): void {
    switch (this.columnsType) {
      case this.TABLE_TYPES.CASE_TYPE_FORMS:
        this.columns = [
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.FORM_NAME, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.FORM_NAME, headerTemplate: this.headerTemplate, cellTemplate: this.formTextBodyTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.DOC_NAME, cellClass: 'd-flex align-items-center', width: 250 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.TAG, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.TAG, headerTemplate: this.headerTemplate, cellTemplate: this.tagTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.TAGS, cellClass: 'd-flex align-items-center' },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.STATUS, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.STATUS, headerTemplate: this.statusTemplateWithTypeDropdownTemplate, cellTemplate: this.statusTemplateWithTypeTemplate, dropdown: this.dropDownFilterData.status, placeholder: this.FORMS_TABLE.PLACEHOLDER.STATUS, cellClass: 'd-flex align-items-center', width: 70 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.EFFECTIVE_DATE.FILLABLE, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.EFFECTIVE_DATE.FILLABLE, headerTemplate: this.dateTemplate, cellTemplate: this.dateBodyTemplate, cellClass: 'd-flex align-items-center', width: 100 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.EFFECTIVE_DATE.GI, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.EFFECTIVE_DATE.GI, headerTemplate: this.dateTemplate, cellTemplate: this.dateBodyTemplate, cellClass: 'd-flex align-items-center', width: 100 },
          { prop: '', cellTemplate: this.tableOptionsTemplate, cellClass: 'd-flex align-items-center', sortable: false, canAutoResize: false, resizeable: false, width: 80 },
        ];
        break;
      case this.TABLE_TYPES.IN_PROGRESS_FORMS:
        this.columns = [
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.FORM_NAME, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.FORM_NAME, headerTemplate: this.headerTemplate, cellTemplate: this.clickableBodyTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.DOC_NAME, cellClass: 'd-flex align-items-center', width: 250 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.STATUS, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.STATUS, headerTemplate: this.headerDropdownTemplate, dropdown: this.dropDownFilterData.status, placeholder: this.FORMS_TABLE.PLACEHOLDER.STATUS, cellClass: 'd-flex align-items-center', width: 70 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.STATUS_DATE, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.STATUS_DATE, headerTemplate: this.dateTemplate, cellTemplate: this.dateBodyTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.DATE, cellClass: 'd-flex align-items-center', width: 100 },
        ];
        break;
      case this.TABLE_TYPES.AUDIT_HISTORY:
        this.columns = [
          { prop: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.PROP.STATUS, name: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.NAME.STATUS, cellTemplate: this.statusTemplateWithTypeTemplate, sortable: false },
          { prop: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.PROP.USER, name: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.NAME.USER, sortable: false },
          { prop: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.PROP.DATE, name: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.NAME.DATE, sortable: false, cellTemplate: this.dateBodyTemplate },
          { prop: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.PROP.COMMENTS, name: FORM_ENGINE.AUDIT_HISTORY_COLUMNS.NAME.COMMENTS, sortable: false },
        ];
        break;
      case this.TABLE_TYPES.ALL_FORMS:
        this.columns = [
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.FORM_NAME, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.FORM_NAME, headerTemplate: this.headerTemplate, cellTemplate: this.formTextBodyTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.DOC_NAME, cellClass: 'd-flex align-items-center', width: 250 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.RULES, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.RULES, headerTemplate: this.headerTemplate, cellTemplate: this.tagTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.RULES, cellClass: 'd-flex align-items-center', width: 85 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.CASE_TYPES, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.CASE_TYPES, headerTemplate: this.headerTemplate, cellTemplate: this.tagTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.CASE_TYPES, cellClass: 'd-flex align-items-center', width: 70 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.TAG, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.TAG, headerTemplate: this.headerTemplate, cellTemplate: this.tagTemplate, placeholder: this.FORMS_TABLE.PLACEHOLDER.TAGS, cellClass: 'd-flex align-items-center', width: 75 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.STATUS, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.STATUS, headerTemplate: this.statusTemplateWithTypeDropdownTemplate, cellTemplate: this.statusTemplateWithTypeTemplate, dropdown: this.dropDownFilterData.status, placeholder: this.FORMS_TABLE.PLACEHOLDER.STATUS, cellClass: 'd-flex align-items-center', width: 100 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.EFFECTIVE_DATE.FILLABLE, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.EFFECTIVE_DATE.FILLABLE, headerTemplate: this.dateTemplate, cellTemplate: this.dateBodyTemplate, cellClass: 'd-flex align-items-center', width: 75 },
          { prop: FORM_ENGINE.CASE_TYPE_COLUMNS.PROP.EFFECTIVE_DATE.GI, name: FORM_ENGINE.CASE_TYPE_COLUMNS.NAME.EFFECTIVE_DATE.GI, headerTemplate: this.dateTemplate, cellTemplate: this.dateBodyTemplate, cellClass: 'd-flex align-items-center', width: 75 },
          { prop: '', cellTemplate: this.tableOptionsTemplate, cellClass: 'd-flex align-items-center', sortable: false, canAutoResize: false, resizeable: false, width: 80 },
        ];
        break;
    }
  }

  /**
   * Redirects to the forms page or opens assign tags modal with the corresponding document ID
   *
   * @param {FormEngineForm} currentRowData
   * @memberof DataTableComponent
   */
  redirectToNextStep(currentRowData: FormEngineForm, status = ''): void {
    if (status === this.PUBLISH_STATUS_TYPES.GUIDED_INTERVIEW) {
      this.redirectToEditInterviewEmitter.emit(currentRowData)
    }
    else this.redirectToNextStepEmitter.emit(currentRowData);
  }


  /**
   * Funtion to set fontawesone icon based on the form type and form status
   * @param fileType 
   * @param status 
   * @returns string
   */
   getStatusIcon(fileType: string, status: FormEngineFormAllStatus): string{
    let statusIconName = '';
    let iconStatus = (status!== FormEngineFormAllStatus.Pending) ? FormEngineFormAllStatus.Published : FormEngineFormAllStatus.Pending;
    switch(fileType){
      case this.PUBLISH_STATUS_TYPES.BASIC_PDF: 
        statusIconName = this.FONTAWESOME_ICONS[iconStatus].BASIC_PDF;
        break;
      case this.PUBLISH_STATUS_TYPES.CONTENT_FORM:
        statusIconName = this.FONTAWESOME_ICONS[iconStatus].CONTENT_FORM;
        break;
      case this.PUBLISH_STATUS_TYPES.FILLABLE_FORM: 
        statusIconName = this.FONTAWESOME_ICONS[iconStatus].FILLABLE_FORM;
        break;
      case this.PUBLISH_STATUS_TYPES.GUIDED_INTERVIEW:
        statusIconName = this.FONTAWESOME_ICONS[iconStatus].GUIDED_INTERVIEW;
    }
    return statusIconName;
  }

  /**
   * function to set styles for icons based on the status
   * @param status 
   * @returns string
   */
  getIconClassBasedOnStatus(status: FormEngineFormAllStatus): string{
    let className: string = '';
    switch(status){
      case FormEngineFormAllStatus.InProgress: 
          className = 'icon-inProgress'; break;
      case FormEngineFormAllStatus.Pending: 
          className = 'icon-pending'; break;
      case FormEngineFormAllStatus.Published:
          className = 'icon-published';
    }
    return className;
  }


  /**
   * Method to open the particular modal type
   * @param modalType 
   * @param formId 
   */
  openModal(modalType: string, formData: FormEngineForm): void {
    this.openModalEmitter.emit({ modalType, formData });
  }
}
