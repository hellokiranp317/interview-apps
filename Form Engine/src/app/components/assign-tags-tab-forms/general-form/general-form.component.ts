import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormEngineFormTagsPayload, FormEngineGeneralFormTags } from 'src/app/client';
import { FORM_ENGINE } from 'src/app/core/constants';
import { NotificationService } from 'src/app/core/services';

@Component({
  selector: 'app-general-form',
  templateUrl: './general-form.component.html',
  styleUrls: ['./general-form.component.scss']
})
export class GeneralFormComponent implements OnInit {

  readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;

  assignTagsModalOptions = {
    backdrop: 'static',
    keyboard: false
  };

  generalTabForm: FormGroup;
  showInPlaceLoader: boolean = false;

  @Input() generalTagsFormData: FormEngineFormTagsPayload = null;
  @Input() isTagsSaved: boolean = false;

  _selectedGeneralTags: FormEngineGeneralFormTags;
  get selectedGeneralTags(): FormEngineGeneralFormTags {
      return this._selectedGeneralTags;
  }
  @Input() set selectedGeneralTags(value: FormEngineGeneralFormTags) {
      this._selectedGeneralTags = value;
      if(value && this.isTagsSaved) {
        this.generalTabForm.get('selectedCaseTypes').setValue(value.caseTypes);
        this.generalTabForm.get('selectedRoles').setValue(value.roles);
        this.generalTabForm.get('selectedSteps').setValue(value.steps);
        this.generalTabForm.get('selectedRules').setValue(value.rules);
        this.generalTabForm.get('selectedStatutes').setValue(value.statutes);
      }
  }

  @Output() onGeneralFormGroupChange: EventEmitter<FormGroup> = new EventEmitter();
  @Input() isSubmitted: boolean = false;

  constructor(private fb: FormBuilder) { 
      this.generalTabForm = this.fb.group({
        selectedCaseTypes: [[], Validators.required],
        selectedRoles: [[], Validators.required],
        selectedSteps: [[]],
        selectedRules: [[]],
        selectedStatutes: [[]],
      });
    }

  ngOnInit(): void {
    this.onGeneralFormGroupChange.emit(this.generalTabForm);
    this.generalTabForm.valueChanges.subscribe(() => this.onGeneralFormGroupChange.emit(this.generalTabForm));
  }

}
