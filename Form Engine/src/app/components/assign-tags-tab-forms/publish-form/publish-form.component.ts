import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormEnginePublishFormTags } from 'src/app/client';
import { FORM_ENGINE } from 'src/app/core/constants';
import { formatDate } from 'src/app/core/utils';
import { multiCheckboxRequiredValidator } from 'src/app/shared/validators/multi-checkbox-required.validator';

@Component({
  selector: 'app-publish-form',
  templateUrl: './publish-form.component.html',
  styleUrls: ['./publish-form.component.scss']
})
export class PublishFormComponent implements OnInit {

  readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;

  publishTabForm: FormGroup;
  showPageLoader: boolean = false;

  @Output() onPublishFormGroupChange: EventEmitter<FormGroup> = new EventEmitter();
  @Input() isSubmitted: boolean = false;

  @Input() isTagsSaved: boolean = false;
  @Input() showEffectiveDate: boolean = false;

  @Input() set basicPDFDisabled(value: boolean ) { 
    value && this.publishTabForm.get('basicPdf').disable();
  }
  @Input() set fillOutDisabled(value: boolean ) { 
    value && this.publishTabForm.get('fillableForm').disable();
  }

  _selectedPublishFormTags: FormEnginePublishFormTags;
  get selectedPublishFormTags(): FormEnginePublishFormTags {
    return this._selectedPublishFormTags;
  }
  @Input() set selectedPublishFormTags(value: FormEnginePublishFormTags) {
      this._selectedPublishFormTags = value;
      if(value && this.isTagsSaved) {
        this.publishTabForm.get('basicPdf').setValue(value.basicPdf);
        this.publishTabForm.get('fillableForm').setValue(value.fillableForm);
        this.publishTabForm.get('comments').setValue(value.comments);
        value.effectiveDate ? this.publishTabForm.get('effectiveDate').setValue(value.effectiveDate) : this.publishTabForm.get('effectiveDate').setValue(this.getDate());
      }
  }

  presentDate = new Date();

  constructor(private fb: FormBuilder) { 
      this.publishTabForm = this.fb.group({
        basicPdf: [{ value: false, disabled: false }],
        fillableForm: [{ value: false, disabled: false }],
        comments: [''],
        effectiveDate: [this.getDate(), Validators.required]
      });
      this.publishTabForm.setValidators(multiCheckboxRequiredValidator());
    }

  getDate() {
    return formatDate(this.presentDate)
  }

  ngOnInit(): void {
    this.onPublishFormGroupChange.emit(this.publishTabForm)
    this.publishTabForm.valueChanges.subscribe(() => this.onPublishFormGroupChange.emit(this.publishTabForm));
  }
}
