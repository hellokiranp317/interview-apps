import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AemFormComponent } from './aem-form.component';

describe('AemFormComponent', () => {
  let component: AemFormComponent;
  let fixture: ComponentFixture<AemFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AemFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
