import { Component, EventEmitter, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { AEMAssociatedUrls, FormEngineAEMFormTags } from 'src/app/client';
import { FORM_ENGINE } from 'src/app/core/constants';

@Component({
  selector: 'app-aem-form',
  templateUrl: './aem-form.component.html',
  styleUrls: ['./aem-form.component.scss']
})
export class AemFormComponent implements OnInit{
  readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;

  @ViewChildren('urlRow') rows;
  @Output() onAemFormGroupChange: EventEmitter<FormGroup> = new EventEmitter();

  @Input() isTagsSaved: boolean = false;

  _selectedAEMTags: FormEngineAEMFormTags;
  get selectedAEMTags(): FormEngineAEMFormTags {
      return this._selectedAEMTags;
  }
  @Input() set selectedAEMTags(value: FormEngineAEMFormTags) {
      this._selectedAEMTags = value;
      if(this.isTagsSaved && this._selectedAEMTags.associatedUrls && this._selectedAEMTags.associatedUrls.length > 0) {
        this.removeAssociatedUrl(0)
        this._selectedAEMTags.associatedUrls.forEach((tags: AEMAssociatedUrls)=> {
          this.associatedURLs().push(this.newAssociatedUrl(tags.typeUrl, tags.typeLabel));
        })
      }
  }

  aemForm: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.aemForm = this.fb.group({
      associatedURLs: this.fb.array([this.fb.group({
        typeUrl: [''],
        typeLabel: [''],
      })]),
    });
  }

  ngOnInit(): void {
    this.onAemFormGroupChange.emit(this.aemForm)
    this.aemForm.valueChanges.subscribe(() => this.onAemFormGroupChange.emit(this.aemForm));
  }

  /****** START : Methods to dynamically add and remove form controls ******/ 

  associatedURLs() : FormArray {
    return this.aemForm.get("associatedURLs") as FormArray;
  }
   
  newAssociatedUrl(url = '', label = ''): FormGroup {
    return this.fb.group({
      typeUrl: [url],
      typeLabel: [label],
    })
  }
   
  addAssociatedUrl() {
    this.associatedURLs().push(this.newAssociatedUrl());
    setTimeout(() => {
      this.rows.last.nativeElement.children[0].focus();
    }, 0);
  }
   
  removeAssociatedUrl(i:number) {
    this.associatedURLs().removeAt(i);
  }

  /****** END : Methods to dynamically add and remove form controls ******/ 
}
