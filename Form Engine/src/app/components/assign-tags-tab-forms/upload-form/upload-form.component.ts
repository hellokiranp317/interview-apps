import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faXmark } from '@fortawesome/pro-solid-svg-icons';

import { FormEngineFormTag } from 'src/app/client';
import { COMMON, FORM_ENGINE } from 'src/app/core/constants';
import { requiredFileType } from 'src/app/shared/validators/required-file-type.validator';

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.scss']
})
export class UploadFormComponent implements OnInit {

  readonly UPLOAD_NEW_FORM_MODAL = FORM_ENGINE.UPLOAD_NEW_FORM_MODAL;
  readonly FONTAWESOME_ICONS = {
    XMARK: faXmark
  };

  @ViewChild('uploadNewFormRef') uploadNewFormRef: ElementRef<HTMLInputElement>;
  @ViewChild('uploadFieldRef') uploadFieldRef: ElementRef<HTMLInputElement>;
  @Output() uploadFormGroupChange: EventEmitter<FormGroup> = new EventEmitter();
  @Input() uploadFormErrors: any;
  @Input() isTagsEdited: boolean = false; // Variable to check if pre-populate data is present
  @Input() isFormUploadRequired: boolean = true;
  @Input() isSubmitted: boolean = false;
  @Input() hasFormUpload: boolean = true;

  _existingFormData: FormEngineFormTag;
  get existingFormData(): FormEngineFormTag {
    return this._existingFormData;
  }
  @Input() set existingFormData(value: FormEngineFormTag) {
    this._existingFormData = value;
    if (value && this.isTagsEdited) {
      this.uploadFormTab.get('uploadFormName').setValue(value.formName);
      this.uploadFormTab.get('uploadFormNumber').setValue(value.formNumber);
      this.uploadFormTab.get('description').setValue(value.description);
    }
  }

  uploadFileValidation = {
    formNameError: "",
    formNumberError: "",
    fileError: ""
  }
  canUploadForm: boolean = false;
  isUploadForm: boolean = true;
  showPageLoader: boolean = false;

  uploadFormTab: FormGroup;

  constructor(private fb: FormBuilder) {
    this.uploadFormTab = this.fb.group({
      uploadFormName: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      uploadFormNumber: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[a-zA-Z0-9_.]*$')]],
      uploadedFile: ['', []],
      description: ['', Validators.maxLength(250)]
    });
  }

  ngOnInit(): void {
    if (!this.isFormUploadRequired || this.isTagsEdited) {
      this.uploadFormTab.controls['uploadedFile'].setValidators([requiredFileType(COMMON.FILE_TYPES.PDF)]);
    }
    else {
      this.uploadFormTab.controls['uploadedFile'].setValidators([Validators.required, requiredFileType(COMMON.FILE_TYPES.PDF)]);
    }
    this.uploadFormGroupChange.emit(this.uploadFormTab);
    this.uploadFormTab.valueChanges.subscribe(() => {
      this.uploadFormGroupChange.emit(this.uploadFormTab);
    });
  }

  /**
   * Method for input type file to open file browser
   */
  openFileBrowserForUpload() {
    this.uploadNewFormRef.nativeElement.click();
  }

  /**
  * Method to update the file name in the input field 
  */
  updateUploadFileField(event) {
    const file = event.target.files[0];
    if (file) {
      if (this.uploadFormErrors) {
        this.uploadFormErrors.pdfFile = false;
      };
      this.uploadFormTab.controls.uploadedFile.setValue(file);
      this.uploadFieldRef.nativeElement.value = this.uploadNewFormRef?.nativeElement?.files[0]?.name;
    }
  }

  clearFileUpload() {
    this.uploadFormTab.controls['uploadedFile'].setValue('');
    this.uploadFieldRef.nativeElement.value = "";
  }
}
