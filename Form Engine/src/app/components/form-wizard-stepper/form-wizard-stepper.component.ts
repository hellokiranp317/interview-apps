import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-form-wizard-stepper',
  templateUrl: './form-wizard-stepper.component.html',
  styleUrls: ['./form-wizard-stepper.component.scss']
})
export class FormWizardStepperComponent implements OnInit {

  @Input('components') components: Array<any>;
  @Input('currentPage') currentPage: number;
  @Input('formWizardPageErrors') formWizardPageErrors: Array<string>;
  @Input('stepperType') stepperType: string = "horizontal";
  @Input('seenPages') seenPages: Array<number>;

  @Output() pageClicked: EventEmitter<{pageNum: number, isClickable: boolean}> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Method to set page number programmatically
   * @param pageNum 
   * @param isClickable 
   */
  setPage(pageNum: number, isClickable: boolean): void {
    this.pageClicked.emit({pageNum, isClickable})
  }
}
