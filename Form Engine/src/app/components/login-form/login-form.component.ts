import { AfterViewInit, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BUTTONS, ERROR, FRONT_END_ROUTES, LOADER, LOGIN } from 'src/app/core/constants';
import { LoginService } from 'src/app/core/services';
import { SharedService } from 'src/app/shared';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() showErrorAlert: EventEmitter<string> = new EventEmitter();
  @ViewChild('usernameInput') usernameInput: ElementRef;
  @ViewChild('usernameLabel') usernameLabel: ElementRef;
  @ViewChild('passwordLabel') passwordLabel: ElementRef;
  @ViewChild('errorMessageRef') errorMessageRef: ElementRef;

  readonly LOGIN = LOGIN;
  readonly LOADER = LOADER;
  readonly BUTTONS = BUTTONS;

  loginForm: FormGroup;
  submitted: boolean;
  showError: boolean = false;
  loginSubscription$: Subscription;
  showLoader: boolean;
  errorMessage: string = '';
  
  constructor(private renderer: Renderer2, private loginService: LoginService, private sharedService: SharedService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
    this.submitted = false;
  }

  ngAfterViewInit() {
    this.usernameInput.nativeElement.focus();
  }

  /**
   * Getter for fetching form controls
   * @readonly
   * @memberof LoginFormComponent
   */
  get formControls() { return this.loginForm.controls; }

  /**
   * Authenticates the user
   * @return {*}
   * @memberof LoginFormComponent
   */
  loginUser(): void {
    this.submitted = true;
    this.showError = false;
    this.showLoader = true;
    if (this.loginForm.invalid) {
      this.showLoader = false;
      return;
    }
    else if(this.loginForm.valid && (!this.formControls.username.value.trim() || !this.formControls.password.value.trim())) {
      this.showLoader = false;
      this.showErrorMessage();
      return;
    }
    else{
      this.loginSubscription$ = this.loginService.login(this.loginForm.value)
      .subscribe(
        response => {
          const role = response?.data?.role;
          if(role){
            this.sharedService.redirectBasedOnRole(role);
          }
          else{
            this.sharedService.redirectRoute(FRONT_END_ROUTES.LOGIN, { state: { message: ERROR.MESSAGE.UN_AUTHORIZED } });
          }
          this.showLoader = false;
        },
        error => {
          this.showLoader = false;

          switch(error?.error?.error?.code){
            case LOGIN.INVALID_CREDENTIALS : {
              this.showErrorMessage();
              break;
            }
            case LOGIN.USER_ROLE_NULL : {
              this.showErrorAlert.next(ERROR.MESSAGE.UN_AUTHORIZED);
              break;
            }
            default: 
              this.showErrorAlert.next(error?.error?.error?.message)
          }
        }
      );
    }
  }
  
  /**
   * Displays form error message
   * Error message is toggled in order to make the screen reader read it whenever the login fails
   * @return void
   * @memberof LoginFormComponent
   */
  showErrorMessage(): void{
    this.renderer.removeAttribute(this.errorMessageRef?.nativeElement,'role');
    this.errorMessage = LOGIN.COMMON_ERROR_MESSAGE;
    this.renderer.setAttribute(this.errorMessageRef?.nativeElement,'role','alert');
    this.showError = true;
  }

  /**
   * Add 'animate-label' class to label on focus of form field 
   * @param label
   * @returns void
   */
  onFocus(label : ElementRef): void{
    this.renderer.addClass(label,'label-animate');
  }

  /**
   * Remove 'animate-label' class to label on blur of form field 
   * @param field
   * @param label
   * @returns void
   */
  onBlur(field: string,label : ElementRef): void{
    if(this.formControls[field].value) return;
    this.renderer.removeClass(label,'label-animate');
  }

  ngOnDestroy(): void {
    this.loginSubscription$ ? this.loginSubscription$.unsubscribe() : null;
  }
}
