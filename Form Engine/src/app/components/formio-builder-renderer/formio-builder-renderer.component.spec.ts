import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormioBuilderRendererComponent } from './formio-builder-renderer.component';

describe('FormioBuilderRendererComponent', () => {
  let component: FormioBuilderRendererComponent;
  let fixture: ComponentFixture<FormioBuilderRendererComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormioBuilderRendererComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormioBuilderRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
