import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormioComponent } from 'angular-formio';
import { ALERT_TYPES, FORM_ENGINE, GUIDED_INTERVIEW } from 'src/app/core/constants';

@Component({
  selector: 'app-formio-builder-renderer',
  templateUrl: './formio-builder-renderer.component.html',
  styleUrls: ['./formio-builder-renderer.component.scss']
})
export class FormioBuilderRendererComponent implements OnInit {

  readonly ALERT_TYPES = ALERT_TYPES;
  readonly FORM_ENGINE = FORM_ENGINE;
  readonly GUIDED_INTERVIEW = GUIDED_INTERVIEW;

  @Input() transformedBuilderOptions: any;
  @Input() builderForm: any;
  @Input() formSrc: any;
  @Input() builderActionsTemplate: TemplateRef<any>;
  @Input() isFormIOWizard: boolean = true;
  @Input() wizardBtnGroup: any;

  @ViewChild('formEl') formEl: FormioComponent;

  formIOOptions: any;
  formWizardPageErrors: Array<string> = [];
  currentPage: number = 0;
  isLastPageSubmitted: boolean = false;
  seenPages: Array<number> = [];

  constructor() { }

  ngOnInit(): void {
    this.setFormIOOptions();
  }

   /************ START: FORM IO WIZARD CONTROL METHODS ************/

  /**
   * Sets the Form IO Options
   *
   * @memberof GuidedInterviewComponent
   */
   setFormIOOptions(): void {
    this.formIOOptions = {
      submitMessage: '',
      disableAlerts: true,
      noAlerts: true,
      saveDraft: true,
      noDefaultSubmitButton: true,
    }
  };

  
  /**
   * Method that gets called when the form is changed
   */
  onFormChange() {
    this.formWizardPageErrors = [];
    this.formEl?.formio?.pages?.forEach(element => {
      this.formWizardPageErrors.push(element.errors)
    });
    if(this.currentPage !== this.formEl?.formio?.pages?.length - 1) {
      this.isLastPageSubmitted = false;
    }
  }

  /**
   * Method that gets called when the errors in form are added or resolved
   */
  onFormErrorChange() {
    this.formWizardPageErrors = [];
    this.formEl?.formio?.pages?.forEach(element => {
      this.formWizardPageErrors.push(element.errors)
    });
    const isLastPage = this.currentPage === this.formEl?.formio?.pages?.length - 1;
    if(isLastPage) {
      this.isLastPageSubmitted = true;
      const firstErrorPage = this.formWizardPageErrors.findIndex((errorPage) => errorPage.length > 0);
      if(firstErrorPage !== this.currentPage) {
        this.formEl?.formio?.setPage(firstErrorPage)
        this.currentPage = firstErrorPage;
      }
    }
  }
  
  /**
   * Method to update current page number
   */
  updateCurrentPage(): void {
    this.seenPages = this.formEl?.formio?.root?._seenPages;
    this.currentPage = this.formEl?.formio?.page;
  }

  /**
   * Method to set page number programmatically
   * @param pageNum 
   * @param isClickable 
   */
  setPage($event): void {
    this.seenPages = this.formEl?.formio?.root?._seenPages;
    if($event.isClickable)
      this.formEl?.formio?.setPage($event.pageNum)
    this.currentPage = $event.pageNum
  }

  /**
   * Handle Cancel of Wizard form
   */
  cancelWizard() {
    this.formEl?.formio?.cancel()
  }

  /**
   * Method to go to previous page
   */
  goToPreviousWizardPage() {
    this.formEl?.formio?.prevPage()
  }

  /**
   * Method to go to next page
   */
  goToNextWizardPage() {
    this.formEl?.formio?.nextPage()
  }

  /**
   * Method to submit the form
   */
  submitWizard() {
    this.formEl?.formio?.submit()
  }

  /************ END: FORM IO WIZARD CONTROL METHODS ************/

}
