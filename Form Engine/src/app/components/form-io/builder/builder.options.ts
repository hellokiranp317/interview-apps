import { FORMIO_CONSTANTS } from "src/app/core/constants";


const fieldProperties = [
  {
    key: 'display',
    label: 'Basic',
    components: [
      {
        key: 'labelMargin',
        ignore: true,
      },
      {
        key: 'tooltip',
        ignore: true,
      },
      {
        key: 'labelWidth',
        ignore: true,
      },
      {
        key: 'widget.type',
        ignore: true,
      },
      {
        key: 'inputMask',
        ignore: true,
      },
      {
        key: 'allowMultipleMasks',
        ignore: true,
      },
      {
        key: 'prefix',
        ignore: true,
      },
      {
        key: 'suffix',
        ignore: true,
      },
      {
        key: 'tabindex',
        ignore: true,
      },
      {
        key: 'persistent',
        ignore: true,
      },
      {
        key: 'multiple',
        ignore: true,
      },
      {
        key: 'clearOnHide',
        ignore: true,
      },
      {
        key: 'protected',
        ignore: true,
      },
      {
        key: 'hidden',
        ignore: true,
      },
      {
        key: 'mask',
        ignore: true,
      },
      {
        key: 'autofocus',
        ignore: true,
      },
      {
        key: 'tableView',
        ignore: true,
      },
      {
        key: 'alwaysEnabled',
        ignore: true,
      },
      {
        key: 'showWordCount',
        ignore: true,
      },
      {
        key: 'showCharCount',
        ignore: true,
      },
      {
        key: 'autocomplete',
        ignore: true,
      },
      {
        key: 'hideLabel',
        ignore: true,
      },
      {
        key: 'spellcheck',
        ignore: true,
      },
      {
        key: 'modalEdit',
        ignore: true,
      },
    ],
  },
  {
    key: 'layout',
    ignore: true,
  },
  {
    key: 'logic',
    ignore: true,
  },
];

const wizardFieldProperties = [
  { 
    key: 'customTab',
    label: "Display",
    components: [
        {
          input: true,
          key: "title",
          label: "Title",
          placeholder: "Panel Title",
          tooltip: "The title text that appears in the header of this panel.",
          type: "textfield",
          validate: { required: true }
        },
        {
          input: true,
          key: "customClass",
          label: "Custom CSS Class",
          placeholder: "Custom CSS Class",
          tooltip: "Custom CSS class to add to this component.",
          type: "textfield",
        },
        {
          label: "Wizard Custom Properties",
          tableView: false,
          defaultValue: {
            isSubmitPage: false,
            hasPreviewEditOption: true,
            showStepperTitle: true
          },
          tooltip: FORMIO_CONSTANTS.BUILDER.WIZARD_CUSTOM_PROPERTIES,
          values: [
            {
              label: "Show Stepper Title",
              value: "showStepperTitle",
              shortcut: ""
            },
            {
              label: "Is Submit Page",
              value: "isSubmitPage",
              shortcut: "",
            },
            {
              label: "Has Preview Edit Option",
              value: "hasPreviewEditOption",
              shortcut: ""
            },
            {
              label: "Form with Payment Option",
              value: "isPaymentForm",
              shortcut: ""
            },
            {
              label: "Is Payment Page",
              value: "isPaymentPage",
              shortcut: ""
            }
          ],
          validate: {
            onlyAvailableItems: false
          },
          key: "wizardCustomProperties",
          type: "selectboxes",
          input: true,
          inputType: "checkbox"
        },
        {
          label: "Previous Button Text",
          tableView: true,
          defaultValue: "Previous",
          key: "previousTextField",
          type: "textfield",
          input: true,
          tooltip: FORMIO_CONSTANTS.BUILDER.BUTTON_TOOLTIP,
        },
        {
          label: "Next Button Text",
          tableView: true,
          defaultValue: "Next",
          key: "nextTextField",
          type: "textfield",
          input: true,
          tooltip: FORMIO_CONSTANTS.BUILDER.BUTTON_TOOLTIP,
        },
        {
          label: "Submit Button Text",
          tableView: true,
          defaultValue: "Submit",
          key: "submitTextField",
          type: "textfield",
          input: true,
          tooltip: FORMIO_CONSTANTS.BUILDER.BUTTON_TOOLTIP,
        },
        {
          label: "Cancel Button Text",
          tableView: true,
          defaultValue: "Cancel",
          key: "cancelTextField",
          type: "textfield",
          input: true,
          tooltip: FORMIO_CONSTANTS.BUILDER.BUTTON_TOOLTIP,
        },
        {
          label: "Cancel Redirection",
          defaultValue: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION_OPTIONS.DASHBOARD.VALUE,
          data: {
            values: [
              {
                label: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION_OPTIONS.DASHBOARD.LABEL,
                value: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION_OPTIONS.DASHBOARD.VALUE
              },
              {
                label: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION_OPTIONS.MYCASE.LABEL,
                value: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION_OPTIONS.MYCASE.VALUE
              }
            ]
          },
          key: "cancelRedirection",
          type: "select",
          input: true,
          tableView: true,
          tooltip: FORMIO_CONSTANTS.BUILDER.CANCEL_REDIRECTION,
        },
    ],
    weight: 0,
    default: true
  },
  {
    key: 'display',
    ignore: true,
  },
  {
    key: 'layout',
    ignore: true,
  },
  {
    key: 'logic',
    ignore: true,
  },
]

const editForm = {
  textfield: fieldProperties,
  textarea: fieldProperties,
  checkbox: fieldProperties,
  radio: fieldProperties,
  select: fieldProperties,
  panel: wizardFieldProperties,
}

const defaultOptions: any = {
  basic: {
    default: false
  },
  data: false,
  premium: false,
}

const builderOptionsWithFormTemplate: any = {
  builder: {
    ...defaultOptions,
    formTemplateFields: {
      title: 'Form Template Fields',
      default: true,
      weight: -1,
      components: {},
    },
    template: {
      title: 'Templates',
      weight: -1,
      components: {},
    }
  },
  editForm,
};

const defaultBuilderOptions: any = {
  builder: { ...defaultOptions, basic: { default: true } } ,
  editForm,
  noDefaultSubmitButton: true
};

export { builderOptionsWithFormTemplate, defaultBuilderOptions, fieldProperties };