import { Component, Input, OnInit } from '@angular/core';
import { GUIDED_INTERVIEW } from 'src/app/core/constants';
import { FormEngineFormsService } from 'src/app/core/services';

@Component({
  selector: 'app-formio-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})
export class BuilderComponent implements OnInit {

  @Input('builderOptions') builderOptions: any;
  @Input('form') form: any;

  // Initial values of formBuilder
  formBuilder = { 
    type: GUIDED_INTERVIEW.FORM,
    display: GUIDED_INTERVIEW.FORM,
    components: [],
  };

  wizardBuilder = { 
    type: GUIDED_INTERVIEW.FORM,
    display: GUIDED_INTERVIEW.WIZARD,
    components: [],
  };

  constructor(private formEngineFormsService: FormEngineFormsService) { }

  ngOnInit(): void {
  }

  /**
   * Listener for Form Change Event 
   * 
   * @param event 
   */
  onFormChange(event: any): void {
    if(event.form?.display === GUIDED_INTERVIEW.WIZARD) {
      const components = event.form?.components ? event.form?.components[0]?.components : null;
      (components && components.length > 0) ? this.formEngineFormsService.setFormWizard(event.form) : this.formEngineFormsService.setFormWizard(this.wizardBuilder);
    } else if(event.form?.display === GUIDED_INTERVIEW.FORM) {
      const components = event.form?.components ? event.form?.components : null;
      (components && components.length > 0) ? this.formEngineFormsService.setFormWizard(event.form) : this.formEngineFormsService.setFormWizard(this.formBuilder);
    }
  }
  
}
