import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbNav } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import {
  AEMAssociatedUrls, FormEngineAEMFormTags, FormEngineContentFormEditTagsRequest, FormEngineContentFormTagsRequest, FormEngineCreateFormTagsRequest, FormEngineEditFormTagsRequest, FormEngineFormOperation, FormEngineFormStatus, FormEngineFormTag, FormEngineFormTagsPayload, FormEngineFormTagsResponse, FormEngineGeneralFormTags, FormEnginePublishFormTags, FormEngineUploadResponse
} from 'src/app/client';
import { FormEngineUploadPayload } from 'src/app/client/model/formEngineUploadPayload';
import { ERROR, FORM_ENGINE, FRONT_END_ROUTES, LOADER } from 'src/app/core/constants';
import { FormEngineContentInterviewService, FormEngineFormsService, FormEngineUploadFormsService, LoadingService, NotificationService } from 'src/app/core/services';
import { ModalComponent } from 'src/app/shared';
import { UploadFormComponent } from '../../assign-tags-tab-forms/upload-form/upload-form.component';
@Component({
  selector: 'app-assign-tags-modal',
  templateUrl: './assign-tags-modal.component.html',
  styleUrls: ['./assign-tags-modal.component.scss']
})
export class AssignTagsModalComponent implements AfterViewInit, OnDestroy {

  readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;
  readonly UPLOAD_NEW_FORM_MODAL = FORM_ENGINE.UPLOAD_NEW_FORM_MODAL;
  readonly CONTENT_GENERATION_MODAL = FORM_ENGINE.CONTENT_GENERATION_MODAL;
  readonly LOADER = LOADER;
  readonly FORMIO_ROUTE_FORM_TYPES = FORM_ENGINE.FORMIO_ROUTE_FORM_TYPES;

  assignTagsModalOptions = {
    backdrop: 'static',
    keyboard: false
  };

  activeTab: number = 1;
  previousTab: number = 1;

  @Input('currentTab') currentTab: number = 1;
  @Input() isTagsSaved: boolean = false;
  @Input() isTagsEdited: boolean = false;
  @Input() isFormUploadRequired: boolean = true;
  @Input() showPublishTab: boolean = true;
  @Input() modalHeader: string = this.ASSIGN_TAGS_MODAL.ASSIGN_HEADER;
  @Input() assignTagsType: string = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
  @Input() hasFormUpload: boolean = true;

  _formDetails: any;
  get formDetails(): any {
    return this._formDetails;
  }
  @Input() set formDetails(value: any) {
    this._formDetails = value;
    if (value)
      this.getAllFormTags();
  }

  @ViewChild('tagsModal') tagsModalComponent: ModalComponent;
  @ViewChild('nav') navComponent: NgbNav;

  @Output() assignModalClosed: EventEmitter<void | boolean> = new EventEmitter();
  @Output() fetchFormDashboardDetails: EventEmitter<boolean> = new EventEmitter();
  @Output() tagsEditSuccess: EventEmitter<void> = new EventEmitter();

  uploadFormValues: any;
  isUploadFormValid: boolean = false;
  generalFormValues: any;
  isGeneralFormValid: boolean = false;
  aemFormValues: any;
  formDetailsResponse: FormEngineUploadPayload = null;
  isAemFormValid: boolean = true; // As no validation required
  publishFormValues: any;
  isPublishFormValid: boolean = false;

  isTagsUpdatedAfterFetch: boolean = false;
  isUploadFormUpdatedAfterFetch: boolean = false;
  uploadedFile: string = "";

  allTagsFormData: FormEngineFormTagsPayload;

  uploadFormErrors = {
    formName: false,
    formNumber: false,
    pdfFile: false
  }

  isSubmitted: boolean = false;

  canFetchFormDashboardDetails: boolean = false;

  showPublishBtn: boolean = true;

  assignTagsTabLength: number = Object.keys(FORM_ENGINE.ASSIGN_TAGS_MODAL.TABS).length;

  //Upload form modal variables
  @ViewChild('uploadForm') uploadForm: UploadFormComponent;
  isUploadForm: boolean = true;
  basicPDFDisabled: boolean = false;
  fillOutDisabled: boolean = false;

  // Subscriptions
  uploadFormSubscription$: Subscription;
  storeFormTagsSubscription$: Subscription;
  allFormTagsSubscription$: Subscription;
  saveContentFormTagsSubscription$: Subscription;

  constructor(private formEngineFormsService: FormEngineFormsService, 
    private formEngineUploadFormsService: FormEngineUploadFormsService, 
    private formEngineContentInterviewService: FormEngineContentInterviewService, 
    private notificationService: NotificationService, 
    public loader: LoadingService, private router: Router) { }

  ngAfterViewInit(): void {
    // Setting Up the Tabs for Edit Modal
    if(this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM) {
      const { PUBLISH, ...PRIMARY_TABS } = FORM_ENGINE.ASSIGN_TAGS_MODAL.TABS
      this.assignTagsTabLength = Object.keys(PRIMARY_TABS).length;
    }
    this.tagsModalComponent.open();
    setTimeout(() => {
      this.navComponent.select(this.currentTab);
    }, 0);
  }

  /******** START: GET ALL TAGS INFORMATION **********/
  getAllFormTags() {
    const formNumber = (this.formDetailsResponse && this.formDetailsResponse.formNumber) || (this.formDetails && this.formDetails.formNumber);
    this.allFormTagsSubscription$ = this.formEngineFormsService.getAllFormTags(formNumber).subscribe( 
      (response: FormEngineFormTagsResponse) => {
        this.allTagsFormData = response.data;
        if(this.isTagsEdited){
          this.basicPDFDisabled = response?.data?.assignedTags?.publishTags?.basicPdf;
          this.fillOutDisabled = response?.data?.assignedTags?.publishTags?.fillableForm;
        }
      },
      (err: ErrorEvent) => {
        const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.showErrorToast(errResponse, { code: err?.error?.error?.code })
      }
    );
  }
  /******** END: GET ALL TAGS INFORMATION **********/

  async readFileAsDataURL(file) {
    let result_base64 = await new Promise((resolve) => {
      let fileReader = new FileReader();
      fileReader.onload = (e) => resolve(fileReader.result);
      fileReader.readAsDataURL(file);
    });
    let fileData = result_base64.toString().split(',')[1];
    return fileData;
  }

  uploadOrUpdateForms(uploadedFileData: string, formId: number, formOperation = FormEngineFormOperation.UploadForm) {
    this.loader.showLoader();
    this.uploadFormSubscription$ = this.formEngineUploadFormsService.uploadBasedOnFormOperation(uploadedFileData,formId, this.uploadFormValues.uploadFormName, this.uploadFormValues.uploadFormNumber, this.uploadFormValues.description, formOperation).subscribe(
      (response: FormEngineUploadResponse) => {
        this.formDetailsResponse = response.data;
        this.loader.hideLoader();
        if (!this.isTagsEdited) { // Upload Flow
          this.showSuccessToast(`${this.formDetailsResponse.formName} ${this.UPLOAD_NEW_FORM_MODAL.FORM_UPLOAD_SUCCESS}`)
          this.afterUploadForm();
        }
        else { // Edited Upload Flow
          this.moveToNextTab();
        }
        this.canFetchFormDashboardDetails = true;
        this.uploadFormErrors.formName = false;
        this.uploadFormErrors.formNumber = false;
      },
      (err: ErrorEvent) => {
        const errorCode = err?.error?.error?.code;
        const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        if (errorCode === this.ASSIGN_TAGS_MODAL.ERROR_CODE.INVALID_FORM) {
          this.uploadFormErrors.formName = true;
          this.uploadFormErrors.formNumber = true;
        }
        else if (errorCode === this.ASSIGN_TAGS_MODAL.ERROR_CODE.INVALID_FORM_TITLE) {
          this.uploadFormErrors.formName = true;
          this.uploadFormErrors.formNumber = false;
        } else if (errorCode === this.ASSIGN_TAGS_MODAL.ERROR_CODE.INVALID_FORM_NUMBER) {
          this.uploadFormErrors.formName = false;
          this.uploadFormErrors.formNumber = true;
        } else if (errorCode === this.ASSIGN_TAGS_MODAL.ERROR_CODE.INVALID_FORM_TEMPLATE || errorCode === this.ASSIGN_TAGS_MODAL.ERROR_CODE.NO_FORM_FIELDS_FOUND) {
          this.uploadFormErrors.pdfFile = true;
          this.uploadFormErrors.formName = false;
          this.uploadFormErrors.formNumber = false;
        }
        else {
          this.uploadFormErrors.formName = false;
          this.uploadFormErrors.formNumber = false;
          this.uploadFormErrors.pdfFile = false;
          this.showErrorToast(errResponse, errorCode)
        }
        this.loader.hideLoader();
      }
    );
  }

  constructContentFormTagsRequest(status) {
    const formNumber = (this.formDetailsResponse && this.formDetailsResponse.formNumber) || (this.formDetails && this.formDetails.formNumber);
    const generalTabValues: FormEngineGeneralFormTags = this.constructGeneralTagFormValues();
    const aemTabValues: FormEngineAEMFormTags = this.constructAEMTabFormValues();
    const createTagsContentFormPayload: FormEngineContentFormTagsRequest = {
      formNumber,
      status: status,
      generalTags: generalTabValues,
      aemTags: aemTabValues
    };
    return createTagsContentFormPayload;
  }

  createContentForm(action) {
    this.loader.showLoader();
    // Building Request
    const formName = (this.formDetailsResponse && this.formDetailsResponse.formName) || (this.formDetails && this.formDetails.formName);
    const formNumber = (this.formDetailsResponse && this.formDetailsResponse.formNumber) || (this.formDetails && this.formDetails.formNumber);
    const status: FormEngineFormStatus = action === this.ASSIGN_TAGS_MODAL.SAVE ?
      FormEngineFormStatus.New : FormEngineFormStatus.InProgress;
    const createTagsContentFormPayload = this.constructContentFormTagsRequest(status);

    this.saveContentFormTagsSubscription$ = 
      this.formEngineContentInterviewService.saveContentFormTagInformation(createTagsContentFormPayload)
        .subscribe(
          (response) => {
            this.loader.hideLoader();
            if(action === this.ASSIGN_TAGS_MODAL.SAVE) {
              this.tagsModalComponent.close();
              this.assignModalClosed.emit(true);
              this.showSuccessToast(formName + this.ASSIGN_TAGS_MODAL.SAVE_TAGS_SUCCESS);
            } else {
              this.router.navigate([ FRONT_END_ROUTES.FORMS,this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM, formNumber])
            }
          },
          (err) => {
            this.loader.hideLoader();
            const errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
            this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
          }
        )
  }

  constructEditFormTagsRequest() {
    const formNumber = (this.formDetailsResponse && this.formDetailsResponse.formNumber) || (this.formDetails && this.formDetails.formNumber);
    const generalTabValues: FormEngineGeneralFormTags = this.constructGeneralTagFormValues();
    const aemTabValues: FormEngineAEMFormTags = this.constructAEMTabFormValues();
    const createTagsContentFormPayload: FormEngineContentFormEditTagsRequest = {
      formNumber,
      generalTags: generalTabValues,
      aemTags: aemTabValues
    };
    return createTagsContentFormPayload;
  }

  editContentFormTags() {
    this.loader.showLoader();
    // Building Request
    const editTagsContentFormRequest = this.constructEditFormTagsRequest();

    this.saveContentFormTagsSubscription$ = 
      this.formEngineContentInterviewService.editContentFormTagInformation(editTagsContentFormRequest)
        .subscribe(
          (response) => {
            this.loader.hideLoader();
            this.showSuccessToast(this.ASSIGN_TAGS_MODAL.EDIT_TAGS_SUCCESS);
            this.tagsEditSuccess.emit()
          }, 
          (err) => {
            this.loader.hideLoader();
            const errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
            this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
          }
        )
  }

  uploadFormAction(dataURL, formOperation) {
    let formId: number = this.formDetails?.formId || null;
    if(this.isUploadFormUpdatedAfterFetch || !this.isTagsEdited) {
      this.uploadOrUpdateForms(dataURL, formId, formOperation);
    }
    else {
      this.moveToNextTab();
    }
    this.isUploadFormUpdatedAfterFetch = false;
  }

  async uploadNewForm() {
    let formOperation;
    if(this.isTagsEdited) {
    // Edit Operation
      formOperation = (this.isTagsEdited && this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM) 
                        ? FormEngineFormOperation.EditContentForm 
                        : FormEngineFormOperation.EditForm;
    } else {
      // Upload Operation
      formOperation = (!this.isTagsEdited && this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM) 
                        ? FormEngineFormOperation.CreateContentForm 
                        : FormEngineFormOperation.UploadForm;
    }

    
    if (this.uploadFormValues?.uploadedFile) {
      this.readFileAsDataURL(this.uploadFormValues?.uploadedFile).then(dataURL => {
        this.uploadedFile = dataURL;
        this.isUploadFormUpdatedAfterFetch = true;
        this.uploadFormAction(dataURL, formOperation);
      });
    }
    else {
      this.uploadFormAction("", formOperation);
    }
  }

  afterUploadForm() {
    this.isSubmitted = false;
    this.getAllFormTags();
    this.moveToNextTab();
  }


  /******* END: Upload form tab methods *******/

  /******* START: Store Form Tag Methods *******/

  /**
   * Constructs the Form Tab form values in FormEngineFormTag format
   *
   * @return {*}  {FormEngineFormTag}
   * @memberof AssignTagsModalComponent
   */
  constructEditFormValues(): FormEngineFormTag {
    const description = this.uploadFormValues?.description;
    const Form: FormEngineFormTag = {
      pdfFile: this.uploadedFile,
      formName: this.uploadFormValues?.uploadFormName,
      formNumber: this.uploadFormValues?.uploadFormNumber,
      description: description?.trim() === '' ? '' : description
    };
    return Form;
  }

  /**
   * Constructs the General Tab form values in FormEngineGeneralFormTags format
   *
   * @return {*}  {FormEngineGeneralFormTags}
   * @memberof AssignTagsModalComponent
   */
  constructGeneralTagFormValues(): FormEngineGeneralFormTags {
    const generalTabValues: FormEngineGeneralFormTags = {
      caseTypes: this.generalFormValues?.selectedCaseTypes?.length != 0 ? this.generalFormValues?.selectedCaseTypes : [],
      roles: this.generalFormValues?.selectedRoles?.length != 0 ? this.generalFormValues?.selectedRoles : [],
      rules: this.generalFormValues?.selectedRules?.length != 0 ? this.generalFormValues?.selectedRules : [],
      steps: this.generalFormValues?.selectedSteps?.length != 0 ? this.generalFormValues?.selectedSteps : [],
      statutes: this.generalFormValues?.selectedStatutes?.length != 0 ? this.generalFormValues?.selectedStatutes : [],
    };
    return generalTabValues;
  }

  /**
   * Constructs the AEM Tab form values in FormEngineAEMFormTags format
   *
   * @return {*}  {FormEngineAEMFormTags}
   * @memberof AssignTagsModalComponent
   */
  constructAEMTabFormValues(): FormEngineAEMFormTags {
    const associatedURLs: [AEMAssociatedUrls] = this.aemFormValues?.associatedURLs;
    let validAssociateURLs: boolean = false;
    associatedURLs?.forEach((eachAssociatedURL, index) => {
      if (eachAssociatedURL.typeUrl.trim() != '' || eachAssociatedURL.typeLabel.trim() != '') {
        validAssociateURLs = true;
      } else if (eachAssociatedURL.typeUrl.trim() === '' && eachAssociatedURL.typeLabel .trim()=== '') {
        associatedURLs.splice(index, 1);
      }
    });

    const aemTabValues: FormEngineAEMFormTags = {
      associatedUrls: validAssociateURLs ? associatedURLs : [],
    };

    return aemTabValues;
  }

  /**
   * Constructs the Publish Tab form values in FormEnginePublishFormTags format
   *
   * @return {*}  {FormEnginePublishFormTags}
   * @memberof AssignTagsModalComponent
   */
  constructPublishFormValues(): FormEnginePublishFormTags {
    let effectiveDateWithTime = '';
    const comments = this.publishFormValues?.comments;
    if(this.publishFormValues?.effectiveDate)
      effectiveDateWithTime = (this.publishFormValues?.effectiveDate === (new Date()).toISOString().substring(0, 10)) ?
        new Date().toISOString() :
        new Date(this.publishFormValues?.effectiveDate)?.toISOString();
    const publishTabValues: FormEnginePublishFormTags = {
      basicPdf: this.publishFormValues?.basicPdf,
      fillableForm: this.publishFormValues?.fillableForm,
      effectiveDate: this.publishFormValues?.fillableForm || this.publishFormValues?.basicPdf ? effectiveDateWithTime : null,
      comments: comments?.trim() !== '' ? comments : ''
    };
    return publishTabValues;
  }

  /**
   * Constructs the payload in FormEngineCreateFormTagsRequest format for storing the form tags 
   *
   * @param {number} formId
   * @param {FormEngineFormStatus} status
   * @return {*}  {FormEngineCreateFormTagsRequest}
   * @memberof AssignTagsModalComponent
   */
  constructFormEngineCreateFormTagsRequest(formNumber: string, status: FormEngineFormStatus): FormEngineCreateFormTagsRequest {
    const generalTabValues: FormEngineGeneralFormTags = this.constructGeneralTagFormValues();
    const aemTabValues: FormEngineAEMFormTags = this.constructAEMTabFormValues();
    const publishTabValues: FormEnginePublishFormTags = this.constructPublishFormValues();
    const requestPayload: FormEngineCreateFormTagsRequest = {
      formNumber,
      status: status,
      generalTags: generalTabValues,
      aemTags: aemTabValues,
      publishTags: publishTabValues
    }

    return requestPayload;
  }

  saveAsDraft(action) {
    if(this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM) {
      this.createContentForm(action)
    } else {
      this.storeFormTagsInformation(action)
    }
  }

  /**
   * Makes the API call for storing the Form Tags Information
   *
   * @param {string} action
   * @memberof AssignTagsModalComponent
   */
  storeFormTagsInformation(action: string) {
    const formNumber: string = this.formDetailsResponse?.formNumber || this.formDetails?.formNumber;
    const status: FormEngineFormStatus = action === this.ASSIGN_TAGS_MODAL.SAVE ?
      FormEngineFormStatus.InProgress : FormEngineFormStatus.Published;
    this.isSubmitted = status === FormEngineFormStatus.Published ? true : false;

    if (!formNumber || !status) {
      this.showErrorToast(ERROR.MESSAGE.SOMETHING_WENT_WRONG);
    }

    this.loader.showLoader();
    const payload: FormEngineCreateFormTagsRequest =
      this.constructFormEngineCreateFormTagsRequest(formNumber, status);

    this.storeFormTagsSubscription$ = this.formEngineFormsService.storeFormTagsInformation(payload)
      .subscribe(
        (response) => {
          const basicPdfPublished: boolean = payload?.publishTags?.basicPdf;
          const fillableFormPublished: boolean = payload?.publishTags?.fillableForm;
          const formStatus: FormEngineFormStatus = payload?.status;
          this.assignTagsSuccessHandler(basicPdfPublished, fillableFormPublished, formStatus);
        },
        (err) => {
          this.loader.hideLoader();
          const errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
        }
      );
  }

  /**
   * Constructs the payload in constructFormEngineEditFormTagsRequest format for updating the form tags 
   *
   * @param {number} formId
   * @param {FormEngineFormStatus} status
   * @return {*}  {FormEngineEditFormTagsRequest}
   * @memberof AssignTagsModalComponent
   */
  constructFormEngineEditFormTagsRequest(formNumber: string): FormEngineEditFormTagsRequest {
    const generalTabValues: FormEngineGeneralFormTags = this.constructGeneralTagFormValues();
    const aemTabValues: FormEngineAEMFormTags = this.constructAEMTabFormValues();
    const publishTabValues: FormEnginePublishFormTags = this.constructPublishFormValues();
    const requestPayload: FormEngineEditFormTagsRequest = {
      formNumber: formNumber,
      generalTags: generalTabValues,
      aemTags: aemTabValues,
      publishTags: publishTabValues
    }
    return requestPayload;
  }

  editFormTagsInformation() {
    const formNumber: string = this.formDetailsResponse?.formNumber || this.formDetails?.formNumber;
    this.loader.showLoader();
    const payload: FormEngineEditFormTagsRequest = this.constructFormEngineEditFormTagsRequest(formNumber);
    this.storeFormTagsSubscription$ = this.formEngineFormsService.editTagsInformation(payload)
      .subscribe(
        (response) => {
          this.loader.hideLoader();
          this.showSuccessToast(this.ASSIGN_TAGS_MODAL.EDIT_TAGS_SUCCESS);
          this.tagsEditSuccess.emit()
        },
        (err) => {
          this.loader.hideLoader();
          const errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
        }
      );
  }

  /**
   * Success Handler for assign tags
   *
   * @param {boolean} basicPdfPublished
   * @param {boolean} fillableFormPublished
   * @param {boolean} proceedToGuidedInterview
   * @memberof AssignTagsModalComponent
   */
  assignTagsSuccessHandler(basicPdfPublished: boolean, fillableFormPublished: boolean,
    formStatus: FormEngineFormStatus) {
    this.loader.hideLoader();
    if (basicPdfPublished && formStatus === FormEngineFormStatus.Published) {
      this.canFetchFormDashboardDetails = true;
      this.showSuccessToast(FORM_ENGINE.BASIC_PDF_PUBLISH_SUCCESS_MESSAGE);
    }
    if (fillableFormPublished && formStatus === FormEngineFormStatus.Published) {
      this.canFetchFormDashboardDetails = true;
      this.showSuccessToast(FORM_ENGINE.FILLABLE_FORM_PUBLISH_SUCCESS_MESSAGE);
    }
    if (formStatus === FormEngineFormStatus.InProgress || formStatus === FormEngineFormStatus.New) {
      this.canFetchFormDashboardDetails = true;
    }
    this.cancel();
  }

  /******* END: Store Form Tag Methods *******/


  /*********** START: TOAST ACTIONS ***********/
  /**
   * Method gets called by notificationService service on success
   * @param message 
   */
  showSuccessToast(message) {
    this.notificationService.showSuccess(message);
  }

  /**
   * Method gets called by notificationService service on error
   * @param message 
   */
  showErrorToast(message, code?) {
    this.notificationService.showError(message, code);
  }
  /*********** END: TOAST ACTIONS ***********/

  /******* START: Tab methods *******/
  /**
   * Method to move to next tab
   */
  moveToNextTab() {
    this.currentTab++;
    this.navComponent.select(this.currentTab);
  }

  onClickNext() {
    this.isSubmitted = true;
    if (this.currentTab === this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.NAV_ID) {
      this.isUploadFormValid && this.uploadNewForm();
    } else if (this.currentTab === this.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID) {
      if (this.isGeneralFormValid) this.moveToNextTab()
    } else if (this.currentTab === this.ASSIGN_TAGS_MODAL.TABS.AEM.NAV_ID) {
      if (this.isAemFormValid) this.moveToNextTab();
    } else if (this.currentTab === this.ASSIGN_TAGS_MODAL.TABS.PUBLISH.NAV_ID) {
      if (this.isPublishFormValid) this.moveToNextTab();
    }
  }

  /**
   * Method to move to previous tab
   */
  moveToPrevTab() {
    this.currentTab--;
    this.navComponent.select(this.currentTab);
  }

  /**
   * Method to move to respective tabs
   * @param tabId 
   */
  moveToTab(tabId) {
    this.navComponent.select(tabId);
  }

  /**
   * Method to change the button group
   * @param navId 
   */
  onActiveIdChange(navId) {
    if (navId === this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.NAV_ID) {
      this.isUploadForm = true;
    } else {
      this.isUploadForm = false;
    }
  }
  /******* END: Tab methods *******/

  /**
   * Method to receive form values from the child form components
   * @param formData 
   * @param tab 
   */
  onAssignTagsFormChange(formData, tab) {
    if (tab === this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.TEXT) {
      this.uploadFormValues = formData.value;
      this.isUploadFormValid = formData.valid;
    } else if (tab === this.ASSIGN_TAGS_MODAL.TABS.GENERAL.TEXT) {
      this.generalFormValues = formData.value;
      this.isGeneralFormValid = formData.valid;
    } else if (tab === this.ASSIGN_TAGS_MODAL.TABS.AEM.TEXT) {
      this.aemFormValues = formData.value;
      this.isAemFormValid = formData.valid;
    } else if (tab === this.ASSIGN_TAGS_MODAL.TABS.PUBLISH.TEXT) {
      this.publishFormValues = formData.getRawValue();
      this.isPublishFormValid = formData.valid;
      if(!this.isTagsEdited) {
        this.isPublishFormValid = formData.valid && (this.publishFormValues.basicPdf || this.publishFormValues.fillableForm)
      }
    }
    // TODO : Make this section to call only if we land on the last tab
    // On last tab, check if tags are updated
    if (this.isTagsEdited) {
      // Upload Tab
      let uploadFormFetchedData = this.allTagsFormData?.assignedTags?.form;
      uploadFormFetchedData["pdfFile"] = "";
      uploadFormFetchedData = this.sortObjectByKey(uploadFormFetchedData);
      const uploadFormCurrentData = this.sortObjectByKey(this.constructEditFormValues());
      this.isUploadFormUpdatedAfterFetch = (JSON.stringify(uploadFormFetchedData) !== JSON.stringify(uploadFormCurrentData)) || !!this.uploadFormValues.uploadedFile;
      
      // Other Tabs
      const orderedAssignedTags = this.sortObjectByKey(this.allTagsFormData?.assignedTags?.generalTags);
      const orderedGeneralTags = this.sortObjectByKey(this.constructGeneralTagFormValues());
      const isGeneralTagsEdited = JSON.stringify(orderedAssignedTags) !== JSON.stringify(orderedGeneralTags);
      const isAEMTagsEdited = JSON.stringify(this.allTagsFormData.assignedTags.aemTags) !== JSON.stringify(this.constructAEMTabFormValues());
      const isPublishTagsEdited = this.checkIfPublishTabUpdated();
      if(this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM)
        this.isTagsUpdatedAfterFetch = isGeneralTagsEdited || isAEMTagsEdited;
      else 
        this.isTagsUpdatedAfterFetch = isGeneralTagsEdited || isAEMTagsEdited || isPublishTagsEdited;
    }
  }

  checkIfPublishTabUpdated() {
    let publishedData = this.allTagsFormData?.assignedTags?.publishTags;
    let isPublishTagsUpdated = false;
    let constructedPublishTabValues = this.constructPublishFormValues();
    const publishDate = constructedPublishTabValues.effectiveDate;
    constructedPublishTabValues = {...constructedPublishTabValues, effectiveDate: publishDate?.slice(0, publishDate?.indexOf('T'))}
    isPublishTagsUpdated = JSON.stringify(this.sortObjectByKey(publishedData)) !== JSON.stringify(this.sortObjectByKey(constructedPublishTabValues));
    return isPublishTagsUpdated;
  }

  sortObjectByKey(object) {
    return Object.keys(object)?.sort().reduce(
      (obj, key) => {
        obj[key] = Array.isArray(object[key]) ? object[key]?.sort() : object[key];
        return obj;
      }, {}
    );
  }

  /**
   * Method to go back on click of cancel
   */
  cancel() {
    if (this.canFetchFormDashboardDetails) {
      this.fetchFormDashboardDetails.emit(this.canFetchFormDashboardDetails);
    }
    this.canFetchFormDashboardDetails = false;
    this.assignModalClosed.emit();
    this.tagsModalComponent.close();
  }

  ngOnDestroy(): void {
    this.tagsModalComponent?.close();
    this.uploadFormSubscription$ ? this.uploadFormSubscription$.unsubscribe() : null;
    this.storeFormTagsSubscription$ ? this.storeFormTagsSubscription$.unsubscribe() : null;
    this.allFormTagsSubscription$ ? this.allFormTagsSubscription$.unsubscribe() : null;
  }

}
