import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgSelectModule } from '@ng-select/ng-select';

import { AssignTagsModalComponent } from './assign-tags-modal.component';
import { ModalComponent } from 'src/app/shared';

describe('AssignTagsModalComponent', () => {
  let component: AssignTagsModalComponent;
  let fixture: ComponentFixture<AssignTagsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignTagsModalComponent, ModalComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, RouterTestingModule, NgSelectModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTagsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
