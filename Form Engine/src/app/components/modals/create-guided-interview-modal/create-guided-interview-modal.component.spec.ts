import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGuidedInterviewModalComponent } from './create-guided-interview-modal.component';

describe('CreateGuidedInterviewModalComponent', () => {
  let component: CreateGuidedInterviewModalComponent;
  let fixture: ComponentFixture<CreateGuidedInterviewModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateGuidedInterviewModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGuidedInterviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
