import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FORM_ENGINE, LOADER } from 'src/app/core/constants';
import { ModalComponent } from 'src/app/shared';

@Component({
  selector: 'app-create-guided-interview-modal',
  templateUrl: './create-guided-interview-modal.component.html',
  styleUrls: ['./create-guided-interview-modal.component.scss']
})
export class CreateGuidedInterviewModalComponent implements OnInit {

  readonly GUIDED_INTERVIEW_CREATE_MODAL = FORM_ENGINE.GUIDED_INTERVIEW_CREATE_MODAL;
  readonly LOADER = LOADER;

  @ViewChild('createGiFormModal') createGiFormModal: ModalComponent;

  @Output() openCreateGuidedInterviewPage: EventEmitter<FormGroup> = new EventEmitter();
  @Output() onCancel?: EventEmitter<void> = new EventEmitter();
  @Input() showTitleError: boolean = false;

  initialFormValues: any;

  createGiForm: FormGroup;
  isSubmitted: boolean = false;

  constructor(private fb: FormBuilder) { 
    this.createGiForm = this.fb.group({
      formName: ['',Validators.required],
      formDescription: ['']
    });
    this.initialFormValues = this.createGiForm.value;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.openModal();
  }

  closeModal(){
    this.createGiFormModal.close();
    this.createGiForm.reset(this.initialFormValues);
    this.onCancel.emit();
  }

  openModal() {
    this.createGiFormModal.open();
  }

  openCreateGuidedInterview(){
    this.isSubmitted = true;
    if(this.createGiForm.valid){
      this.openCreateGuidedInterviewPage.emit(this.createGiForm);
    }
  }

}
