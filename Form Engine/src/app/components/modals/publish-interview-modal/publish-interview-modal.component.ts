import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faXmark } from '@fortawesome/pro-solid-svg-icons';
import { FormEngineAssociatedContentFormsForGIPayload, FormEngineContentFormMappingTypesForGIPayload, FormEngineContentFormPayload } from 'src/app/client';
import { FORM_ENGINE } from 'src/app/core/constants';
import { formatDate } from 'src/app/core/utils';
import { ModalComponent } from 'src/app/shared';

@Component({
  selector: 'app-publish-interview-modal',
  templateUrl: './publish-interview-modal.component.html',
  styleUrls: ['./publish-interview-modal.component.scss']
})
export class PublishInterviewModalComponent implements OnInit {

  readonly GUIDED_INTERVIEW_PUBLISH_MODAL = FORM_ENGINE.GUIDED_INTERVIEW_PUBLISH_MODAL;
  readonly FONTAWESOME_ICONS = {
    XMARK: faXmark
  };

  @ViewChild('publishInterviewModal') publishInterviewModal: ModalComponent;

  @Input() formName: string;
  @Input() contentFormsList: FormEngineContentFormPayload[];
  @Input() cfMappingTypes: FormEngineContentFormMappingTypesForGIPayload[];
  @Input() associatedContentForms: FormEngineAssociatedContentFormsForGIPayload[];
  @Input() contentFormsCount: number = 1;
  @Output() onPublishInterviewFormChange: EventEmitter<FormGroup> = new EventEmitter();

  presentDate = new Date();

  initialFormValues: any;

  publishInterviewForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.publishInterviewForm = this.fb.group({
      comments: [''],
      date: [this.getDate(), Validators.required],
      contentFormsAndMappingType: this.fb.array([])
    });
    this.initialFormValues = this.publishInterviewForm.value;
    this.addCFMappingDropdownContainer();
  }

  contentFormsAndMappingType(): FormArray {
    return this.publishInterviewForm.get("contentFormsAndMappingType") as FormArray
  }

  newContentFormAssociation(): FormGroup {
    return this.fb.group({
      contentFormNumber: null,
      contentFormTypeId: null,
    })
  }
  addCFMappingDropdownContainer() {
    this.contentFormsAndMappingType().push(this.newContentFormAssociation());
  }

  removeCFMappingDropdownContainer(i: number) {
    this.contentFormsAndMappingType().removeAt(i);
  }

  getDate() {
    return formatDate(this.presentDate);
  }

  ngOnInit(): void { }

  closeModal() {
    this.publishInterviewModal.close();
    this.publishInterviewForm.reset(this.initialFormValues);
  }

  openModal() {
    this.publishInterviewModal.open();
  }

  publishForm() {
    this.onPublishInterviewFormChange.emit(this.publishInterviewForm)
  }
}
