import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishInterviewModalComponent } from './publish-interview-modal.component';

describe('PublishInterviewModalComponent', () => {
  let component: PublishInterviewModalComponent;
  let fixture: ComponentFixture<PublishInterviewModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishInterviewModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishInterviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
