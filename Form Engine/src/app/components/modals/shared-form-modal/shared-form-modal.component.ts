import { Component, ViewChild } from '@angular/core';
import { FORM_ENGINE } from 'src/app/core/constants';
import { NavigationService } from 'src/app/core/services';
import { ModalComponent } from 'src/app/shared';

@Component({
  selector: 'app-shared-form-modal',
  templateUrl: './shared-form-modal.component.html',
  styleUrls: ['./shared-form-modal.component.scss']
})
export class SharedFormModalComponent {

  readonly SHARED_FORMS_MODAL =  FORM_ENGINE.SHARED_FORMS_MODAL;

  @ViewChild('sharedFormModal') sharedFormsModalComponent: ModalComponent;

  constructor(private navigationService: NavigationService) { }

  ngAfterViewInit(): void {
    this.sharedFormsModalComponent.open()
  }

  /**
   * Method gets called on submit of assign tags
   */
  closeModal() {
    this.sharedFormsModalComponent.close()
  }

  /**
   * Method to go back on click of cancel
   */
  cancel() {
    this.navigationService.goBack();
  }

  ngOnDestroy(): void {
    this.sharedFormsModalComponent.close();
  }

}
