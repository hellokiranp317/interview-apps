import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateInfoModalComponent } from './template-info-modal.component';

describe('TemplateInfoModalComponent', () => {
  let component: TemplateInfoModalComponent;
  let fixture: ComponentFixture<TemplateInfoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplateInfoModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
