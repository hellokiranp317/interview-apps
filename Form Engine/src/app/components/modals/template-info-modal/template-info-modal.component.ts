import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FORM_ENGINE } from 'src/app/core/constants';
import { ModalComponent } from 'src/app/shared';

@Component({
  selector: 'app-template-info-modal',
  templateUrl: './template-info-modal.component.html',
  styleUrls: ['./template-info-modal.component.scss']
})
export class TemplateInfoModalComponent implements OnInit {

  readonly CREATE_TEMPLATE_MODAL = FORM_ENGINE.CREATE_TEMPLATE_MODAL;

  @ViewChild('createTemplateModal') createTemplateModalComponent: ModalComponent;

  @Output() onCreateTemplate: EventEmitter<string> = new EventEmitter();

  @Input() createTemplateModalOptions;
  @Input() showCancel: boolean = true;
  @Input() showTitleError: boolean = false;

  templateName = new FormControl('');

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Opens up the confirmation modal
   *
   * @memberof FormsComponent
   */
  openTemplateModal(): void {
    this.createTemplateModalComponent.open();
  }

  /**
   * Closes up the confirmation modal
   *
   * @memberof FormsComponent
   */
  closeTemplateModal(): void {
    this.createTemplateModalComponent.close();
  }

  createTemplate(): void {
    this.onCreateTemplate.emit(this.templateName.value);
  }

}
