import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToastrModule } from 'ngx-toastr';

import { AuditHistoryTableComponent } from './audit-history-table.component';
import mockAuditHistoryResponse from 'src/test/data/auditHistoryResponse.json';

describe('AuditHistoryTableComponent', () => {
  let component: AuditHistoryTableComponent;
  let fixture: ComponentFixture<AuditHistoryTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuditHistoryTableComponent ],
      imports: [ HttpClientTestingModule, ToastrModule.forRoot() ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditHistoryTableComponent);
    component = fixture.componentInstance;
    component.selectedFormId = '111';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getDocumentAuditHistory should fetch the form History for the selectedForm', () => {
    component.selectedFormId = '122'
    spyOn(component, 'getDocumentAuditHistory').and.callThrough();
    component.ngOnInit();
    expect(component.getDocumentAuditHistory).toHaveBeenCalledTimes(1);
  });

  it('onClose function should trigger the closeModal event', () => {
    spyOn(component.closeModal, 'emit');
    component.onClose();
    expect(component.closeModal.emit).toHaveBeenCalledTimes(1);
  });

  it('ngOnDestroy should unsubscribe from all subscriptions', () => {
    spyOn(component.auditHistorySubscription$, 'unsubscribe');
    component.ngOnDestroy();
    expect(component.auditHistorySubscription$.unsubscribe).toHaveBeenCalledTimes(1);
  });
});
