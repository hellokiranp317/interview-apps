import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormEngineFormAuditHistoryPayload, FormEngineFormAuditHistoryResponse } from 'src/app/client';
import { ERROR, FORM_ENGINE, LOADER } from 'src/app/core/constants';
import { FormEngineFormsService, NotificationService } from 'src/app/core/services';

@Component({
  selector: 'app-audit-history-table',
  templateUrl: './audit-history-table.component.html',
  styleUrls: ['./audit-history-table.component.scss']
})
export class AuditHistoryTableComponent implements OnInit, OnDestroy {

  @Input('selectedFormNumber') selectedFormNumber: string;
  
  @Output() closeModal: EventEmitter<void> = new EventEmitter();

  readonly LOADER = LOADER;
  readonly auditHistoryTable: String = FORM_ENGINE.TABLE_TYPES.AUDIT_HISTORY;
  readonly AUDIT_HISTORY_MODAL = FORM_ENGINE.AUDIT_HISTORY_MODAL;

  auditHistory: FormEngineFormAuditHistoryPayload[] = null;
  auditHistorySubscription$: Subscription;
  isLoading: boolean = false;
  
  constructor(private formEngineFormsService: FormEngineFormsService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.selectedFormNumber ? this.getDocumentAuditHistory() : null;
  }

  /**
   * Fetches the Audit History for the selected document
   * @return void
   * @memberof AuditHistoryTableComponent
   */
  getDocumentAuditHistory(): void {
    this.isLoading = true;
    this.auditHistorySubscription$ = this.formEngineFormsService.getFormAuditHistory(this.selectedFormNumber)
    .subscribe(
      (response: FormEngineFormAuditHistoryResponse) => {
        this.auditHistory = response?.data || [];
        this.isLoading = false;
      },
      (err: ErrorEvent) => { 
        const errResponse = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
        this.showErrorToast(errResponse, )
        this.isLoading = false;
        this.onClose();
       });
  }
  
  /**
   * Method gets called by notificationService service on error
   * @param message 
   */
  showErrorToast(message) {
    this.notificationService.showError(message);
  }

  /**
   * Closes up the modal
   * @memberof AuditHistoryTableComponent
  */
  onClose(): void {
    this.closeModal.emit();
    this.auditHistorySubscription$ ? this.auditHistorySubscription$.unsubscribe() : null;
  }

  ngOnDestroy(): void {
    this.auditHistorySubscription$ ? this.auditHistorySubscription$.unsubscribe() : null;
  }

}

