import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { COMMON, FRONT_END_ROUTES, LOADER } from 'src/app/core/constants';
import { LoadingService, NavigationService } from './core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  
  readonly LOADER = LOADER;
  readonly COMMON = COMMON;

  loading$ = this.loader.loading$;
  showHeader = false;

  @ViewChild('skipToMain') skipToMain: ElementRef;

  constructor(public loader: LoadingService, private navigationService: NavigationService, private router: Router) {
    this.navigationService.saveAppHistory();
    // To hide header for Login Page
    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        if (event['url'] === FRONT_END_ROUTES.LOGIN || event['urlAfterRedirects'] === FRONT_END_ROUTES.LOGIN) {
          this.showHeader = false;
        } else {
          this.showHeader = true;
        }
      }
    });
  }

  ngOnInit() { }

  skipToMainContent(): void {
    this.skipToMain.nativeElement.focus();
  }

}
