import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ALERT_TYPES, BUTTONS, ERROR, FRONT_END_ROUTES, LOADER } from 'src/app/core/constants';
import { CurrentUser } from 'src/app/core/models';
import { Alert } from 'src/app/models';
import { SharedService } from '../../services';

@Component({
  selector: 'app-interview-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  readonly FRONT_END_ROUTES = FRONT_END_ROUTES;
  readonly ERROR = ERROR;
  readonly ALERT_TYPES = ALERT_TYPES;
  readonly BUTTONS = BUTTONS;
  readonly LOADER = LOADER;

  currentUser: CurrentUser;
  errorMessage: string;
  redirectionLoader: boolean = false;
  errorAlertData: Alert;

  subscriptions$: Subscription = new Subscription();

  constructor(
    private router: Router,
    private sharedService: SharedService
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras?.state as { message: string };
    this.errorMessage = state?.message || ERROR.MESSAGE.NOT_FOUND;
    this.setErrorAlertData();
  }

  ngOnInit(): void {
    this.setCurrentUser();
  }

  /**
   * Setter for the current user
   *
   * @memberof ErrorComponent
   */
   setCurrentUser(): void {
    this.subscriptions$.add(this.sharedService.currentUser.subscribe((currentUser: CurrentUser) => {
      currentUser ? this.currentUser = currentUser : null;
    }));
  }

  /**
   * Initializes the error alert
   *
   * @memberof ErrorComponent
   */
  setErrorAlertData(): void {
    this.errorAlertData = { type: ALERT_TYPES.DANGER, message: this.errorMessage };
  }

  ngOnDestroy(): void {
    this.subscriptions$?.unsubscribe();
  }
}
