import { Component, Input, OnInit } from '@angular/core';
import { LOADER } from 'src/app/core/constants';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

  readonly LOADER = LOADER;

  @Input('width') width: string; // In REM
  @Input('height') height: string; // In REM
  @Input('loaderType') loaderType: string;
  @Input('color') color: string;
  @Input('isFullPage') isFullPage: boolean = false;
}
