import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LOADER } from 'src/app/core/constants';

import { LoaderComponent } from './loader.component';

describe('LoaderComponent', () => {
  let component: LoaderComponent;
  let fixture: ComponentFixture<LoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderComponent);
    component = fixture.componentInstance;
    component.width = '5';
    component.height = '5';
    component.color = LOADER.COLOR.PRIMARY;
    component.loaderType = LOADER.TYPE.BORDER;
    component.isFullPage = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('LOADER has default value', () => {
    expect(component.LOADER).toEqual(LOADER);
  });

  it('isFullPage has default value', () => {
    expect(component.isFullPage).toEqual(false);
  });
});
