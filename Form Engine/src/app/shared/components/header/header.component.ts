import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { ASSETS, COMMON, FRONT_END_ROUTES, HEADER, MYCASE_ROUTES } from 'src/app/core/constants';
import { CurrentUser } from 'src/app/core/models';
import { SharedService } from '../../services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  readonly NAV_ITEMS = HEADER.NAV_ITEMS;
  readonly HYPERLINKS = HEADER.HYPERLINKS;
  readonly ASSETS = ASSETS;
  readonly ROUTES = FRONT_END_ROUTES;
  readonly MYCASE_ROUTES = MYCASE_ROUTES;
  readonly COMMON = COMMON;

  isCollapsed: boolean = true;
  toggled: boolean = false;
  currentUser: CurrentUser;

  subscriptions$: Subscription = new Subscription();

  constructor(private sharedService: SharedService) { }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  /**
   * Toggles the navbar toggled boolean
   *
   * @memberof HeaderComponent
   */
   navbarToggler(): void {
    this.toggled = !this.toggled;
  }

  /**
   * getter for the current user
   *
   * @memberof HeaderComponent
   */
   getCurrentUser(): void {
    this.subscriptions$.add(this.sharedService.currentUser.subscribe((currentUser: CurrentUser) => {
      currentUser ? this.currentUser = currentUser : null;
    }));
  }


  /**
   * Logouts the user
   *
   * @memberof HeaderComponent
   */
  logoutUser(): void {
    this.sharedService.logout();
  }

}
