import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FRONT_END_ROUTES } from 'src/app/core/constants';

import { BreadcrumbComponent } from './breadcrumb.component';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreadcrumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    component.breadcrumbs = [{url: FRONT_END_ROUTES.FORMS_DASHBOARD, label:'Dashboard'},{url: FRONT_END_ROUTES.FORMS_DASHBOARD, label:'Dashboard'}]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('isLast method to identify the last breadcrumb', () => {
    expect(component.isLast(1)).toBeTruthy();
  });
});
