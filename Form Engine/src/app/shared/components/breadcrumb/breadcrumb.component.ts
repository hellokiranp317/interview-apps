import { Component, Input, OnInit } from '@angular/core';
import { BreadCrumb } from 'src/app/core/models/Breadcrumb';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  @Input() breadcrumbs: Array<BreadCrumb>;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Method to check if it is the last breadcrumb
   * @param index 
   * @returns 
   */
  isLast(index) {
    return index === this.breadcrumbs.length - 1;
  }
}
