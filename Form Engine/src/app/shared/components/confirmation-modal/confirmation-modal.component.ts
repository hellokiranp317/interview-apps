import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BUTTONS, MODAL } from 'src/app/core/constants';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit {

  @Input('message') message: string;
  @Input('title') title?: string;
  @Input('cancel') cancel? : string;
  @Input('continue') continue? : string;
  @Output("onCancel") onCancel?: EventEmitter<void> = new EventEmitter();
  @Output("onContinue") onContinue?: EventEmitter<void> = new EventEmitter();
  
  @ViewChild('modal') modalComponent: ModalComponent;

  readonly BUTTONS = BUTTONS;
  readonly modalSize: string = MODAL.SIZE.LG;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.openModal();
  }

  /**
   * Opens the modal
   * 
   * @memberof ConfirmationModalComponent
   */
  openModal(): void {
    this.modalComponent.open();
  }

  /**
   * Closes the modal
   * 
   * @memberof ConfirmationModalComponent
   */
  closeModal(): void {
    this.modalComponent.close();
  }

  /**
   * Emits cancel event
   * 
   * @memberof ConfirmationModalComponent
   */
  onCancelling(): void {
    this.onCancel.emit();
    this.closeModal();
  }

  /**
   * Emits continue event
   * 
   * @memberof ConfirmationModalComponent
   */
  onContinuing(): void {
    this.onContinue.emit();
    this.closeModal();
  }

  ngOnDestroy(): void {
    this.closeModal();
  }

}
