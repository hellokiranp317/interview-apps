import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ALERT_TYPES, ERROR } from 'src/app/core/constants';
import { Alert } from 'src/app/models';
import { AlertService } from '../../services';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss']
})
export class AlertMessageComponent implements OnInit, OnDestroy {

  @Input('staticAlert') staticAlert: Alert;
  @Output() dismissed: EventEmitter<void> = new EventEmitter();
  @ViewChild('ngbAlert') ngbAlert: NgbAlert;

  alertData: Alert;
  alertSubscription$: Subscription;

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.subscribeToAlertMessage();
  }

  /**
   * Show alert message whenever the alertMessage behavior subject changes
   *
   * @memberof AlertMessageComponent
   */
  subscribeToAlertMessage(): void {
    this.alertService.alertMessage.subscribe((alertData: Alert) => { 
      alertData ? this.setAlertData(alertData) : null;
      if(this.alertData?.toBeDismissedIn && this.alertData?.toBeDismissedIn != 0) { 
        this.dismissAlertInSeconds(this.alertData?.toBeDismissedIn) 
      }
    });
  }

  /**
   * Initializes the alert data
   *
   * @param {Alert} alertData
   * @memberof AlertMessageComponent
   */
  setAlertData(alertData: Alert): void {
    this.alertData = {
      type: alertData?.type || ALERT_TYPES.DANGER,
      message: alertData?.message || ERROR.MESSAGE.SOMETHING_WENT_WRONG,
      dismissable: alertData?.dismissable || false,
      toBeDismissedIn: alertData?.toBeDismissedIn || 0
    }; 
  }

  /**
   * Emits a event whenever alert is dismissed
   *
   * @memberof AlertMessageComponent
   */
  onAlertDismissed(): void {
    this.dismissed.emit();
    this.clearAlertMessage();
  }

    /**
   * Dismisses the alert in specified seconds
   *
   * @param {number} seconds
   * @memberof DocumentsTableComponent
   */
  dismissAlertInSeconds(seconds: number): void {
    setTimeout(() => { 
      this.ngbAlert?.close();
      this.clearAlertMessage();
    }, seconds);
  }

  /**
   * Clears the alert message
   *
   * @memberof AlertMessageComponent
   */
  clearAlertMessage(): void {
    this.alertService.setAlertMessage(null);
    this.alertData = null;
  }

  ngOnDestroy(): void {
    this.alertSubscription$?.unsubscribe();
  }
}
