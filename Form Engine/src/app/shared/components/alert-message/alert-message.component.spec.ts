import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { Alert } from 'src/app/models';
import { AlertService } from '../../services';
import { AlertMessageComponent } from './alert-message.component';

describe('AlertMessageComponent', () => {
  let component: AlertMessageComponent;
  let fixture: ComponentFixture<AlertMessageComponent>;

  beforeEach(() => {
    const alertServiceStub = () => ({
      alertMessage: { subscribe: f => f(
        {
          type: "info",
          message: 'sample message',
          dismissable: false
        } 
      )},
      setAlertMessage: arg => ({})
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [AlertMessageComponent],
      providers: [{ provide: AlertService, useFactory: alertServiceStub }]
    });
    fixture = TestBed.createComponent(AlertMessageComponent);
    component = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      spyOn(component, 'subscribeToAlertMessage').and.callThrough();
      component.ngOnInit();
      expect(component.subscribeToAlertMessage).toHaveBeenCalled();
    });
  });

  describe('subscribeToAlertMessage', () => {
    it('makes expected calls', fakeAsync(() => {
      spyOn(component, 'setAlertData').and.callThrough();
      component.subscribeToAlertMessage();
      expect(component.setAlertData).toHaveBeenCalled();
    }));
  });

  describe('setAlertData', () => {
    it('initializes the alert data', () => {
      const alertData: Alert = {
        type: "info",
        message: 'sample message',
        dismissable: true,
        toBeDismissedIn: 2
      }
      spyOn(component, 'setAlertData').and.callThrough();
      component.setAlertData(alertData);
      expect(component.alertData).toEqual(alertData);
    });
  });

  describe('onAlertDismissed', () => {
    it('makes expected calls', () => {
      spyOn(component, 'clearAlertMessage').and.callThrough();
      spyOn(component.dismissed,'emit');
      component.onAlertDismissed();
      expect(component.dismissed.emit).toHaveBeenCalled();
      expect(component.clearAlertMessage).toHaveBeenCalled();
    });
  });

  describe('dismissAlertInSeconds dismisses the alert in specified seconds', () => {
    it('makes expected calls', fakeAsync(() => {
      spyOn(component, 'clearAlertMessage').and.callThrough();
      component.dismissAlertInSeconds(2);
      tick(4);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.clearAlertMessage).toHaveBeenCalled();
      });
      flush();
    }));
  });

  describe('clearAlertMessage', () => {
    it('makes expected calls', () => {
      const alertServiceStub: AlertService = fixture.debugElement.injector.get(
        AlertService
      );
      spyOn(alertServiceStub, 'setAlertMessage').and.callThrough();
      component.clearAlertMessage();
      expect(alertServiceStub.setAlertMessage).toHaveBeenCalled();
      expect(component.alertData).toBeNull();
    });
  });
});
