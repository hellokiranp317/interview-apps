import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FloatingFormInputComponent } from './floating-form-input.component';


@Component({
  template: '<app-floating-form-input label="label" [formControl]="folderName"></app-floating-form-input>'
})
class TestHostFormComponent {
  @ViewChild(FloatingFormInputComponent)
  public floatingFormInputComponent: FloatingFormInputComponent;

  public folderName: FormControl = new FormControl('foo');
}


describe('FloatingFormInputComponent', () => {
  let testHostComponent: TestHostFormComponent;
  let hostFixture: ComponentFixture<TestHostFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloatingFormInputComponent, TestHostFormComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    hostFixture = TestBed.createComponent(TestHostFormComponent);
    testHostComponent = hostFixture.componentInstance;
    hostFixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent.floatingFormInputComponent).toBeTruthy();
  });

  it('should display value as provided initial formControl value', () => {
    expect(testHostComponent.floatingFormInputComponent.value).toEqual('foo');
  });
  
  it('should display valuen based on patched formControl value', () => {
    testHostComponent.folderName.patchValue('bar');
    hostFixture.detectChanges();
    expect(testHostComponent.floatingFormInputComponent.value).toEqual('bar');
  });

  it('should send a change event with the new value when value is typed', () => {
    testHostComponent.floatingFormInputComponent.value = 'foobar'
    testHostComponent.floatingFormInputComponent.onChange('foobar');
    hostFixture.detectChanges();
    expect(testHostComponent.folderName.value).toBe('foobar');
  });

  it('should clear the input field if null', () => {
    testHostComponent.floatingFormInputComponent.value = 'foobar'
    testHostComponent.floatingFormInputComponent.onChange('foobar');
    expect(testHostComponent.folderName.value).toBe('foobar');
    testHostComponent.floatingFormInputComponent.value = '';
    testHostComponent.floatingFormInputComponent.onChange('');
    const compiled = hostFixture.debugElement.nativeElement;
    compiled.querySelector('input').dispatchEvent(new Event('blur'));
    expect(testHostComponent.folderName.value).toBe('');
  })
});
