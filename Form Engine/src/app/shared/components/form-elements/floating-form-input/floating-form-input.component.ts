import { Component, OnInit, Input, Self, Optional, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-floating-form-input',
  templateUrl: './floating-form-input.component.html',
  styleUrls: ['./floating-form-input.component.scss']
})

export class FloatingFormInputComponent implements OnInit, ControlValueAccessor {

  @Input() id: string;
  @Input() disabled: boolean;
  @Input() label: string;
  @Input() placeholder: string = '';
  @Input() autocomplete: string = 'off';
  @Input() required: boolean = false;
  @Input() type: 'text' | 'email' | 'password' = 'text';

  @Output() input: EventEmitter<string> = new EventEmitter();

  isInputInFocus: boolean;

  value: string = '';

  constructor(
    // Retrieve the dependency only from the local injector, not from parent or ancestors.
    @Self()
    // We want to be able to use the component without a form, so we mark the dependency as optional.
    @Optional()
    private ngControl: NgControl,
    private renderer: Renderer2,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  public ngOnInit(): void {
    if (this.ngControl) {
      this.ngControl.control.valueChanges.subscribe(v => {
        this.value = v;
      });
    }    
  }

  /**
   * Write form value to the DOM element (model => view)
   */
  writeValue(value: string): void {
    this.value = value;
  }

  /**
   * Write form disabled state to the DOM element (model => view)
   */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /**
   * Update form when DOM element value changes (view => model)
   */
  registerOnChange(fn: any): void {
    // Store the provided function as an internal method.
    this.onChange = fn;
  }

  /**
   * Update form when DOM element is blurred (view => model)
   */
  registerOnTouched(fn: any): void {
    // Store the provided function as an internal method.
    this.onTouched = fn;
  }

  onChange(value: string) :void {
    this.input.emit(value)
  }
  onTouched: () => void;

  setValue(value) {
    this.value = value;
    this.onChange(this.value)
  }

  /* Floating label methods */
  /**
   * Add 'animate-label' class to label on focus of form field 
   * @param label
   * @returns void
   */
  onFocus(label : ElementRef): void{
    this.isInputInFocus = true;
    this.renderer.addClass(label,'label-animate');
  }

  /**
   * Remove 'animate-label' class to label on blur of form field 
   * @param field
   * @param label
   * @returns void
   */
  onBlur(label : ElementRef): void{
      this.onTouched();
      this.isInputInFocus = false;
      if(this.value) return;
      this.renderer.removeClass(label,'label-animate');
  }
}