import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input('modalTitle') modalTitle: string;
  @Input('modalSize') modalSize: string;
  @Input('showCloseButton') showCloseButton: boolean = true;
  @Input('headerStyle') headerStyle: string;
  @Input('modalOptions') modalOptions: Object = null;
  @Input('showModalHeader') showModalHeader: boolean = true;
  @Input('showModalFooter') showModalFooter: boolean = false;
  @Output() modalClosed: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('modal') private modalContent: TemplateRef<ModalComponent>
  modalRef: NgbModalRef;

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  /**
   * Opens up the modal
   *
   * @return {*}  {Promise<boolean>}
   * @memberof ModalComponent
   */
  open(): Promise<boolean> {
    return new Promise<boolean>(resolve => {
      this.modalRef = this.modalService.open(this.modalContent, { centered: true,  size: this.modalSize, ...this.modalOptions })
      this.modalRef.result.then(resolve, () => { this.modalClosed.emit(true); });
    })
  }

  /**
   * Closes up the modal
   *
   * @memberof ModalComponent
   */
  close(){
    if(this.modalRef){ this.modalRef.close(); }
  }
}
