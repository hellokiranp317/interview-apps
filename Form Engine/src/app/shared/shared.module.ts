import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgBootstrapModule } from 'src/app/ng-bootstrap/ng-bootstrap.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormioModule } from 'angular-formio';

// Components and Pages
import {
  LoaderComponent,
  ModalComponent,
  HeaderComponent,
  BreadcrumbComponent,
  FloatingFormInputComponent,
  AlertMessageComponent } from './components';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { ErrorComponent } from './pages/error/error.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';

@NgModule({
  declarations: [
    LoaderComponent,
    ModalComponent,
    HeaderComponent,
    FloatingFormInputComponent,
    BreadcrumbComponent,
    ClickOutsideDirective,
    ErrorComponent,
    AlertMessageComponent,
    ConfirmationModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgBootstrapModule,
    NgxDatatableModule,
    FormioModule
  ],
  exports: [
    LoaderComponent,
    ModalComponent,
    HeaderComponent,
    FloatingFormInputComponent,
    BreadcrumbComponent,
    NgBootstrapModule,
    NgxDatatableModule,
    FormioModule,
    ClickOutsideDirective,
    AlertMessageComponent,
    ErrorComponent,
    ConfirmationModalComponent
  ]
})
export class SharedModule { }
