import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Configuration, LoginResponse } from 'src/app/client';
import { AuthenticateResponse } from 'src/app/models';
import { COMMON, ERROR, FRONT_END_ROUTES, SECURITY } from '../../core/constants';
import { CurrentUser } from '../../core/models';
import { getCurrentUserFromLocalStorage, removeLocalStorageData, setLocalStorageData } from '../../core/utils';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private currentUserSubject: BehaviorSubject<CurrentUser>;
  public currentUser: Observable<CurrentUser>;
  
  constructor(private router: Router,private http: HttpClient,  private apiConfiguration: Configuration) { 
    this.currentUserSubject = new BehaviorSubject<CurrentUser>(getCurrentUserFromLocalStorage());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  /**
   * Getter for the current user
   *
   * @readonly
   * @type {CurrentUser}
   * @memberof SharedService
   */
  public get currentUserValue(): CurrentUser {
    return this.currentUserSubject.value;
  }

  /**
   * Redirects the user to a specific route
   *
   * @param {string} route
   * @memberof SharedService
   */
  redirectRoute(route: string, navigationExtras?: NavigationExtras, pathParams?: number): void {
    if (navigationExtras) {
      this.router.navigate([route], navigationExtras);
    } else if (pathParams) {
      this.router.navigate([route, pathParams]);
    } else {
      this.router.navigate([route]);
    }
  }

  redirectBasedOnRole(role: string){
    switch (role) {
      case COMMON.DASHBOARD_ACCESS_ROLES.ADMIN:
        
      case COMMON.DASHBOARD_ACCESS_ROLES.AUTHOR:
        
      case COMMON.DASHBOARD_ACCESS_ROLES.READER:
        this.redirectRoute(FRONT_END_ROUTES.FORMS_DASHBOARD, null);
        break;
      default :
        this.redirectRoute(FRONT_END_ROUTES.LOGIN, null);
    }    
  }

  /**
   * Log outs the user
   *
   * @return {void}
   * @memberof SharedService
   */
  logout(): void {
    this.unPersistToken();
    this.redirectRoute(FRONT_END_ROUTES.LOGIN, { state: { message: ERROR.MESSAGE.NOT_LOGGED_IN } });
  }

  /**
   * Returns the API Key from the Client Configuration
   *
   * @readonly
   * @memberof SharedService
   */
  get APIConfigurationKey(): string { return this.apiConfiguration.credentials[COMMON.BEARER].toString(); }

  /**
   * Removes the token persistance
   * and user role
   * @memberof SharedService
   */
  unPersistToken(): void {
    this.setCurrentUserValue(null);
    removeLocalStorageData(COMMON.CURRENT_USER);
    this.apiConfiguration.credentials[COMMON.BEARER] = null;
  }

  /**
   * Persists the token
   *
   * @param {AuthenticateResponse} tokens
   * @return {*}  {CurrentUser}
   * @memberof SharedService
   */
  persistToken(response: AuthenticateResponse | LoginResponse): CurrentUser {
    const currentUser: CurrentUser = this.constructCurrentUser(response);
    this.unPersistToken();
    setLocalStorageData(COMMON.CURRENT_USER, JSON.stringify(currentUser));
    this.apiConfiguration.credentials[COMMON.BEARER] = `${SECURITY.BEARER}${response.accessToken}`;
    return currentUser;
  }

  /**
   * Constructs current user object based on the access and refresh token values
   *
   * @param {AuthenticateResponse} response
   * @return {*}  {CurrentUser}
   * @memberof SharedService
   */
  constructCurrentUser(response: AuthenticateResponse | LoginResponse): CurrentUser {
    let currentUser: CurrentUser = {};
    // If the response has refresh token ( Response from AUTHENTICATE Endpoint ), this will be executed
    if (response?.refreshToken) {
      currentUser = {
        accessToken: response?.accessToken,
        refreshToken: response?.refreshToken,
        username: response?.data?.userFullName || null,
      }
    }
    // If the response contain only access token ( Response from REFRESH_TOKEN Endpoint ), this will be executed
    else {
      const persistedCurrentUser: CurrentUser = getCurrentUserFromLocalStorage();
      currentUser.accessToken = response?.accessToken;
      currentUser.refreshToken = persistedCurrentUser?.refreshToken;
      currentUser.username = persistedCurrentUser?.username;
    }
    return currentUser;
  }

  /**
   * Setter for the current user
   *
   * @param {CurrentUser} value
   * @memberof SharedService
   */
  setCurrentUserValue(value: CurrentUser): void {
    this.currentUserSubject.next(value);
  }

  /**
   * Make a post request
   *
   * @param {string} requestUrl
   * @param {any} requestBody
   * @return {*}  {Observable<any>}
   * @memberof SharedService
   */
  public doPost(requestUrl: string, requestBody: any, credential?: string): Observable<any> {

    let headers = new HttpHeaders();

    // To Determine the Accept header
    let httpHeaderAccepts: string[] = [SECURITY.MIME_TYPE.JSON];
    headers.set(SECURITY.ACCEPT_HEADER, httpHeaderAccepts);

    if (credential) {
      headers = headers.set('Authorization', credential);
    }

    return this.http.post<any>(
      requestUrl,
      requestBody,
      { headers: headers }
    );
  }
    
}
