import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Alert } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private alertMessageSubject: BehaviorSubject<Alert>;
  public alertMessage: Observable<Alert>;

  constructor() { 
    this.alertMessageSubject = new BehaviorSubject<Alert>(null);
    this.alertMessage = this.alertMessageSubject.asObservable();
  }

  /**
   * Getter for alertMessage
   *
   * @readonly
   * @type {Alert}
   * @memberof AlertService
   */
  get getAlertMessage(): Alert { return this.alertMessageSubject.getValue(); }

  /**
   * Setter for alertMessage
   *
   * @param {Alert} alertMessage
   * @memberof AlertService
   */
  setAlertMessage(alertMessage: Alert): void { this.alertMessageSubject.next(alertMessage); }
}
