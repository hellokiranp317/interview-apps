import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { AuthenticationApiService, RefreshTokenResponse } from 'src/app/client';
import { CurrentUser } from 'src/app/core/models';
import { SharedService } from '.';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private sharedService: SharedService, private authenticationApiService: AuthenticationApiService) { }

  /**
   * Refreshes and sets the new JWT for the current user
   *
   * @param {string} currentToken
   * @memberof AuthenticationService
   */
  refreshJWT(refreshToken: string) {
    return this.authenticationApiService.refreshToken({ refreshToken }).pipe(
      tap((response: RefreshTokenResponse) => {
        const currentUser: CurrentUser = this.sharedService.persistToken(response);
        this.sharedService.setCurrentUserValue(currentUser);
        return response;
      })
    );
  }
}
