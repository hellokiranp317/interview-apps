import { FormGroup, ValidatorFn } from '@angular/forms';

/**
 * Validator to check if minimum number of checkbox is checked
 * @param minRequired 
 * @returns ValidatorFn
 */
export function multiCheckboxRequiredValidator(minRequired = 1): ValidatorFn {
    const validtor: ValidatorFn =  (formGroup: FormGroup) => {
        let checked = 0;

        Object.keys(formGroup.controls).forEach(key => {
            const control = formGroup.controls[key];

            if (control.value === true) {
                checked ++;
            }
        });
        return checked < minRequired ? { required: true } : null;
    }

    return validtor;

}