import { FormControl } from "@angular/forms";

/**
 * Custom validator to verify file type
 * @param type 
 * @returns boolean
 */
export function requiredFileType( type: string ) {
    return function (control: FormControl) {
      const file = control.value;
      if (file) {
        if ( type.toLowerCase() !== file.type ) {
          return {
            requiredFileType: true
          };
        }
        
        return null;
      }
  
      return null;
    };
}