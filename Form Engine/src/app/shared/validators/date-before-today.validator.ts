import { FormControl } from "@angular/forms";

/**
 * Custom validator to check if date is lesser than today's date (Past date)
 * @param type 
 * @returns boolean
 */
export function isDateBeforeToday() {
    return function (control: FormControl) {
      const enteredDate = control.value;
      const today = new Date();
      if (new Date(enteredDate.replace(/-/g, '\/')).setHours(0,0,0,0) < today.setHours(0,0,0,0)) {
        return {
            isDateBeforeToday: true
        }
      }
      return null;
    };
}