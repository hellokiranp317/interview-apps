import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  NgbDropdownModule,
  NgbCollapseModule,
  NgbAlertModule,
  NgbPaginationModule,
  NgbNavModule
} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgbDropdownModule,
    NgbCollapseModule,
    NgbAlertModule,
    NgbPaginationModule,
    NgbNavModule
  ],
  exports: [
    NgbDropdownModule,
    NgbCollapseModule,
    NgbAlertModule,
    NgbPaginationModule,
    NgbNavModule
  ]
})
export class NgBootstrapModule { }
