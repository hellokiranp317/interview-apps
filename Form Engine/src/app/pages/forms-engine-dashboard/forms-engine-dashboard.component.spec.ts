import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { of } from 'rxjs';
import { NgSelectComponent } from '@ng-select/ng-select';

import { DashboardService } from 'src/app/core/services';
import { LoadingService } from 'src/app/core/services/loading.service';
import mockFormsJson from 'src/test/data/formsResponse.json';
import mockCaseTypeJson from 'src/test/data/caseTypes.json';
import mockFormEngineFormData from 'src/test/data/formEngineFormResponse.json';

import { FormsEngineDashboardComponent } from './forms-engine-dashboard.component';
import { ModalComponent } from 'src/app/shared';
import { Router } from '@angular/router';
import { FORM_ENGINE, FRONT_END_ROUTES } from 'src/app/core/constants';

describe('FormsEngineDashboardComponent', () => {
  let component: FormsEngineDashboardComponent;
  let fixture: ComponentFixture<FormsEngineDashboardComponent>;
  let router: Router;

  // let mockRouter = {
  //   navigate: jasmine.createSpy('navigate')
  // }
  let mockDashboardService = {
    getFormEngineFormsResponse: null,
    getForms: () => of(mockFormsJson),
    getCaseTypes: () => of(mockCaseTypeJson)
  }

  let mockToastrService = {
    success: () => {},
    error: () => {}
  }

  let mockLoaderService = {
    showLoader: () => {},
    hideLoader: () => {}
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsEngineDashboardComponent, ModalComponent, NgSelectComponent ],
      imports: [ RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot(), NgSelectModule ],
      providers: [
        {
          provide: DashboardService,
          useValue: mockDashboardService
        },
        {
          provide: LoadingService,
          useValue: mockLoaderService
        },
        {
          provide: ToastrService,
          useValue: mockToastrService
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsEngineDashboardComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit function gets called', () => {
    spyOn(component, 'getInterviewCaseTypes').and.callThrough();
    spyOn(component, 'getForms').and.callThrough();
    component.ngOnInit();
    expect(component.getForms).toHaveBeenCalled();
    expect(component.getInterviewCaseTypes).toHaveBeenCalled();
  });

  it('getForms function gets called', () => {
    spyOn(component, 'setFormEngineFormsResponse').and.callThrough();
    spyOn(mockDashboardService, 'getForms').and.returnValue(of(mockFormsJson))
    component.getForms();
    expect(mockDashboardService.getForms).toHaveBeenCalled();
    expect(component.setFormEngineFormsResponse).toHaveBeenCalled();
    expect(component.formsData).toEqual(mockFormsJson.data)
  });

  it('getCaseTypes function gets called', () => {
    spyOn(mockDashboardService, 'getCaseTypes').and.returnValue(of(mockCaseTypeJson))
    component.getMyCaseCaseTypes();
    expect(mockDashboardService.getCaseTypes).toHaveBeenCalled();
    expect(component.caseTypeData).toEqual(mockCaseTypeJson.data.caseTypes)
  });

  it('openModal should open up the modal', () => {
    spyOn(component.modalComponent, 'open');
    component.openModal();
    expect(component.modalComponent.open).toHaveBeenCalledTimes(1);
  })
  
  it('closeModal should close the modal', () => {
    spyOn(component.modalComponent, 'close');
    component.closeModal();
    expect(component.modalComponent.close).toHaveBeenCalledTimes(1);
    expect(component.errorMessage).toEqual('');
  });

  it('isNewForm returns false if date diff is greater than 7', () => {
    expect(component.isNewForm('07-08-2019')).toBeFalsy;
  });

  it('removeItem removes item from the list', () => {
    const list = [{ formId: 10 }, { formId: 11 }]
    component.removeItem(10, list)
    expect(list.length).toBe(1)
  });
  it('show success toast', () => {
    spyOn(mockToastrService, 'success').and.returnValue()
    component.showSuccessToast('xxx');
    expect(mockToastrService.success).toHaveBeenCalled();
  })
  it('show failure toast', () => {
    spyOn(mockToastrService, 'error').and.returnValue()
    component.showErrorToast('xxx');
    expect(mockToastrService.error).toHaveBeenCalled();
  })

  it('filterFormsByStatus returns array with that particular status', () => {
    expect(component.filterFormsByStatus('In Progress').length).toBe(1);
  });

  it('openGeneralTagsModal should open the openGeneralTagsModal', () => {
    component.openGeneralTagsModal();
    expect(component.assignTabCurrentTab).toEqual(FORM_ENGINE.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
    expect(component.showAssignTagsModal).toBeTruthy();
  });

  it('Redirect to case type dashboard', () => {
    spyOn(router, 'navigate');
    const caseTypeId = '101';
    component.redirectToCaseType(caseTypeId);
    expect(router.navigate).toHaveBeenCalledWith([FRONT_END_ROUTES.FORMS_LIST_DASHBOARD], { queryParams: { 'case-type': caseTypeId }});
  });

  it('redirectToNextStep redirects to Forms page', () => {
    spyOn(router, 'navigate');
    const currentRow = mockFormEngineFormData;
    component.redirectToNextStep(currentRow);
    expect(router.navigate).toHaveBeenCalledWith([FRONT_END_ROUTES.FORMS], currentRow.formId);
  });

});