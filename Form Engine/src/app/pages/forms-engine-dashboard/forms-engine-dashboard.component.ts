import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormEngineCaseTypes, FormEngineCaseTypesResponse, FormEngineCreateGuidedFormsResponse, FormEngineForm, FormEngineFormsResponse, FormEngineFormStatus, FormEngineFormTypes, FormEngineGetFormStatus, FormEnginePostTemplateResponse, RequestedSystemEnum, SortOrderEnum } from 'src/app/client';
import { CreateGuidedInterviewModalComponent, TemplateInfoModalComponent } from 'src/app/components';
import { ERROR, FORM_ENGINE, FRONT_END_ROUTES, LOADER } from 'src/app/core/constants';
import { BreadCrumb } from 'src/app/core/models/Breadcrumb';
import { DashboardService, FormEngineCaseTypeService, FormEngineGuidedInterviewService, FormEngineTemplatesService, LoadingService, NotificationService } from 'src/app/core/services';
import { ModalComponent } from 'src/app/shared';


@Component({
    selector: 'app-forms-engine-dashboard',
    templateUrl: './forms-engine-dashboard.component.html',
    styleUrls: ['./forms-engine-dashboard.component.scss']
})
export class FormsEngineDashboardComponent implements OnInit {

    readonly LOADER = LOADER;
    readonly FORMS_ROUTE = FRONT_END_ROUTES.FORMS;
    readonly FORM_ENGINE_CONSTANTS = FORM_ENGINE;
    readonly UPLOAD_NEW_FORM_MODAL = FORM_ENGINE.UPLOAD_NEW_FORM_MODAL;
    readonly ADD_CASE_TYPE_MODAL = FORM_ENGINE.ADD_CASE_TYPE_MODAL;
    readonly breadcrumbs: Array<BreadCrumb> = [{ url: FRONT_END_ROUTES.FORMS_DASHBOARD, label: 'Dashboard' }]
    readonly NUMBER_OF_IN_PROGRESS_FORMS = 4;
    readonly NUMBER_OF_PUBLISHED_FORMS = 4;
    readonly FORM_STATUS = FormEngineFormStatus;
    readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;
    readonly PAGE_CONSTANTS = FORM_ENGINE.FORM_ENGINE_DASHBOARD_PAGE_CONSTANTS;
    readonly FORMIO_ROUTE_FORM_TYPES = FORM_ENGINE.FORMIO_ROUTE_FORM_TYPES;
    readonly FORM_TYPES = FormEngineFormTypes;
    readonly GUIDED_INTERVIEW_CREATE_MODAL = FORM_ENGINE.GUIDED_INTERVIEW_CREATE_MODAL;
    readonly CREATE_TEMPLATE_MODAL = FORM_ENGINE.CREATE_TEMPLATE_MODAL;

    @ViewChild('modal') modalComponent: ModalComponent;
    @ViewChild('createGuidedInterviewModalComponent') createGuidedInterviewModal: CreateGuidedInterviewModalComponent;
    @ViewChild('createTemplateModal') createTemplateModalComponent: TemplateInfoModalComponent;

    formEngineFormsSubscription$: Subscription;
    caseTypesSubscription$: Subscription;
    createGiSubscription$: Subscription;
    createTemplateSubscription$: Subscription;

    formsData: FormEngineForm[];
    inProgressAndNewForms: FormEngineForm[];
    publishedForms: FormEngineForm[];
    caseTypeData: FormEngineCaseTypes[];
    selectedCaseTypes: FormEngineCaseTypes[];

    // Commenting out as of now to remove the Add Case Type functionality

    // @ViewChild(NgSelectComponent) caseTypeSelect: NgSelectComponent;
    // isMyCaseCaseTypeLoading: boolean = false;
    // showAddNewCaseTypeModal: boolean = false;
    // newCaseTypeName: FormEngineCreateCaseTypeRequest = null;
    // isPopular: boolean=false; 
    // caseTypeDataMyCase: FormEngineCaseTypes[];
    // createCaseTypesSubscription$: Subscription;

    isPointerOverContainer: boolean = false;
    isLoading: number = 0;
    errorMessage: string = null;
    isFolderNameInputInFocus: boolean = false;
    inProgressCount: number = 0;
    fixedFoldersCount: number = 1;
    caseTypesPageSize: number = 7;
    caseTypeCurrentPage: number = 1;
    navigateToLastPage: boolean = false;
    isFetchDashboardFormDetails: boolean = false;
    isTagsSaved: boolean = false;
    showGiFormLoader: boolean = false;

    //Assign Tag Modal attributes
    showAssignTagsModal: boolean = false;
    showCreateGiModal: boolean = false;
    modalTitle: string;
    createBtnDisabled: boolean = true;
    assignTabCurrentTab: number;
    formDetails: any = null;
    showPublishTab: boolean = true;
    assignTagsModalHeader: string = this.ASSIGN_TAGS_MODAL.ASSIGN_HEADER;
    assignTagsType: string = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
    isFormUploadRequired: boolean = true;
    createGiFormErrors = {
        showTitleError : false
    };
    createTemplateErrors = {
        showTitleError : false
    };
    constructor(private dashboardService: DashboardService,
        private notificationService: NotificationService,
        public loader: LoadingService,
        private formEngineGuidedService: FormEngineGuidedInterviewService,
        private formEngineCaseTypeService: FormEngineCaseTypeService,
        private formEngineTemplateService: FormEngineTemplatesService,
        private router: Router) { }

    ngOnInit(): void {
        this.getInterviewCaseTypes();
        this.getInprogressAndNewForms();
        this.getPublishedForms();
    }

    /******************** START: Methods to fetch dashboard data ********************/
    /**
     * Method to get all case_types of Interview type
     * @memberof FormsDashboardComponent
     * @returns void
     */
    getInterviewCaseTypes(): void {
        this.isLoading += 1;
        this.caseTypesSubscription$ = this.formEngineCaseTypeService.getCaseTypes(RequestedSystemEnum.Interview)
            .subscribe((response: FormEngineCaseTypesResponse) => { this.setFormEngineCaseTypesResponse(response); },
                (err: ErrorEvent) => {
                    const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
                    this.showErrorToast(errResponse, { code: err?.error?.error?.code })
                    this.isLoading--;
                })
    }

    /**
     * function to get the forms details
     * @memberof FormsDashboardComponent
     * @returns void
     */
    getPublishedForms(): void {
        this.isLoading += 1;
        this.formEngineFormsSubscription$ = this.dashboardService.getForms([FormEngineGetFormStatus.Published], [FormEngineFormTypes.ContentForm, FormEngineFormTypes.GuidedInterview], this.NUMBER_OF_PUBLISHED_FORMS, SortOrderEnum.Desc)
            .subscribe((response: FormEngineFormsResponse) => { this.setFormEnginePublishedFormsResponse(response); },
                (err: ErrorEvent) => {
                    const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
                    this.showErrorToast(errResponse, { code: err?.error?.error?.code })
                    this.isLoading--;
                })
    }

    getInprogressAndNewForms(): void {
        this.isLoading += 1;
        this.formEngineFormsSubscription$ = this.dashboardService.getForms([ FormEngineGetFormStatus.InProgress, FormEngineGetFormStatus.New], [FormEngineFormTypes.ContentForm, FormEngineFormTypes.GuidedInterview, FormEngineFormTypes.Form], this.NUMBER_OF_IN_PROGRESS_FORMS, SortOrderEnum.Desc)
            .subscribe((response: FormEngineFormsResponse) => { this.setFormEngineInProgressAndNewFormsResponse(response); },
                (err: ErrorEvent) => {
                    const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
                    this.showErrorToast(errResponse, { code: err?.error?.error?.code })
                    this.isLoading--;
                })
    }

    setFormEngineInProgressAndNewFormsResponse(response: FormEngineFormsResponse) {
        if (response.success) {
            this.inProgressAndNewForms = response.data ? response.data.slice(0, this.NUMBER_OF_IN_PROGRESS_FORMS) : [];
            this.inProgressCount = response.inProgressCount;
        }
        this.isLoading -= 1;
    }


    /**
     * Setter for  FormEngineFormsResponse
     *
     * @param {FormEngineFormsResponse} response
     * @memberof FormsDashboardComponent
     */
    setFormEnginePublishedFormsResponse(response: FormEngineFormsResponse): void {
        if (response.success) {
            this.publishedForms = response.data ? response.data : [];
        }
        this.isLoading -= 1;
    }

    /**
     * Setter for  FormEngineCaseTypesResponse
     *
     * @param {FormEngineCaseTypesResponse} response
     * @memberof FormsDashboardComponent
     */
    setFormEngineCaseTypesResponse(response: FormEngineCaseTypesResponse): void {
        if (response.success) {
            this.caseTypeData = response.data && response.data.caseTypes ?
                [...response.data.caseTypes].sort((caseTypeA, caseTypeB) => Number(caseTypeB.popular) - Number(caseTypeA.popular))
                : [];
        }
        this.isLoading -= 1;
    }

    /******************** END: Methods to fetch dashboard data ********************/


    // Commenting out as of now to remove the Add Case Type functionality

    /**
     * Method to open add new case modal
     */
    // async openAddNewCaseModal(){
    //     this.modalTitle = this.ADD_CASE_TYPE_MODAL.TITLE;
    //     if(!this.caseTypeDataMyCase) this.getMyCaseCaseTypes()
    //     this.showAddNewCaseTypeModal = true;
    //     await this.openModal();  
    // }

    /**
     * Method to get all case_types
     * @memberof FormsDashboardComponent
     * @returns void
     */
    // getMyCaseCaseTypes(): void {
    //     this.isMyCaseCaseTypeLoading = true;
    //     this.caseTypesSubscription$ = this.dashboardService.getCaseTypes(RequestedSystemEnum.Mycase)
    //         .subscribe((response: FormEngineCaseTypesResponse) => {
    //             this.caseTypeDataMyCase = response.data && response.data.caseTypes ? response.data.caseTypes : [];
    //             this.isMyCaseCaseTypeLoading = false;
    //          },(err: ErrorEvent) => { 
    //             const errResponse = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
    //             this.showErrorToast(errResponse)
    //             this.isMyCaseCaseTypeLoading = false;
    //         })
    // }

    /**
     * Method to create new case type
     */
    // createNewCaseType(): void {
    //     const newCaseType = this.newCaseTypeName;
    //     newCaseType.popular=this.isPopular;
    //     if(!newCaseType) {
    //         this.errorMessage = this.ADD_CASE_TYPE_MODAL.CASE_TYPE_EMPTY_ERROR;
    //         return;
    //     }
    //     const isCaseTypeExists = this.caseTypeData.some(caseType => caseType.caseType.toLowerCase() === newCaseType.caseType.toLowerCase());
    //     if(isCaseTypeExists) {
    //         this.errorMessage = this.ADD_CASE_TYPE_MODAL.CASE_EXIST_ERROR;
    //     } 
    //     else {
    //         this.loader.showLoader();
    //         this.createCaseTypesSubscription$ = this.dashboardService.createCaseType(newCaseType)
    //         .subscribe(
    //         (response) => { 
    //             this.showSuccessToast(newCaseType?.description + " case type has been successfully added");
    //             this.navigateToLastPage = true;
    //             this.getInterviewCaseTypes(); 
    //             this.closeModal();
    //             this.loader.hideLoader();
    //             },
    //         (err: ErrorEvent) => { 
    //             const errResponse = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
    //             this.showErrorToast(errResponse)
    //             this.closeModal();
    //             this.loader.hideLoader();
    //         }
    //         );
    //     }
    // }


    /******************** START: Methods for Assign Tags Modal ********************/
    /**
     * Method to open the modal
     *
     * @return {Promise<void>}
     * @memberof FormsEngineDashboardComponent
     */
    async openModal(): Promise<void> {
        await this.modalComponent.open();
    }

    /**
     * Method to close the modal
     */
    closeModal(): void {
        this.errorMessage = '';
        this.clearModalInfo();
        this.modalComponent.close();

    }

    /**
     * method to remove the modal related information when the modal closes
     */
    clearModalInfo() {
        this.showAssignTagsModal = false;
        // Commenting out as of now to remove the Add Case Type functionality
        // this.showAddNewCaseTypeModal = false;
        // this.caseTypeSelect?.handleClearClick();
        // this.isPopular = false;
    }

    /**
     * Method to invoke fetchUpdatedFormsByStatus after tagModal closed.
     *
     * @param {boolean} [event=false]
     * @memberof FormsEngineDashboardComponent
     */
    onCloseAssignTagsModal(event: boolean = false) {
        this.showAssignTagsModal = false;
        if (this.isFetchDashboardFormDetails || event) {
            this.fetchUpdatedFormsByStatus();
        }
        this.isFetchDashboardFormDetails = false;
    }

    /**
     * Method to set the current tab in Assign tags modal
     * @param id Tab Nav Id
     */
    openTabByIdInAssignTagsModal(id) {
        this.assignTabCurrentTab = id;
    }

    fetchUpdatedFormsByStatus() {
        this.getInprogressAndNewForms();
        this.getPublishedForms();
    }

    openCreateGuidedInterviewPage(formdetails){
        let formValues = formdetails?.value;
        this.createGiFormErrors.showTitleError= false;
        this.loader.showLoader();
        this.createGiSubscription$ = this.formEngineGuidedService.createGuidedInterviewForm(formValues?.formName, formValues?.formDescription)
        .subscribe((response: FormEngineCreateGuidedFormsResponse) => { 
            this.loader.hideLoader();
            this.closeCreateGiModal();
            this.router.navigate([ FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW, response?.data?.formNumber]);
        },
        (err: ErrorEvent) => {
            this.loader.hideLoader();
            const errResponse = err?.error?.error?.message ? err.error.error.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
            const errorCode = err?.error?.error?.code;
            if (errorCode === this.GUIDED_INTERVIEW_CREATE_MODAL.TITLE_ERROR_CODE) {
                this.createGiFormErrors.showTitleError= true;
            }
            else{
                this.showErrorToast(errResponse, { code: err?.error?.error?.code })
            }
        })
    }


    /**
     * Method to open create content form modal
     */
    createContentFormStart() {
        this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.NAV_ID);
        this.formDetails = null;
        this.setContentFormAssignTagsModalProperties();
    }

    openCreateGiFormModal() {
        this.showCreateGiModal = true;
    }

    closeCreateGiModal(){
        this.showCreateGiModal = false;
    }

    /**
     * Method to open New file upload Modal
     */
    openUploadFormModal() {
        this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.NAV_ID);
        this.formDetails = null;
        this.setUploadFormAssignTagsModalProperties();
    }

    /**
     * Method to set the properties for Assign tags Modal for "Upload New Form" functionality
     */
    setUploadFormAssignTagsModalProperties() {
        this.showAssignTagsModal = true;
        this.isFormUploadRequired = true;
        this.showPublishTab = true;
        this.assignTagsModalHeader = this.ASSIGN_TAGS_MODAL.ASSIGN_HEADER;
        this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
    }

    /**
     * Method to set the properties for Assign tags Modal for "Create Content Form" functionality
     */
    setContentFormAssignTagsModalProperties() {
        this.showAssignTagsModal = true;
        this.isFormUploadRequired = false;
        this.showPublishTab = false;
        this.assignTagsModalHeader = this.ASSIGN_TAGS_MODAL.CREATE_CONTENT_FORM;
        this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM;
    }

    /******************** END: Methods for Assign Tags Modal ********************/


    /******************** START: TOAST ACTIONS ********************/
    /**
     * Method gets called by notificationService service on success
     * @param message 
     */
    showSuccessToast(message) {
        this.notificationService.showSuccess(message);
    }

    /**
     * Method gets called by notificationService service on error
     * @param message 
     */
    showErrorToast(message, code?) {
        this.notificationService.showError(message, code);
    }
    /******************** END: TOAST ACTIONS ********************/

    /******************** START: Form Engine Dashboard Redirections ********************/
    /**
     * Method to redirect to case type page
     * @param caseTypeId 
     */
    redirectToCaseType(caseTypeId: string): void {
        this.router.navigate([FRONT_END_ROUTES.FORMS_LIST_DASHBOARD], { queryParams: { 'case-type': caseTypeId } });
    }


    /**
     * Redirects to the forms page or opens assign tags modal with the corresponding document ID
     *
     * @param {FormEngineForm} currentRowData
     * @memberof FormsDashboardComponent
     */
    redirectToNextStep(currentRowData: FormEngineForm): void {
        if ((currentRowData?.type === FormEngineFormTypes.Form) && (currentRowData?.status === FormEngineFormStatus.New || currentRowData?.status == FormEngineFormStatus.InProgress)) {
            this.formDetails = currentRowData;
            this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
            this.setUploadFormAssignTagsModalProperties(); 
            this.isTagsSaved = true;
        }
        else if ((currentRowData?.type === FormEngineFormTypes.ContentForm) && (currentRowData?.status === FormEngineFormStatus.New)) {
            this.formDetails = currentRowData;
            this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
            this.setContentFormAssignTagsModalProperties();
            this.isTagsSaved = true;
        }
        else if ((currentRowData?.type === FormEngineFormTypes.ContentForm) && (currentRowData?.status === FormEngineFormStatus.InProgress || currentRowData?.status === FormEngineFormStatus.Published)) {
            this.router.navigate([ FRONT_END_ROUTES.FORMS,this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM, currentRowData.formNumber])
        }
        else if (currentRowData?.type === FormEngineFormTypes.GuidedInterview && (currentRowData?.status === FormEngineFormStatus.New || currentRowData?.status === FormEngineFormStatus.InProgress || currentRowData?.status === FormEngineFormStatus.Published)) {
            this.router.navigate([FRONT_END_ROUTES.FORMS,this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW , currentRowData.formId]);
        }
    }

    redirectToFormsPageWithStatus(status: string): void {
        const queryParams: any = {};
        let statusArray: Array<string>;
        if (status === FORM_ENGINE.STATUS.IN_PROGRESS.code)
            statusArray = [FORM_ENGINE.STATUS.NEW.code, FORM_ENGINE.STATUS.IN_PROGRESS.code];
        else if (status === FORM_ENGINE.STATUS.PUBLISHED.code)
            statusArray = [FORM_ENGINE.STATUS.PUBLISHED.code];
        queryParams.status = JSON.stringify(statusArray);
        const navigationExtras: NavigationExtras = {
            queryParams
        };
        this.router.navigate([FRONT_END_ROUTES.FORMS_LIST_DASHBOARD], navigationExtras);
    }

    /******************** END: Form Engine Dashboard Redirections ********************/

    /********* START: Template Creation Methods ***********/

    openTemplateModal() {
        this.createTemplateModalComponent.openTemplateModal();
    }

    closeTemplateModal(){
        this.createTemplateModalComponent.closeTemplateModal();
    }

    createTemplate(templateName: string) {
        this.createTemplateErrors.showTitleError= false;
        this.loader.showLoader();
        this.createTemplateSubscription$ = this.formEngineTemplateService.saveTemplate(templateName)
        .subscribe((response: FormEnginePostTemplateResponse) => { 
            this.loader.hideLoader();
            this.closeTemplateModal();
            this.router.navigate([ FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.TEMPLATE, response?.data?.id]);
        },
        (err: ErrorEvent) => {
            this.loader.hideLoader();
            const errResponse = err?.error?.error?.message ? err.error.error.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
            const errorCode = err?.error?.error?.code;
            if (errorCode === this.CREATE_TEMPLATE_MODAL.TITLE_ERROR_CODE) {
                this.createTemplateErrors.showTitleError= true;
            }
            else{
                this.closeTemplateModal();
                this.showErrorToast(errResponse, { code: err?.error?.error?.code })
            }
        })
    }

    /********* END: Template Creation Methods ***********/

    ngOnDestroy(): void {
        this.formEngineFormsSubscription$ ? this.formEngineFormsSubscription$.unsubscribe() : null;
        this.caseTypesSubscription$ ? this.caseTypesSubscription$?.unsubscribe() : null;
        this.createGiSubscription$ ? this.createGiSubscription$?.unsubscribe(): null;
        this.createGuidedInterviewModal?.closeModal();
        this.createTemplateModalComponent.closeTemplateModal();
        this.createTemplateSubscription$ && this.createTemplateSubscription$.unsubscribe();
        // this.createCaseTypesSubscription$ ? this.createCaseTypesSubscription$.unsubscribe() : null;
    }
}
