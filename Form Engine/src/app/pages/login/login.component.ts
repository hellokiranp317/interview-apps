import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ALERT_TYPES, ASSETS, COMMON, ERROR, LOGIN } from 'src/app/core/constants';
import { AlertService } from 'src/app/shared';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  readonly LOGIN = LOGIN;
  readonly ASSETS = ASSETS;
  readonly ERROR = ERROR.MESSAGE;
  readonly ALERT_TYPES = ALERT_TYPES;
  readonly COMMON = COMMON;

  returnUrl: string;
  errorMessage: string;
  skipLinkPath: string;

  constructor(private router: Router, private alertService: AlertService)
  {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras?.state as { message: string };
    state?.message ? this.showErrorAlert(state?.message) : null;
  }

  ngOnInit() {
    this.skipLinkPath = `${this.router.url}${COMMON.MAIN_CONTAINER}`;
  }

  /**
   * Show Error alert message
   * @returns null
   * @memberof LoginFormComponent
   */
  showErrorAlert(message: string): void {
    this.alertService.setAlertMessage({
      type: ALERT_TYPES.DANGER, message: message || ERROR.MESSAGE.SOMETHING_WENT_WRONG, dismissable: true 
    });
  }

  /**
   * Dismisses the alert
   *
   * @memberof LoginFormComponent
   */
  dismissAlert(): void {
    this.alertService.setAlertMessage(null);
  }

  ngOnDestroy(): void {
    this.dismissAlert();
  }
  
}
