import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { of } from 'rxjs';
import { Configuration } from 'src/app/client';
import { DashboardService, DynamicFormService } from 'src/app/core/services';
import { mockAPIConfiguration } from 'src/test/test.utils';
import mockFormsJson from 'src/test/data/formsResponse.json';
import mockFormComponentResponse from 'src/test/data/form.json';
import mockDynamicFormResponse from 'src/test/data/dynamicFormsResponse.json';

import { FormsComponent } from './forms.component';
import { ActivatedRoute } from '@angular/router';

describe('FormsComponent', () => {
  let component: FormsComponent;
  let fixture: ComponentFixture<FormsComponent>;

  // Mock Activated Route
  const mockActivatedRoute = {
    snapshot: {
      params: { id: 101 }
    }
  }

  let mockDashboardService = {
    getFormEngineFormsResponse: mockFormsJson,
    getForms: () => of(mockFormsJson)
  }

  let mockDynamicFormService = {
    getFormComponents: () => of(mockFormsJson),
    setBuilderOptions: () => {},
    builderOptions: of(mockFormsJson),
    formWizard: of(mockFormsJson),
    getDynamicForm: (id) => of(mockDynamicFormResponse),
    setFormWizard: () => {}
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsComponent ],
      imports: [ RouterTestingModule, HttpClientTestingModule ],
      providers: [ 
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute
        },
        {
          provide: Configuration,
          useFactory: mockAPIConfiguration,
          multi: false,
        },
        {
          provide: DashboardService,
          useValue: mockDashboardService
        },
        {
          provide: DynamicFormService,
          useValue: mockDynamicFormService
        }
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(mockDashboardService, 'getForms').and.returnValue(of(mockFormsJson))
    spyOn(mockDynamicFormService, 'getFormComponents').and.returnValue(of(mockFormComponentResponse))
    expect(component).toBeTruthy();
  });

  it('ngOnInit calls', () => {
    spyOn(component, 'fetchDashboardAndBuilderOptions').and.callThrough();
    spyOn(component, 'subscribeToBuilderOptions').and.callThrough();
    spyOn(component, 'subscribeToFormWizard').and.callThrough();
    component.ngOnInit();
    expect(component.fetchDashboardAndBuilderOptions).toHaveBeenCalled();
    expect(component.subscribeToBuilderOptions).toHaveBeenCalled();
    expect(component.subscribeToFormWizard).toHaveBeenCalled();
    expect(component.formId).toEqual(mockActivatedRoute.snapshot.params.id);
  });

  it('fetchDashboardAndBuilderOptions method fetches form data', () => {
    spyOn(component, 'fetchBuilderOptions').and.callThrough();
    spyOn(component, 'setBuilderOptions').and.callThrough();
    spyOn(component, 'setBuilderForm').and.callThrough();
    component.fetchDashboardAndBuilderOptions();
    expect(component.fetchBuilderOptions).toHaveBeenCalled();
    expect(component.formFieldProperties).toEqual(mockFormsJson.data);
    expect(component.setBuilderOptions).toHaveBeenCalled();
    expect(component.setBuilderForm).toHaveBeenCalled();
  })
});
