import { Location } from '@angular/common';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import {
  EditInterviewFormStatus,
  FormEngineCaseTypes, FormEngineContentFormMappingTypesForGIPayload, FormEngineContentFormMappingTypesResponse, FormEngineContentFormPayload, FormEngineContentFormPropertiesPayload, FormEngineContentFormPropertyResponse, FormEngineContentFormSaveRequest, FormEngineCreateGuidedFormsResponse, FormEngineFormComponentsPayload, FormEngineFormStatus, FormEngineGetGuidedFormPayload, FormEngineGetGuidedFormResponse, FormEngineGetTemplateByIdResponse, FormEngineGetTemplatePayload,
  FormEnginePostGuidedFormsResponse, FormIOComponentProperty, SuccessResponse
} from 'src/app/client';

import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { PublishInterviewModalComponent, TemplateInfoModalComponent } from 'src/app/components';
import { builderOptionsWithFormTemplate, defaultBuilderOptions } from 'src/app/components/form-io/builder/builder.options';
import { ALERT_TYPES, BUTTONS, ERROR, FORMS, FORM_ENGINE, FRONT_END_ROUTES, GUIDED_INTERVIEW, LOADER, MODAL } from 'src/app/core/constants';
import { FormEngineContentInterviewService, FormEngineFormsService, FormEngineGuidedInterviewService, FormEngineTemplatesService, LoadingService, NotificationService } from 'src/app/core/services';
import { ModalComponent } from 'src/app/shared';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit, OnDestroy {

  readonly LOADER = LOADER;
  readonly ALERT_TYPES = ALERT_TYPES;
  readonly ROUTES = FRONT_END_ROUTES;
  readonly FORM_ENGINE = FORM_ENGINE;
  readonly CONTENT_GENERATION_MODAL = FORM_ENGINE.CONTENT_GENERATION_MODAL;
  readonly FORMS_BUTTON = FORM_ENGINE.BUTTON;
  readonly BUTTONS = BUTTONS;
  readonly CANCEL_CONFIRMATION_MODAL = FORM_ENGINE.CANCEL_CONFIRMATION_MODAL;
  readonly FORMIO_ROUTE_FORM_TYPES = FORM_ENGINE.FORMIO_ROUTE_FORM_TYPES;
  readonly FORMS_PAGE_CONSTANTS = FORM_ENGINE.FORMS_PAGE_CONSTANTS;
  readonly MODAL_SIZE = MODAL.SIZE;
  readonly FORMIO_ACTIONS = FORM_ENGINE.FORMIO_ACTIONS;

  @ViewChild('publishInterviewModal') publishInterviewModal: PublishInterviewModalComponent;
  @ViewChild('contentGenerationModal', { static: true }) contentGenerationModal: ModalComponent;
  @ViewChild('contentFormPublishModal') contentFormPublishModal: ModalComponent;
  @ViewChild('formsContainer') formsContainer: ElementRef;
  @ViewChild('createTemplateModal') createTemplateModalComponent: TemplateInfoModalComponent;

  public isCollapsed = true;

  filledData: any = { data: {} };
  formSrc: any;
  formName: string;
  formId: number;
  formNumber: string;
  formFieldProperties: FormIOComponentProperty[];
  contentFormFieldProperties;
  templateFormProperties: FormEngineGetTemplatePayload;
  builderOptions: any = builderOptionsWithFormTemplate;
  transformedBuilderOptions: any;
  errorMessage: string = null;
  builderForm: any;
  createFormLoader: boolean = false;
  caseTypeData: FormEngineCaseTypes[];
  showConfirmationModal: boolean = false;
  formProperties: FormEngineFormComponentsPayload;
  publishInterviewDateFormData: any;
  contentGenModalNgbOptions: NgbModalOptions;
  wizardBtnGroup: any;
  formType: string = this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW;
  contentFormPublishFormData: any;
  contentFormProperties: FormEngineContentFormPropertiesPayload;
  guidedFormProperties: FormEngineGetGuidedFormPayload;

  modalFormTitle: string;
  today = (new Date()).toISOString().substring(0, 10);

  // Subscriptions
  formComponentsSubscription$: Subscription;
  builderOptionsSubscription$: Subscription;
  formWizardSubscription$: Subscription;
  createGuidedInterviewFormSubscription$: Subscription;
  getGuidedFormSubscription$: Subscription;
  getFormTemplateSubscription$: Subscription;
  formEngineFormsSubscription$: Subscription;
  caseTypesSubscription$: Subscription;
  storeContentFormSubscription$: Subscription;
  editContentFormSubscription$: Subscription;
  saveContentFormSubscription$: Subscription;
  getContentFormSubscription$: Subscription;
  saveGuidedInterviewPropertiesSubscription$: Subscription;
  templateComponentsSubscription$: Subscription;
  templateName: string = '';
  createTemplateSubscription$: Subscription;
  getTemplatesByIdSubscription$: Subscription;
  getGuidedInterviewFormSubscription$: Subscription;
  getCfMappingTypesSubscription$: Subscription;
  contentFormsList: FormEngineContentFormPayload[];
  cfMappingTypes: FormEngineContentFormMappingTypesForGIPayload[];
  contentFormsCount: number = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formEngineFormsService: FormEngineFormsService,
    private _location: Location,
    private notificationService: NotificationService,
    private formEngineContentInterviewService: FormEngineContentInterviewService,
    private formEngineTemplate: FormEngineTemplatesService,
    private formEngineGuidedInterviewService: FormEngineGuidedInterviewService,
    public loader: LoadingService
  ) { }

  ngOnInit(): void {
    // TODO: Remove FormID related code once Guided Interview retrieval are done
    // TODO: To Remove the Content Form Generate Modal realted code
    this.formId = parseInt(this.route.snapshot?.params[FORMS.ID]);
    this.formNumber = this.route.snapshot?.params[FORMS.ID];
    this.formType = this.route.snapshot?.params[FORMS.TYPE];
    if (this.formType === this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM) {
      this.setBuilderOptions();
      this.retrieveContentFormByFormNumber();
    } else if (this.formType === this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW) {
      this.setBuilderOptions();
      this.retrieveGuidedFormByFormNumber();
    }
    else if (this.formType === this.FORMIO_ROUTE_FORM_TYPES.TEMPLATE) {
      this.setBuilderOptions();
      this.retriveTemplateByTemplateNumber();
    }
  }

  setBuilderOptions() {
    this.subscribeToBuilderOptions();
    this.subscribeToFormWizard();
    this.formEngineFormsService.setBuilderOptions(defaultBuilderOptions);
  }

  updateTemplate() {
    this.loader.showLoader();
    this.createTemplateSubscription$ =
      this.formEngineTemplate.updateTemplate(this.templateFormProperties.templateId, JSON.stringify(this.formSrc))
        .subscribe(
          (response: SuccessResponse) => {
            this.loader.hideLoader();
            this.showSuccessToast(this.templateFormProperties.templateName + FORM_ENGINE.CREATE_TEMPLATE_SUCCESS);
            this.router.navigate([FRONT_END_ROUTES.FORMS_DASHBOARD]);
          },
          (err: ErrorEvent) => {
            const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
            this.showErrorToast(errResponse, { code: err?.error?.error?.code });
            this.loader.hideLoader();
          });
  }

  // /** Feature Guided Interview
  //  * Show the Content Generation Modal / Load Guided Interview Form Properties based on the form status
  //  * 
  //  * @memberof FormsComponent
  //  */
  // showContentGenerationModal(): void {

  //   if(this.formProperties?.formStatus === FormEngineFormStatus.New) {
  //     this.openContentGenerationModal();
  //   }
  //   else if(this.formProperties?.formStatus === FormEngineFormStatus.InProgress 
  //     || this.formProperties?.formStatus === FormEngineFormStatus.Published)
  //   {
  //     this.getGuidedFormSubscription$ = this.formEngineFormsService.getGuidedInterviewForm(this.formId, EditInterviewFormStatus.InProgress)
  //     .subscribe(
  //       (response) => { 
  //         this.errorMessage = null;
  //         if(response.data.formComponentProperties) {
  //           const formComponentPropertiesParsed = JSON.parse(response.data.formComponentProperties);
  //           this.builderForm = formComponentPropertiesParsed; 
  //           this.formEngineFormsService.setFormWizard(formComponentPropertiesParsed);
  //           this.setWizardButtonText(formComponentPropertiesParsed);
  //         }
  //       },
  //       (err: ErrorEvent) => { 
  //         this.errorMessage = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
  //       }
  //     );
  //   } else {
  //     this.errorMessage = ERROR.MESSAGE.SOMETHING_WENT_WRONG;
  //   }
  // }

  /**
   * Generate the Draft Properties and set it as initial form template
   *
   * @memberof FormsComponent
   */
  generateDraftAndSetFormTemplate(): void {
    this.builderForm = null;
    this.getFormTemplateSubscription$ = this.formEngineFormsService.getFormTemplate(this.formId)
      .subscribe(
        (response) => {
          this.errorMessage = null;
          if (response.data.components) {
            this.builderForm = response.data;
            this.formEngineFormsService.setFormWizard(response.data);
          }
        },
        (err: ErrorEvent) => {
          const errorMessage: string = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
          this.setInitialBuilderForm();
        }
      );
  }

  /**
   * Initialize Builder Form
   *
   * @memberof FormsComponent
   */
  setInitialBuilderForm(): void {
    this.builderForm = {
      title: this.guidedFormProperties.formTitle,
      type: GUIDED_INTERVIEW.FORM,
      display: GUIDED_INTERVIEW.WIZARD,
      components: [],
      name: this.guidedFormProperties.formTitle,
    };
  }

  /**
   * Initialize Builder Form for template
   *
   * @memberof FormsComponent
   */
  setInitialTemplateBuilderForm(): void {
    this.builderForm = {
      type: GUIDED_INTERVIEW.FORM,
      display: GUIDED_INTERVIEW.FORM,
      components: []
    };
  }

  /**
   * Subscription to the builder options Observable
   *
   * @memberof FormsComponent
   */
  subscribeToBuilderOptions(): void {
    this.builderOptionsSubscription$ = this.formEngineFormsService.builderOptions.subscribe((value: any) => {
      this.transformedBuilderOptions = value ? value : null;
    })
  }

  /**
   * Subscription to the Form Wizard Observable
   *
   * @memberof FormsComponent
   */
  subscribeToFormWizard(): void {
    this.formWizardSubscription$ = this.formEngineFormsService.formWizard.subscribe((value: any) => {
      this.formSrc = value ? value : null;
    })
  }

  // /** Feature Get guided Interview
  //  * Fetches the builder options and sets the value to an BehaviorSubject
  //  * 
  //  * @memberof FormsComponent
  //  */
  // getCustomFormComponents(): void {
  //   this.formComponentsSubscription$ = this.formEngineFormsService.getFormComponents(this.formId)
  //     .subscribe(
  //       (response: FormEngineFormComponentsResponse) => {
  //         this.builderForm = null;
  //         this.errorMessage = null;
  //         this.formProperties = response.data;
  //         this.formFieldProperties = response.data?.formComponentProperties;
  //         this.setInitialBuilderOptions();
  //         this.showContentGenerationModal();
  //       },
  //       (err: ErrorEvent) => { 
  //         this.errorMessage = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
  //       }
  //     );
  //     this.templateComponentsSubscription$ = this.formEngineContentInterviewService.retriveTemplate()
  //     .subscribe((response: FormEngineGetTemplateResponse) => {
  //       this.templateFormProperties = response.data;
  //       this.setTemplateBuilderOptions();
  //     },
  //     (err: ErrorEvent) => { 
  //       this.errorMessage = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
  //     }
  //     )
  //   }

  // /**
  //  * Transforms the builder options
  //  *
  //  * @memberof FormsComponent
  //  */
  // setTemplateBuilderOptions(): void {
  //   this.builderOptions.builder.template.components = [];
  //   this.templateFormProperties.forEach((temp) => {
  //     let templateCompProperties = JSON.parse(temp.templateComponentProperties);
  //     templateCompProperties.key = templateCompProperties.referenceNumber;
  //     templateCompProperties.title = temp.templateName;
  //     templateCompProperties.schema = {
  //       type: FORM_ENGINE.SECTION,
  //       input: false,
  //       name: temp.templateName,
  //       key: templateCompProperties.referenceNumber,
  //       referenceNumber: templateCompProperties.referenceNumber,
  //       referenceType: templateCompProperties.referenceType,
  //       persistent: false,
  //       components: templateCompProperties.components,
  //       customClass: 'template-section'
  //     }
  //     delete templateCompProperties.display;
  //     delete templateCompProperties.components;

  //     this.builderOptions.builder.template.components[temp.templateName] = templateCompProperties;
  //   });
  //   this.formEngineFormsService.setBuilderOptions(this.builderOptions);
  // }

  /**
   * Transforms the builder options
   *
   * @memberof FormsComponent
   */
  setInitialBuilderOptions(): void {
    this.builderOptions.builder.formTemplateFields.components = [];
    this.formFieldProperties.forEach((eachValue: FormIOComponentProperty) => {
      this.builderOptions.builder.formTemplateFields.components[eachValue.key] = eachValue;
    });
    this.formEngineFormsService.setBuilderOptions(this.builderOptions);
  }

  setWizardButtonText(formProperties) {
    this.wizardBtnGroup = {
      nextBtn: formProperties?.components[0]?.nextTextField,
      prevBtn: formProperties?.components[0]?.previousTextField,
      cancelBtn: formProperties?.components[0]?.cancelTextField,
      submitBtn: formProperties?.components[0]?.submitTextField
    };
  }

  publishInterviewDateForm(formData) {
    this.publishInterviewDateFormData = formData.value;
    this.saveGuidedInterviewProperties();
    this.publishInterviewModal.closeModal();
  }

  openPublishInterviewModal() {
    if (this.formType === this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM) {
      this.showContentFormPublishModal();
    }
    else {
      this.getCFMappingDataForGuidedInterview();
      this.publishInterviewModal?.openModal();
    }
  }

  getCFMappingDataForGuidedInterview() {
    let canSendMappingType: boolean = true;
    this.getCfMappingTypesSubscription$ = this.formEngineGuidedInterviewService.getCFMappingTypesForGuidedInterview(canSendMappingType, this.formNumber)
      .subscribe(
        (response: FormEngineContentFormMappingTypesResponse) => {
          this.errorMessage = null;
          this.contentFormsList = response.data?.contentForms;
          this.cfMappingTypes = response.data?.formTypes;
        },
        (err: ErrorEvent) => {
          const errorMessage: string = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(errorMessage, { code: err?.error?.error?.code });
        }
      );
  }





  /**
   * Creates a new dynamic form and stores the JSON in a file
   *
   * @memberof FormsComponent
   */
  saveGuidedInterviewProperties(): void {
    if (!this.formSrc) { return; }
    this.createFormLoader = true;
    const formComponentProperties: string = JSON.stringify(this.formSrc);
    const navigationExtras: NavigationExtras = {
      state: { formSubmitted: true, formName: this.builderForm?.title }
    }
    let effectiveDateWithTime;
    effectiveDateWithTime = (this.publishInterviewDateFormData?.date === (new Date()).toISOString().substring(0, 10)) ?
      new Date().toISOString() :
      new Date(this.publishInterviewDateFormData?.date).toISOString();

    this.saveGuidedInterviewPropertiesSubscription$ = this.formEngineGuidedInterviewService.saveGuidedInterviewProperties(this.formNumber, formComponentProperties, FormEngineFormStatus.Published, effectiveDateWithTime, this.publishInterviewDateFormData.comments, this.publishInterviewDateFormData.contentFormsAndMappingType)
      .subscribe(
        (response: FormEngineCreateGuidedFormsResponse) => {
          this.showSuccessToast(response?.data?.formName + FORM_ENGINE.PUBLISH_FORM_SUCCESS_MESSAGE);
          this.router.navigate([FRONT_END_ROUTES.FORMS_DASHBOARD]);
        },
        (err: ErrorEvent) => {
          this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
          this.createFormLoader = false;
        }
      );
  }

  saveAsDraft() {
    if (!this.formSrc) { return; }
    if (this.formType === this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM) {
      !this.contentFormProperties.contentFormAlreadyCreated ? this.saveContentForm(FormEngineFormStatus.InProgress, this.FORMIO_ACTIONS.SAVE_DRAFT) : this.editContentForm(FormEngineFormStatus.InProgress, this.FORMIO_ACTIONS.SAVE_DRAFT);
    }
    // Feature - below lines can be refactored in GI safe as draft flow
    // else {
    //   this.saveGuidedFormAsDraft();
    // }
  }

  // Feature - can be refactored in GI safe as draft flow
  // saveGuidedFormAsDraft() {
  //   const effectiveDate = '';
  //   const comments = '';
  //   this.createFormLoader = true;
  //   const formComponentProperties: string = JSON.stringify(this.formSrc);

  //   this.createGuidedInterviewFormSubscription$ = this.formEngineFormsService.createGuidedInterviewForm(formComponentProperties, this.formProperties.formNumber, FormEngineFormStatus.InProgress, effectiveDate, comments)
  //     .subscribe(
  //       (response) => { 
  //         this.showSuccessToast(this.builderForm?.title + FORM_ENGINE.SAVE_FORM_SUCCESS_MESSAGE);
  //         this.createFormLoader = false; 
  //         const queryParams: any = {};
  //         let inProgressStatusArray: Array<string> = [FORM_ENGINE.STATUS.NEW.code, FORM_ENGINE.STATUS.IN_PROGRESS.code];
  //         queryParams.status = JSON.stringify(inProgressStatusArray);
  //         const navigationExtras: NavigationExtras = {
  //           queryParams
  //         };
  //         this.router.navigate([ FRONT_END_ROUTES.FORMS_LIST_DASHBOARD ], navigationExtras);
  //       },
  //       (err: ErrorEvent) => { 
  //         this.errorMessage = err?.error?.error?.message ?  err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG; 
  //         this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
  //         this.createFormLoader = false;
  //       }
  //     );
  // }

  /**************** START: Content Form Methods  ****************/

  /**
  * Initialize Builder Form
  *
  * @memberof FormsComponent
  */
  setInitialBuilderContentForm(): void {
    this.builderForm = {
      formNumber: this.contentFormProperties?.formNumber,
      title: this.contentFormProperties?.formTitle,
      type: GUIDED_INTERVIEW.FORM,
      display: GUIDED_INTERVIEW.FORM,
      components: [],
      name: this.contentFormProperties?.formTitle,
    };
  }

  setInitialBuilderOptionsContentForm(): void {
    this.builderOptions.builder.formTemplateFields.components = [];
    this.contentFormFieldProperties.forEach((eachValue: FormIOComponentProperty) => {
      this.builderOptions.builder.formTemplateFields.components[eachValue.key] = eachValue;
    });
    this.formEngineFormsService.setBuilderOptions(this.builderOptions);
  }

  /**
   * Retrieve content form from Form Number
   */
  retrieveContentFormByFormNumber() {
    this.storeContentFormSubscription$ = this.formEngineContentInterviewService.retrieveContentFormByFormNumber(this.formNumber).subscribe(
      (response: FormEngineContentFormPropertyResponse) => {
        this.contentFormProperties = response.data;
        const formComponentProperties = response.data.formComponentProperties ? JSON.parse(response.data.formComponentProperties) : '';
        const formTemplate = response.data.formTemplate ? JSON.parse(response.data.formTemplate) : '';
        if (formTemplate && Object.keys(formTemplate).length > 0) {
          this.contentFormFieldProperties = JSON.parse(response.data.formTemplate);
          this.setInitialBuilderOptionsContentForm();
        }
        if (formComponentProperties && Object.keys(formComponentProperties).length > 0) {
          this.builderForm = JSON.parse(response.data.formComponentProperties);
          this.formEngineFormsService.setFormWizard(this.builderForm);
        } else {
          this.setInitialBuilderContentForm();
          this.formEngineFormsService.setFormWizard(this.builderForm);
        }
      },
      (err: ErrorEvent) => {
        this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
      }
    );
  }

  /**
   * Retrieve Guided Interview form from Form Number
   */
  retrieveGuidedFormByFormNumber() {
    this.getGuidedInterviewFormSubscription$ = this.formEngineGuidedInterviewService.getGuidedInterviewForm(this.formNumber, EditInterviewFormStatus.InProgress).subscribe(
      (response: FormEngineGetGuidedFormResponse) => {
        this.guidedFormProperties = response.data;
        const formComponentProperties = response.data.formComponentProperties ? JSON.parse(response.data.formComponentProperties) : '';
        if (formComponentProperties && Object.keys(formComponentProperties).length > 0) {
          this.builderForm = JSON.parse(response.data.formComponentProperties);
          this.formEngineFormsService.setFormWizard(this.builderForm);
        } else {
          this.setInitialBuilderForm();
          this.formEngineFormsService.setFormWizard(this.builderForm);
        }
      },
      (err: ErrorEvent) => {
        this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
      }
    );
  }

  showContentFormPublishModal() {
    this.contentFormPublishModal.open();
  }

  closeContentFormPublishModal() {
    this.contentFormPublishModal.close();
  }

  onContentFormPublishFormChanges(event) {
    this.contentFormPublishFormData = event;
  }

  /**
   * Method to get the HTML string
   * Use this method to style the PDF
   * @returns string
   */
  getTemplateHTML() {
    let headTags = '<html> <head> <title>Formio HTML</title> <style> * { margin: 0; padding: 0; box-sizing: border-box; font-size: 14px; } body { padding: 30px; } .card-header { display: none; } .formio-component-textfield>div[ref="element"], .formio-component-select>div, .formio-component-textarea>div, .formio-component-datetime>div { width: 300px; border: 0; border-bottom: 1px solid #000; margin-top: 10px; } .formio-component-textfield.full-width-input>div[ref="element"] { width: 100%; } .formio-component-textfield.left-aligned-label { display: flex; } .formio-component-textfield.left-aligned-label .field-content { border-bottom: 1px solid #000; } .formio-component-textarea>label { margin-bottom: 25px; } .formio-component-textarea>div { width: 75%; } label { display: block; margin-top: 8px; margin-bottom: 8px; } .formio-component { margin-bottom: 20px; } .formio-errors { display: none; } .true-checkbox { display: inline-block; transform: rotate(45deg); height: 20px; width: 10px; margin-right: 10px; border-bottom: 3px solid black; border-right: 3px solid black; } .false-checkbox { display: inline-block; width: 20px; height: 20px; display: inline-block; margin-right: 10px; border: 2px solid; } .formio-component-checkbox div, .formio-component-checkbox label { display: inline-block; } .formio-component-checkbox div[ref="value"] { display: none; } .formio-component-columns { display: flex; gap: 20px; } .formio-component-columns.with-column-separate .col-md-6:first-child { border-right: 1px solid; padding-right: 30px; } .formio-component-textfield input { width: 100%; border: 0; } .col-md-6 { margin-right: 30px; } </style> </head> <body>'
    let endTags = ' </body></html>'
    let node = document.getElementById('formio-html').getElementsByClassName('formio-form')[0];
    let tmpNode = document.createElement("div");
    tmpNode.appendChild(node.cloneNode(true));
    let str = tmpNode.innerHTML;
    tmpNode = node = null;
    const contentFormHTMLString = headTags + str + endTags;
    return contentFormHTMLString;
  }

  /**
   * Method to construct content form save request
   * @param status 
   * @returns FormEngineContentFormSaveRequest
   */
  constructContentFormSaveRequest(status): FormEngineContentFormSaveRequest {
    let effectiveDate = '';
    let contentFormHTMLString = '';
    let comments = '';
    let basicPdf = false;
    let fillableForm = false;
    const publishContentFormValues = this.contentFormPublishFormData.getRawValue();
    if (status === FormEngineFormStatus.Published) {
      effectiveDate = (publishContentFormValues?.effectiveDate === (new Date()).toISOString().substring(0, 10)) ?
        new Date().toISOString() :
        new Date(publishContentFormValues?.effectiveDate)?.toISOString();
      contentFormHTMLString = this.getTemplateHTML();
      comments = publishContentFormValues.comments;
      basicPdf = publishContentFormValues.basicPdf;
      fillableForm = publishContentFormValues.fillableForm;
    }
    const formEngineStoreContentFormRequest: FormEngineContentFormSaveRequest = {
      formNumber: this.formNumber,
      html: contentFormHTMLString,
      formComponentProperties: JSON.stringify(this.formSrc),
      effectiveDate: (basicPdf || fillableForm) ? effectiveDate : null,
      formStatus: status,
      comments: comments,
      publishTags: {
        basicPdf: basicPdf,
        fillableForm: fillableForm
      }
    }
    return formEngineStoreContentFormRequest;
  }

  onClickPublishContentForm() {
    if (this.contentFormProperties.contentFormAlreadyCreated) {
      this.editContentForm(FormEngineFormStatus.Published, this.FORMIO_ACTIONS.PUBLISH)
    } else {
      this.saveContentForm(FormEngineFormStatus.Published, this.FORMIO_ACTIONS.PUBLISH)
    }
  }

  /**
   * Method to publish content form
   */
  saveContentForm(status = FormEngineFormStatus.Published, action: string) {
    this.loader.showLoader();
    const requestData = this.constructContentFormSaveRequest(status);
    this.storeContentFormSubscription$ = this.formEngineContentInterviewService.saveContentForm(requestData).subscribe(
      (response: FormEnginePostGuidedFormsResponse) => {
        this.closeContentFormPublishModal();
        this.loader.hideLoader();
        if (action === this.FORMIO_ACTIONS.PUBLISH) {
          this.router.navigate([FRONT_END_ROUTES.FORMS_LIST_DASHBOARD], { queryParams: { 'case-type': response.caseTypes.caseTypeId } });
          this.showSuccessToast(this.builderForm.title + this.FORMS_PAGE_CONSTANTS.CONTENT_FORM.PUBLISH_SUCCESS);
        } else {
          this.router.navigate([FRONT_END_ROUTES.FORMS_DASHBOARD]);
          this.showSuccessToast(this.builderForm.title + this.FORMS_PAGE_CONSTANTS.CONTENT_FORM.SAVE_SUCCESS);
        }
      },
      (err: ErrorEvent) => {
        this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.closeContentFormPublishModal();
        this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
        this.loader.hideLoader();
      }
    );
  }

  /**
   * Method to publish edited content form
   */
  editContentForm(status = FormEngineFormStatus.Published, action = this.FORMIO_ACTIONS.SAVE_DRAFT) {
    this.loader.showLoader();
    const requestData = this.constructContentFormSaveRequest(status);
    this.editContentFormSubscription$ = this.formEngineContentInterviewService.editContentForm(requestData).subscribe(
      (response: FormEnginePostGuidedFormsResponse) => {
        this.closeContentFormPublishModal();
        this.showSuccessToast(this.builderForm.title + this.FORMS_PAGE_CONSTANTS.CONTENT_FORM.EDIT_SUCCESS);
        this.loader.hideLoader();
        if (action === this.FORMIO_ACTIONS.PUBLISH)
          this.router.navigate([FRONT_END_ROUTES.FORMS_LIST_DASHBOARD], { queryParams: { 'case-type': response.caseTypes.caseTypeId } });
      },
      (err: ErrorEvent) => {
        this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.closeContentFormPublishModal();
        this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
        this.loader.hideLoader();
      }
    );
  }

  /**************** END: Content Form Methods  ****************/

  /*********** START: TOAST ACTIONS ***********/
  /**
   * Method gets called by notificationService service on success
   * @param message 
   */
  showSuccessToast(message) {
    this.notificationService.showSuccess(message);
  }

  /**
   * Method gets called by notificationService service on error
   * @param message 
   */
  showErrorToast(message, code?) {
    this.notificationService.showError(message, code);
  }
  /*********** END: TOAST ACTIONS ***********/

  /**
   * Opens up the confirmation modal
   *
   * @memberof FormsComponent
   */
  openConfirmationModal(): void {
    this.showConfirmationModal = true;
  }

  /**
   * Closes up the confirmation modal
   *
   * @memberof FormsComponent
   */
  closeConfirmationModal(): void {
    this.showConfirmationModal = false;
  }

  onCancelModal() {
    this._location.back();
  }

  /******* START: Content Generation Modal  ********/

  /**
   * Initialize the Content Generation Modal NGB Options
   *
   * @memberof FormsComponent
   */
  setContentGenerationModalOptions(): void {
    this.contentGenModalNgbOptions = {
      backdrop: 'static',
      keyboard: false,
      beforeDismiss: () => {
        return false;
      }
    };
  }

  /**
   * Opens the content generation modal
   *
   * @memberof FormsComponent
   */
  openContentGenerationModal(): void {
    this.setInitialBuilderForm();
    this.contentGenerationModal.open();
  }

  /**
   * Performs the appropriate operations based on the content generation modal action
   *
   * @param {string} formBuilderOptionType
   * @memberof FormsComponent
   */
  contentGenerationModalAction(formBuilderOptionType: string): void {
    if (formBuilderOptionType === this.CONTENT_GENERATION_MODAL.TYPE.DRAFT) {
      this.generateDraftAndSetFormTemplate();
    } else {
      this.setInitialBuilderForm()
    }
    this.contentGenerationModal.close();
  }

  /**
   * Retrieve template
   */
  retriveTemplateByTemplateNumber() {
    this.getTemplatesByIdSubscription$ = this.formEngineTemplate.getTemplateById(this.formNumber).subscribe(
      (response: FormEngineGetTemplateByIdResponse) => {
        this.templateFormProperties = response.data;
        const formComponentProperties = response.data.templateComponentProperties ? JSON.parse(response.data.templateComponentProperties) : '';
        if (formComponentProperties && Object.keys(formComponentProperties).length > 0) {
          this.builderForm = JSON.parse(response.data.templateComponentProperties);
          this.formEngineFormsService.setFormWizard(this.builderForm);
        } else {
          this.setInitialTemplateBuilderForm();
          this.formEngineFormsService.setFormWizard(this.builderForm);
        }
      },
      (err: ErrorEvent) => {
        this.errorMessage = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.showErrorToast(this.errorMessage, { code: err?.error?.error?.code });
      }
    );
  }

  /******* END: Content Generation Modal  ********/

  ngOnDestroy(): void {
    this.formComponentsSubscription$ && this.formComponentsSubscription$.unsubscribe();
    this.builderOptionsSubscription$ && this.builderOptionsSubscription$.unsubscribe();
    this.formWizardSubscription$ && this.formWizardSubscription$.unsubscribe();
    this.createGuidedInterviewFormSubscription$ && this.createGuidedInterviewFormSubscription$.unsubscribe();
    this.getGuidedFormSubscription$ && this.getGuidedFormSubscription$.unsubscribe();
    this.getFormTemplateSubscription$ && this.getFormTemplateSubscription$.unsubscribe();
    this.formEngineFormsSubscription$ && this.formEngineFormsSubscription$.unsubscribe();
    this.storeContentFormSubscription$ && this.storeContentFormSubscription$.unsubscribe();
    this.editContentFormSubscription$ && this.editContentFormSubscription$.unsubscribe();
    this.saveContentFormSubscription$ && this.saveContentFormSubscription$.unsubscribe();
    this.getContentFormSubscription$ && this.getContentFormSubscription$.unsubscribe();
    this.saveGuidedInterviewPropertiesSubscription$ && this.saveGuidedInterviewPropertiesSubscription$.unsubscribe();
    this.templateComponentsSubscription$ && this.templateComponentsSubscription$.unsubscribe();
    this.getTemplatesByIdSubscription$ && this.getTemplatesByIdSubscription$.unsubscribe();
    this.getGuidedInterviewFormSubscription$ && this.getGuidedInterviewFormSubscription$.unsubscribe();
    this.getCfMappingTypesSubscription$ && this.getCfMappingTypesSubscription$.unsubscribe();
    this.formEngineFormsService.setFormWizard(null);
    this.contentGenerationModal?.close();
    this.publishInterviewModal?.closeModal();
    this.closeConfirmationModal();
    this.createTemplateModalComponent?.closeTemplateModal();
  }

}
