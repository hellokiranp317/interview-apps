import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { FormEngineCaseTypeRetrievalPayload, FormEngineForm, FormEngineFormsByCaseTypeResponse, FormEngineFormsResponse, FormEngineFormStatus, FormEngineFormTypes, FormEngineGetFormStatus } from 'src/app/client';
import { ALERT_TYPES, DASHBOARD, DATA_TABLE, ERROR, FORM_ENGINE, FRONT_END_ROUTES, LOADER } from 'src/app/core/constants';
import { DropdownFilterdata, FilterData, SortOrder, TableModalEmitter } from 'src/app/core/models';
import { BreadCrumb } from 'src/app/core/models/Breadcrumb';
import { DashboardService, LoadingService, NotificationService } from 'src/app/core/services';
import { ModalComponent } from 'src/app/shared';


@Component({
  selector: 'app-forms-dashboard',
  templateUrl: './forms-dashboard.component.html',
  styleUrls: ['./forms-dashboard.component.scss']
})
export class FormsDashboardComponent implements OnInit {

  @ViewChild('staticAlert') staticAlert: NgbAlert;
  // @ViewChild('auditHistoryModal') auditHistoryModalComponent: ModalComponent;
  // @ViewChild('duplicateFormModal') duplicateFormModalComponent: ModalComponent;

  readonly LOADER = LOADER;
  readonly DASHBOARD = DASHBOARD;
  readonly ALERT_TYPES = ALERT_TYPES;
  readonly FORM_EMPTY_DATA_MESSAGE = FORM_ENGINE.EMPTY_FORMS_DATA;
  readonly CASE_TYPE_FORMS_TABLE = FORM_ENGINE.TABLE_TYPES.CASE_TYPE_FORMS;
  readonly IN_PROGRESS_FORMS_TABLE = FORM_ENGINE.TABLE_TYPES.IN_PROGRESS_FORMS;
  readonly FORM_MODAL_TYPES = FORM_ENGINE.MODAL_TYPES;
  readonly AUDIT_HISTORY_MODAL = FORM_ENGINE.AUDIT_HISTORY_MODAL;
  readonly DUPLICATE_FORM_MODAL = FORM_ENGINE.DUPLICATE_FORM_MODAL;
  readonly ASSIGN_TAGS_MODAL = FORM_ENGINE.ASSIGN_TAGS_MODAL;
  readonly FORMIO_ROUTE_FORM_TYPES = FORM_ENGINE.FORMIO_ROUTE_FORM_TYPES;

  FormEngineFormsSubscription$: Subscription;
  // DuplicateFormSubscription$: Subscription;

  breadcrumbs: Array<BreadCrumb> = [{ url: FRONT_END_ROUTES.FORMS_DASHBOARD, label: 'Dashboard' }]
  formsData: FormEngineForm[];
  dropDownFilterData: DropdownFilterdata = {
    status: new Set()
  };
  filterData: FilterData = {
    formName: "",
    caseType: "",
    status: "",
    statusDate: "",
    roles: "",
    rules: ""
  }
  sortOrder: SortOrder[] = [DATA_TABLE.SORT.DATE_DESC];
  newFormCreated: any;
  selectedForm: FormEngineCaseTypeRetrievalPayload;
  openedModal: string;
  // duplicateFormName = new FormControl('');
  // duplicateFormNameErrorMessage: string = '';
  caseTypeId: string;
  statusArray: string;
  formsListTypeName: string;
  formsStatusesQuery: FormEngineGetFormStatus[] = [];
  columnsType: string = this.CASE_TYPE_FORMS_TABLE;
  showAssignTagsModal: boolean = false;
  assignTabCurrentTab: number;
  formDetails: any = null;
  isTagsSaved: boolean = false;
  isTagsEdited: boolean = false;
  assignTagsModalHeader: string = this.ASSIGN_TAGS_MODAL.ASSIGN_HEADER;
  assignTagsType: string = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
  showPublishTab: boolean = true;
  isFormUploadRequired: boolean = true;
  isFetchDashboardFormDetails: boolean = false;
  isLoading: boolean = false;
  hasFormUpload: boolean = true;

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    public loader: LoadingService,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute) {
    this.newFormCreated = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit(): void {
    this.caseTypeId = this.activatedRoute.snapshot.queryParamMap.get('case-type');
    this.statusArray = this.activatedRoute.snapshot.queryParamMap.get('status');
    const constructBreadcrumb: boolean = true;
    this.fetchDashboardDetails(constructBreadcrumb);
  }

  resetDashboard() {
    this.formsData = [];
  }

  fetchDashboardDetails(constructBreadcrumb = false) {
    this.resetDashboard();
    if (this.caseTypeId) {
      this.getFormsByCaseType(constructBreadcrumb);
    }
    else {
      if (this.statusArray && !(this.formsStatusesQuery && this.formsStatusesQuery.length > 0)) {
        this.formsStatusesQuery = [];
        JSON.parse(this.statusArray)?.map((status) => {
          Object.keys(FORM_ENGINE.STATUS).forEach((s) => {
            if (FORM_ENGINE.STATUS[s].code === status) this.formsStatusesQuery.push(FORM_ENGINE.STATUS[s].API_CODE)
          })
        });
      }
      this.getForms(constructBreadcrumb);
    }
  }

  /**
   * function to get the forms details
   * @memberof FormsDashboardComponent
   * @returns void
   */
  getForms(constructBreadcrumb: boolean = false): void {
    this.isLoading = true;
    this.FormEngineFormsSubscription$ = this.dashboardService.getForms(this.formsStatusesQuery, [FormEngineFormTypes.ContentForm, FormEngineFormTypes.Form, FormEngineFormTypes.GuidedInterview])
      .subscribe((response: FormEngineFormsResponse) => { 
        this.isLoading = false;
        this.setFormEngineFormsResponse(response, constructBreadcrumb); 
      },
      (err) => {
        const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
        this.showErrorToast(errResponse, { code: err?.error?.error?.code })
        this.isLoading = false;
      })
  }

  /**
   * Method to fetch forms based on case type if
   * @memberof FormsDashboardComponent
   * @returns void
   */
  getFormsByCaseType(constructBreadcrumb: boolean = false): void {
      this.isLoading = true;
      const caseTypeIdValue = (this.caseTypeId === FORM_ENGINE.All_FORMS_ID) ? null : parseInt(this.caseTypeId);
      if (this.caseTypeId === FORM_ENGINE.All_FORMS_ID) {
        this.columnsType = FORM_ENGINE.TABLE_TYPES.ALL_FORMS;
      }
      this.FormEngineFormsSubscription$ = this.dashboardService.getFormsByCaseType(caseTypeIdValue)
        .subscribe((response: FormEngineFormsByCaseTypeResponse) => {
          this.setFormEngineFormsByCaseTypeResponse(response, constructBreadcrumb);
          this.isLoading = false;
        },
        (err) => {
          const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
          this.showErrorToast(errResponse, { code: err?.error?.error?.code })
          this.isLoading = false;
        }
        );
      this.sortOrder = [DATA_TABLE.SORT.GI_DATE_DESC];
  }

  /**
   * Setter for  FormEngineFormsResponse
   *
   * @param {FormEngineFormsResponse} response
   * @memberof FormsDashboardComponent
   */
  setFormEngineFormsResponse(response: FormEngineFormsResponse, constructBreadcrumb = false): void {
    let formLabel = '';
    if (response.success) {
      this.formsData = response.data ? response.data : [];
      this.formsData ? this.createDropdownFilterList() : null;
      if (this.formsStatusesQuery?.length > 0) {
        
        this.formsStatusesQuery.forEach((status) => {
          if (status.toLowerCase() === FormEngineFormStatus.InProgress.toLowerCase())
            formLabel = FORM_ENGINE.STATUS.IN_PROGRESS.text
          else if (status.toLowerCase() === FormEngineFormStatus.Published.toLowerCase())
            formLabel = FORM_ENGINE.STATUS.PUBLISHED.text
        })
        constructBreadcrumb && this.breadcrumbs.push({ url: '', label: formLabel + ' ' + DASHBOARD.FORMS });
        this.formsListTypeName = formLabel + ' ' + DASHBOARD.FORMS;
      }
      else constructBreadcrumb && this.breadcrumbs.push({ url: '', label: DASHBOARD.FORMS })
    }
    this.columnsType = (formLabel === FORM_ENGINE.STATUS.IN_PROGRESS.text) ? this.IN_PROGRESS_FORMS_TABLE : this.CASE_TYPE_FORMS_TABLE;
  }

  /**
   * Setter for FormEngineFormsByCaseTypeResponse
   *
   * @param {FormEngineFormsByCaseTypeResponse} response
   * @memberof FormsDashboardComponent
   */
  setFormEngineFormsByCaseTypeResponse(response: FormEngineFormsByCaseTypeResponse, constructBreadcrumb: boolean = false): void {
    if (response.success) {
      this.formsData = response.data && response.data.forms ? response.data.forms : [];
      this.formsListTypeName = response.data && response.data.caseTypeDescription ? response.data.caseTypeDescription : DASHBOARD.HEADER;
      this.formsListTypeName = this.formsListTypeName + ' ' + (this.formsListTypeName.split(" ").splice(-1)[0].toLowerCase() === DASHBOARD.FORMS.toLowerCase() ? '' : DASHBOARD.FORMS);
      constructBreadcrumb && this.breadcrumbs.push({ url: '', label: this.formsListTypeName })
      this.formsData ? this.createDropdownFilterList() : null;
      this.fetchFormCreationStatus();
    }
  }

  /**
   * Fetch the newly created form status from router
   *
   * @memberof FormsDashboardComponent
   */
  fetchFormCreationStatus(): void {
    this.newFormCreated ? this.dismissAlert(5000) : null;
  }

  /**
   * function for populating filters dropdown options
   * with the values from the data 
   * @return void
   * @memberof FormsDashboardComponent
   */
  createDropdownFilterList(): void {
    this.formsData.forEach((element) => {
      this.dropDownFilterData.status.add(element.status);
    });
  }

  /**
   * Dismisses the alert in specified seconds
   *
   * @param {number} seconds
   * @memberof FormsDashboardComponent
   */
  dismissAlert(seconds: number): void {
    setTimeout(() => {
      this.staticAlert ? this.staticAlert.close() : null;
      this.newFormCreated = null;
    }, seconds);
  }

  // Feature - Removing duplicate form as of now
  // duplicateFormSubmit(): void {
  //   const newDuplicateFormName = this.duplicateFormName.value;
  //   const isFormNameExists = this.formsData.some(form => form.formName.toLowerCase() === newDuplicateFormName.toLowerCase());
  //   if (isFormNameExists) {
  //     this.duplicateFormNameErrorMessage = newDuplicateFormName + FORM_ENGINE.DUPLICATE_FORM_MODAL.FORM_EXIST_ERROR;
  //   } else {
  //     this.loader.showLoader()
  //     const duplicateFormRequest: FormEngineDuplicateFormRequest = {
  //       formId: this.selectedForm.formId, formName: newDuplicateFormName
  //     }
  //     this.DuplicateFormSubscription$ = this.formEngineService.duplicateForm(duplicateFormRequest)
  //       .subscribe(
  //         (response) => {
  //           this.showSuccessToast(newDuplicateFormName + FORM_ENGINE.DUPLICATE_FORM_SUCCESS_MESSAGE);
  //           this.getFormsByCaseType();
  //           this.closeDuplicateFormModal();
  //           this.loader.hideLoader()
  //         },
  //         (err: ErrorEvent) => {
  //           const errResponse = err?.error?.error?.message ? err?.error?.error?.message : ERROR.MESSAGE.SOMETHING_WENT_WRONG;
  //           this.showErrorToast(errResponse, { code: err?.error?.error?.code })
  //           this.closeDuplicateFormModal();
  //           this.loader.hideLoader()
  //         }
  //       );
  //   }
  // }

  /*********** START: MODAL ACTIONS ***********/

  /**
   * Listener for table modal emitter
   *
   * @param {ModalData} eventData
   * @memberof FormsDashboardComponent
   */
  openFormModal(eventData: TableModalEmitter): void {
    this.selectedForm = eventData?.formData;
    this.openedModal = eventData?.modalType;
    switch (eventData?.modalType) {
      // Feature - Removing Audit History as of now
      // case this.FORM_MODAL_TYPES.AUDIT_HISTORY:
      //   this.openAuditHistoryModal(); break;
      // Feature - Removing duplicate form as of now
      // case this.FORM_MODAL_TYPES.DUPLICATE_FORM:
      //   this.openDuplicateFormModal(); break;
      case this.FORM_MODAL_TYPES.EDIT_TAGS:
        this.setEditTagsModalProperties();
        break;
      case this.FORM_MODAL_TYPES.EDIT_CONTENT_FORM:
        this.router.navigate([ FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM, this.selectedForm.formNumber])
        break;
    }
  }

  setEditTagsModalProperties() {
    if(this.selectedForm.statusOfTypes.contentForm === FormEngineFormStatus.Published) {
      this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM;
    } else {
      this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
    }
    this.isTagsEdited = true;
    this.isFormUploadRequired = false;
    this.assignTagsModalHeader = this.ASSIGN_TAGS_MODAL.EDIT_HEADER;
    this.isTagsSaved = false;
    this.openUploadFormModal();
    if(this.assignTagsType === this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM) {
      this.hasFormUpload = false;
      this.showPublishTab = false;
    } else {
      this.hasFormUpload = true;
      this.showPublishTab = true;
    }
  }
  // Feature - Removing Audit History as of now
  /**
   * Method to open the modal
   *
   * @return {Promise<void>}
   * @memberof FormsDashboardComponent
   */
  // async openAuditHistoryModal(): Promise<void> {
  //   await this.auditHistoryModalComponent.open();
  // }

  // Feature - Removing Audit History as of now
  /**
   * Method to close the modal
   * @memberof FormsDashboardComponent
   */
  // closeAuditHistoryModal(): void {
  //   this.openedModal = '';
  //   this.auditHistoryModalComponent?.close();
  // }

  // Feature - Removing duplicate form as of now
  /**
   * Method to open the modal
   *
   * @return {Promise<void>}
   * @memberof FormsDashboardComponent
   */
  // async openDuplicateFormModal(): Promise<void> {
  //   await this.duplicateFormModalComponent.open();
  // }

  // Feature - Removing duplicate form as of now
  /**
   * Method to close the modal
   * @memberof FormsDashboardComponent
   */
  // closeDuplicateFormModal(): void {
  //   this.duplicateFormName.setValue('');
  //   this.duplicateFormNameErrorMessage = '';
  //   this.openedModal = '';
  //   this.duplicateFormModalComponent?.close();
  // }

  onCloseAssignTagsModal() {
    this.showAssignTagsModal = false;
    if (this.isFetchDashboardFormDetails) {
      this.fetchDashboardDetails();
    }
    this.isFetchDashboardFormDetails = false;
  }

  /*********** END: MODAL ACTIONS ***********/


  /*********** START: TOAST ACTIONS ***********/
  /**
   * Method gets called by notificationService service on success
   * @param message 
   * @memberof FormsDashboardComponent
   */
  showSuccessToast(message) {
    this.notificationService.showSuccess(message);
  }

  /**
   * Method gets called by notificationService service on error
   * @param message
   * @memberof FormsDashboardComponent 
   */
  showErrorToast(message, code?) {
    this.notificationService.showError(message, code);
  }
  /*********** END: TOAST ACTIONS ***********/

  /**
     * Method to set the current tab in Assign tags modal
     * @param id Tab Nav Id
     */
   openTabByIdInAssignTagsModal(id) {
    this.assignTabCurrentTab = id;
  }
   /**
     * Method to set the properties for Assign tags Modal for "Upload New Form" functionality
     */
    setUploadFormAssignTagsModalProperties() {
      this.showAssignTagsModal = true;
      this.isFormUploadRequired = true;
      this.showPublishTab = true;
      this.assignTagsModalHeader = this.ASSIGN_TAGS_MODAL.ASSIGN_HEADER;
      this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.UPLOAD_FORM;
  }

  /**
   * Method to set the properties for Assign tags Modal for "Create Content Form" functionality
   */
  setContentFormAssignTagsModalProperties() {
      this.showAssignTagsModal = true;
      this.isFormUploadRequired = false;
      this.showPublishTab = false;
      this.assignTagsModalHeader = this.ASSIGN_TAGS_MODAL.CREATE_CONTENT_FORM;
      this.assignTagsType = this.ASSIGN_TAGS_MODAL.ASSIGN_TAGS_TYPE.CONTENT_FORM;
  }


  redirectToNextStep(currentRowData: FormEngineForm): void {
    if ((currentRowData?.type === FormEngineFormTypes.Form) && (currentRowData?.status === FormEngineFormStatus.New || currentRowData?.status == FormEngineFormStatus.InProgress)) {
      this.formDetails = currentRowData;
      this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
      this.setUploadFormAssignTagsModalProperties(); 
      this.isTagsSaved = true;
    }
    else if ((currentRowData?.type === FormEngineFormTypes.ContentForm) && (currentRowData?.status === FormEngineFormStatus.New)) {
        this.formDetails = currentRowData;
        this.openTabByIdInAssignTagsModal(this.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
        this.setContentFormAssignTagsModalProperties();
        this.isTagsSaved = true;
    }
    else if ((currentRowData?.type === FormEngineFormTypes.ContentForm) && (currentRowData?.status === FormEngineFormStatus.InProgress || currentRowData?.status === FormEngineFormStatus.Published)) {
        this.router.navigate([ FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.CONTENT_FORM, currentRowData.formNumber])
    }
    else if (currentRowData?.type === FormEngineFormTypes.GuidedInterview && (currentRowData?.status === FormEngineFormStatus.New || currentRowData?.status === FormEngineFormStatus.InProgress || currentRowData?.status === FormEngineFormStatus.Published)) {
        this.router.navigate([FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW, currentRowData.formId]);
    }
}

  redirectToEditInterview(currentRowData) {
    this.router.navigate([FRONT_END_ROUTES.FORMS, this.FORMIO_ROUTE_FORM_TYPES.GUIDED_INTERVIEW, currentRowData.formId]);
  }

  openUploadFormModal() {
    this.assignTabCurrentTab = this.ASSIGN_TAGS_MODAL.TABS.UPLOAD.NAV_ID;
    this.showAssignTagsModal = true;
    if (this.isTagsEdited) this.formDetails = this.selectedForm;
  }

  closeGeneralTagsModal() {
    this.showAssignTagsModal = false;
    this.isTagsEdited = false;

    if (this.caseTypeId) {
      this.formsData = null;
      this.getFormsByCaseType();
    }
  }

  ngOnDestroy(): void {
    this.FormEngineFormsSubscription$ ? this.FormEngineFormsSubscription$.unsubscribe() : null;
    // this.DuplicateFormSubscription$ ? this.DuplicateFormSubscription$.unsubscribe() : null;
    // this.closeAuditHistoryModal();
    // this.closeDuplicateFormModal();
  }
}