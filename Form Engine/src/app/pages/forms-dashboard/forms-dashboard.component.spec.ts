import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { FORM_ENGINE, FRONT_END_ROUTES } from 'src/app/core/constants';
import { DashboardService } from 'src/app/core/services';
import { FormEngineService } from 'src/app/core/services/form-engine.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { ModalComponent } from 'src/app/shared';
import mockFormsJson from 'src/test/data/formsResponse.json';
import mockCaseTypeFormsJson from 'src/test/data/formsDashboardResponse.json'
import mockFormEngineFormData from 'src/test/data/formEngineFormResponse.json';

import { FormsDashboardComponent } from './forms-dashboard.component';

describe('FormsDashboardComponent', () => {
  let component: FormsDashboardComponent;
  let fixture: ComponentFixture<FormsDashboardComponent>;
  const routerStub = () => ({ getCurrentNavigation: () => { return {extras: {state: true}} } });
  let loaderService: LoadingService; 
  let router: Router;

  let mockDashboardService = {
    getFormEngineFormsResponse: null,
    getFormsByCaseType: () => of(mockCaseTypeFormsJson)
  }

  let mockFormEngineService = {
    duplicateForm: () => of()
  }

  let mockToastrService = {
    success: () => {},
    error: () => {}
  }

  let mockLoaderService = {
    showLoader: () => {},
    hideLoader: () => {}
  }

  // Mock Activated Route
  const mockActivatedRoute = {
    snapshot: {
      queryParamMap: {
        get: () => {
          return 101;
        }
      }
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsDashboardComponent, ModalComponent, NgbAlert ],
      imports: [ RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot() ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute
        },
        { provide: Router, useFactory: routerStub },
        {
          provide: DashboardService,
          useValue: mockDashboardService
        },
        {
          provide: FormEngineService,
          useValue: mockFormEngineService
        },
        {
          provide: LoadingService,
          useValue: mockLoaderService
        },
        {
          provide: ToastrService,
          useValue: mockToastrService
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsDashboardComponent);
    component = fixture.componentInstance;
    loaderService = TestBed.inject(LoadingService);

    component.dropDownFilterData = {
      status : new Set('New')
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(mockDashboardService, 'getFormsByCaseType').and.returnValue(of(mockFormsJson))
    expect(component).toBeTruthy();
  });

  it('ngOnInit function gets called', () => {
    spyOn(component, 'getFormsByCaseType').and.callThrough();
    component.ngOnInit();
    expect(component.getFormsByCaseType).toHaveBeenCalled();
  });

  it('getFormsByCaseType function gets called', () => {
    spyOn(component, 'setFormEngineFormsByCaseTypeResponse').and.callThrough();
    spyOn(component, 'createDropdownFilterList').and.callThrough();
    spyOn(component, 'fetchFormCreationStatus').and.callThrough();
    spyOn(component, 'dismissAlert').and.callThrough();
    spyOn(mockDashboardService, 'getFormsByCaseType').and.returnValue(of(mockCaseTypeFormsJson))
    component.getFormsByCaseType();
    expect(mockDashboardService.getFormsByCaseType).toHaveBeenCalled();
    expect(component.setFormEngineFormsByCaseTypeResponse).toHaveBeenCalled();
    expect(component.formsData).toEqual(mockCaseTypeFormsJson.data.forms)
    expect(component.createDropdownFilterList).toHaveBeenCalled();
    expect(component.fetchFormCreationStatus).toHaveBeenCalled();
    expect(component.newFormCreated).toBeTruthy();
    expect(component.dismissAlert).toHaveBeenCalled();
  });

  it("tests the dismiss alert timeout", fakeAsync(() => {
    spyOn(component.staticAlert, 'close'); 
    component.dismissAlert(50);
    tick(50)
    fixture.detectChanges()
  
    fixture.whenStable().then(() => {
      expect(component.staticAlert.close).toHaveBeenCalledTimes(1);
    })
    flush();
  }))

  it('openGeneralTagsModal should open the openGeneralTagsModal', () => {
    component.openGeneralTagsModal();
    expect(component.assignTabCurrentTab).toEqual(FORM_ENGINE.ASSIGN_TAGS_MODAL.TABS.GENERAL.NAV_ID);
    expect(component.showAssignTagsModal).toBeTruthy();
  });

  it('openModal should open up the modal', () => {
    spyOn(component.auditHistoryModalComponent, 'open');
    spyOn(component.duplicateFormModalComponent, 'open')
    const auditHistoryEvent = { formData: mockFormsJson.data[0], modalType: FORM_ENGINE.MODAL_TYPES.AUDIT_HISTORY }
    const duplicateFormEvent = { formData: mockFormsJson.data[0], modalType: FORM_ENGINE.MODAL_TYPES.DUPLICATE_FORM }
    component.openFormModal(auditHistoryEvent);
    expect(component.auditHistoryModalComponent.open).toHaveBeenCalledTimes(1);
    component.openFormModal(duplicateFormEvent);
    expect(component.duplicateFormModalComponent.open).toHaveBeenCalledTimes(1);
  })
  
  it('closeModal should close the modal', () => {
    spyOn(component.auditHistoryModalComponent, 'close');
    spyOn(component.duplicateFormModalComponent, 'close')
    component.closeAuditHistoryModal();
    expect(component.auditHistoryModalComponent.close).toHaveBeenCalledTimes(1);
    expect(component.openedModal).toEqual('');
    component.closeDuplicateFormModal()
    expect(component.duplicateFormModalComponent.close).toHaveBeenCalledTimes(1);
    expect(component.duplicateFormName.value).toEqual('');
    expect(component.duplicateFormNameErrorMessage).toEqual('');
    expect(component.openedModal).toEqual('')
  });


  it('submit of duplicate form', () => {
    spyOn(component, 'duplicateFormSubmit');
    spyOn(loaderService, 'showLoader')
    component.formsData = mockFormsJson;
    component.duplicateFormName.setValue('Foo');
    component.duplicateFormSubmit();
  });

  it('redirectToNextStep redirects to Forms page', () => {
    spyOn(router, 'navigate');
    const currentRow = mockFormEngineFormData;
    component.redirectToNextStep(currentRow);
    expect(router.navigate).toHaveBeenCalledWith([FRONT_END_ROUTES.FORMS], currentRow.formId);
  });
});
