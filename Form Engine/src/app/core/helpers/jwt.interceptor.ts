import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { SharedService } from 'src/app/shared';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { API_ROUTES, ERROR, FRONT_END_ROUTES } from '../constants';
import { CurrentUser } from '../models';
import { canSkipInterceptor } from '../utils';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        private authenticationService: AuthenticationService,
        private sharedService: SharedService,
        private modalService: NgbModal) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(request)
            .pipe(catchError(
                err => {
                    if (err instanceof HttpErrorResponse && err?.status === 401) {
                        // Auto Logout, if 401 response returned from REFRESH_TOKEN Endpoint
                        if(request.url.includes(API_ROUTES.REFRESH_TOKEN)) { 
                            return this.logoutAndRedirect(err); 
                        }
                        // Refreshes the access token, if 401 response returned from endpoints other than JAQ LOGIN Endpoint
                        else if(err?.error?.error?.code === ERROR.CODE.EXPIRED_JWT && !(request?.url?.includes(API_ROUTES.LOGIN.FORM_ENGINE))) {
                            return this.refreshToken(request, next);
                        }
                        else {
                            return throwError(err);
                        }
                    } 
                    // Endpoints that has to be skipped by the interceptor
                    else if(canSkipInterceptor(request?.url)){
                        return throwError(err);
                    }
                    else if(err instanceof HttpErrorResponse){
                        this.modalService.dismissAll();
                        let errorMessage: string = err?.error?.error?.message || ERROR.MESSAGE.SOMETHING_WENT_WRONG;
                        this.sharedService.redirectRoute(FRONT_END_ROUTES.ERROR, { state: { message: errorMessage } });
                        return throwError(err);
                    }
                }
            ));
    }

    /**
     * Refreshes the access token using refresh token
     *
     * @private
     * @param {HttpRequest<any>} request
     * @param {HttpHandler} next
     * @return {*}  {Observable<HttpEvent<any>>}
     * @memberof JwtInterceptor
     */
    private refreshToken(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const currentUser: CurrentUser = this.sharedService.currentUserValue;
        return this.authenticationService.refreshJWT(currentUser.refreshToken)
            .pipe(
                switchMap(() => next.handle(this.addTokenHeader(request)))
            );
    }

    /**
     * Adding the token header for the Refresh Token API call
     *
     * @param {HttpRequest<any>} request
     * @return {*}  {HttpRequest<any>}
     * @memberof JwtInterceptor
     */
    addTokenHeader(request: HttpRequest<any>): HttpRequest<any>{
        request = request.clone({
            setHeaders: {
                Authorization: this.sharedService.APIConfigurationKey
            }
        });
        return request;
    }

    /**
     * Log outs the user and redirect to LOGIN page
     *
     * @private
     * @param {HttpErrorResponse} err
     * @return {*}  {Observable<HttpEvent<any>>}
     * @memberof JwtInterceptor
     */
    private logoutAndRedirect(err: HttpErrorResponse): Observable<HttpEvent<any>> {
        this.modalService.dismissAll();
        this.sharedService.logout();
        return throwError(err);
    }
}