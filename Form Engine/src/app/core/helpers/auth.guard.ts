import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AlertService, AuthorizationService, SharedService } from 'src/app/shared';
import { ALERT_TYPES, ERROR, FRONT_END_ROUTES } from '../constants';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor( private sharedService: SharedService, 
        private authorizationService: AuthorizationService,
        private alertService: AlertService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const currentUser = this.sharedService.currentUserValue;
        
        if(currentUser){
            const allowedRoles = route.data.allowedRoles;
            const isAuthorized = this.authorizationService.isAuthorized(allowedRoles);
            if (isAuthorized) {
                return true;
            }
            else{
                this.showErrorAlert(ERROR.MESSAGE.UN_AUTHORIZED)
                this.sharedService.redirectRoute(FRONT_END_ROUTES.LOGIN);
                return false;
            }
        }
        this.sharedService.redirectRoute(FRONT_END_ROUTES.LOGIN);
        return false;
    }

    /**
    * Show Error alert message
    * @returns null
    * @memberof AuthGuard
    */
    showErrorAlert(message: string): void {
        this.alertService.setAlertMessage({
            type: ALERT_TYPES.DANGER, message: message || ERROR.MESSAGE.SOMETHING_WENT_WRONG, dismissable: true 
        });
    }
}