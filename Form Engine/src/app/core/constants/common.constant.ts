const COMMON = {
    CURRENT_USER: 'currentUser',
    RETURN_URL: 'returnUrl',
    BEARER: "Bearer",
    UNDERSCORE: "_",
    FORWARD_SLASH: "/",
    PLUS: "+",
    HASH: "#",
    AMPERSAND: "&",
    EQUALTO: "=",
    USERNAME: "username",
    MAIN_CONTAINER: '#mainContainer',
    SKIP_TO_MAIN: "Skip to main content",
    APPLICATION_ACCESS: {
        FORM_ENGINE: "formEngine",
    },
    DASHBOARD_ACCESS_ROLES: {
        READER: 'reader', 
        AUTHOR: 'author',
        ADMIN: 'admin'
    },
    FILE_TYPES: {
        PDF: 'application/pdf'
    }
}

const SECURITY = {
    BEARER: 'Bearer ',
    AUTHORIZATION: 'Authorization',
    ACCEPT_HEADER: 'Accept',
    MIME_TYPE: {
        JSON: 'application/json'
    }
}

const IMAGES_DIR_PATH = 'assets/images';

const ASSETS = {
    UTAH_COURTS_LOGO: `${IMAGES_DIR_PATH}/USC_logo.svg`
}

const ALERT_TYPES = {
    SUCCESS: 'success',
    INFO: 'info',
    WARNING: 'warning',
    DANGER: 'danger',
    PRIMARY: 'primary',
    SECONDARY: 'secondary',
    LIGHT: 'light',
    DARK: 'dark',
}

const TABLE = {
    PAGE_SIZE : 25,
    SELECTED_MESSAGE: "selected",
    TOTAL_MESSAGE: "total",
    EMPTY_MESSAGE: "No Results",
    MIN_ROWS: 10
}

const ERROR = {
    MESSAGE: {
        NOT_LOGGED_IN: 'You have been logged out of the application! Please Login.',
        NOT_FOUND: "UH OH! You're lost. The page you are looking for does not exist.",
        SOMETHING_WENT_WRONG: 'Oops! Something went wrong!',
        UN_AUTHORIZED: "You don't have access. Contact your supervisor for access."
    },
    CODE: {
        EXPIRED_JWT: "EXPIRED_JWT"
    }
};

const BUTTONS = {
    LOGIN: 'Login',
    CANCEL: 'Cancel',
    CONTINUE: 'Continue',
    SUBMIT: 'Submit',
    GO_TO_LOGIN: 'GO TO LOGIN',
}

const USER_ROLES = {
    ADMIN: 'admin',
    AUTHOR: 'author',
    READER: 'reader',
} 

const AUTHORIZED_USERS = {
    DASHBOARD: [ USER_ROLES.ADMIN, USER_ROLES.AUTHOR, USER_ROLES.READER], 
    FORMS: [ USER_ROLES.ADMIN, USER_ROLES.AUTHOR, USER_ROLES.READER]
}

const MODAL = {
    SIZE: {
        S: 's',
        M: 'm',
        LG: 'lg',
        XL: 'xl'
    }
}


export { COMMON, SECURITY, ASSETS, ALERT_TYPES, TABLE, ERROR, BUTTONS, MODAL, USER_ROLES, AUTHORIZED_USERS };
