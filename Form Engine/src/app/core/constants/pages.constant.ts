import { FormEngineFormStatus } from 'src/app/client';
import { environment } from 'src/environments/environment';

const LOADER = {
    TYPE: {
        BORDER: "border",
        GROW: "grow",
    },
    COLOR: {
        PRIMARY: "primary",
    }
}
const DASHBOARD = {
    HEADER: "Guided Interview Dashboard",
    FORMS: "Forms"
}

const HEADER = {
    NAV_ITEMS: {
        FORMS: "Form Engine",
        MY_CASE: "My Case",
        HELP: {
            TEXT: "Help",
            PRIVACY_POLICY: "Privacy Policy",
            CONTACT_US: "Contact Us"
        },
        USER_PROFILE: {
            WELCOME_MESSAGE: "Welcome",
            LOGOUT: "Logout"
        },
    },
    HYPERLINKS: {
        PRIVACY_POLICY: "https://www.utcourts.gov/privacy/",
        CONTACT_US: "https://www.utcourts.gov/smallclaimsodr/contact.html"
    }
}

const FORMS = {
    ID: "id",
    TYPE: "type"
}

const GUIDED_INTERVIEW = {
    ADOBE_SIGN_HOST_URL: environment.adobeSignHostUrl,
    ADOBE_SIGN_EVENTS: {
        PAGE_LOAD: "PAGE_LOAD",
        ESIGN: "ESIGN"
    },
    FORM: "form",
    WIZARD: "wizard"
}

const DATA_TABLE = {
    PAGE_LIMIT: [
        { key: '10', value: 10 },
        { key: '25', value: 25 },
        { key: '50', value: 50 },
        { key: '100', value: 100 }
    ],
    PER_PAGE: 'per page',
    DATA_PARAMS: {
        pageNum: 0,
        pageSize: 0
    },
    SORT: {
        DATE_ASC: { prop: 'statusDate', dir: 'asc' },
        DATE_DESC: { prop: 'statusDate', dir: 'desc' },
        GI_DATE_DESC: { prop: 'effectiveDateGuidedForm', dir: 'desc' }
    },
    DATE_FIELD_ID: "#datefilter",
}

const FORM_ENGINE = {
    FORM_ENGINE_DASHBOARD_PAGE_CONSTANTS: {
        CREATE_BTN: 'Create',
        CONTENT_FORM: 'Content Form',
        GUIDED_INTERVIEW: 'Guided Interview',
        TEMPLATE: 'Template'
    },
    FORMS_PAGE_CONSTANTS: {
        CONTENT_FORM: {
            CANCEL: 'Cancel',
            PUBLISH_CONTENT_FORM: 'Publish Form',
            PUBLISH_SUCCESS: ' Content Form published successfully',
            EDIT_SUCCESS: ' Content Form Edited successfully',
            SAVE_SUCCESS: ' Content Form draft saved successfully',
            PUBLISH_TITLE: 'Publish Content Form'
        }
    },
    FORMIO_ROUTE_FORM_TYPES: {
        QUERY_PARAM: 'form-type',
        CONTENT_FORM: 'cf',
        GUIDED_INTERVIEW: 'gi',
        TEMPLATE: 'template'
    },
    STATUS: {
        NEW: {
            text: "New",
            code: 'n',
            API_CODE: FormEngineFormStatus.New
        },
        IN_PROGRESS: {
            text: "In Progress",
            code: 'ip',
            API_CODE: FormEngineFormStatus.InProgress
        },
        PUBLISHED: {
            text: "Published",
            code: 'p',
            API_CODE: FormEngineFormStatus.Published
        },
        RETIRED: {
            text: "Retired",
            code: 'r',
            API_CODE: FormEngineFormStatus.Retired
        }
    },

    PUBLISH_STATUS_TYPES: {
        BASIC_PDF: "Basic PDF",
        FILLABLE_FORM: "Fillable Form",
        GUIDED_INTERVIEW: "Guided Interview",
        SAVE_DRAFT_GI: "Save as Draft Guided Interview",
        UNCREATED_GI: "Uncreated Guided Interview",
        CONTENT_FORM: "Content Form"
    },
    FORMS_STATUS_WITH_TYPE_KEYS: [
        'Pending Basic PDF', 'Published Basic PDF', 'In Progress Basic PDF',
        'Pending Fillable Form', 'Published Fillable Form', 'In Progress Fillable Form',
        'Pending Content Form', 'Published Content Form', 'In Progress Content Form',
        'Pending Guided Interview', 'Published Guided Interview', 'In Progress Guided Interview'],
    FORMS_STATUS_WITH_TYPE: {
        'pending basic pdf': {
            key: 'basicPdf',
            value: 'PENDING',
            NAME: 'Pending Basic PDF'
        },
        'published basic pdf': {
            key: 'basicPdf',
            value: 'PUBLISHED',
            NAME: 'Published Basic PDF'
        },
        'in progress basic pdf': {
            key: 'basicPdf',
            value: 'IN PROGRESS',
            NAME: 'In Progress Basic PDF'
        },
        'pending fillable form': {
            key: 'fillableForm',
            value: 'PENDING',
            NAME: 'Pending Fillable Form'
        },
        'published fillable form': {
            key: 'fillableForm',
            value: 'PUBLISHED',
            NAME: 'Published Fillable Form'
        },
        'in progress fillable form': {
            key: 'fillableForm',
            value: 'IN PROGRESS',
            NAME: 'In Progress Fillable Form'
        },
        'pending content form': {
            key: 'contentForm',
            value: 'PENDING',
            NAME: 'Pending Content Form'
        },
        'published content form': {
            key: 'contentForm',
            value: 'PUBLISHED',
            NAME: 'Published Content Form'
        },
        'in progress content form': {
            key: 'contentForm',
            value: 'IN PROGRESS',
            NAME: 'In Progress Content Form'
        },
        'pending guided interview': {
            key: 'guidedInterview',
            value: 'PENDING',
            NAME: 'Pending Guided Interview'
        },
        'published guided interview': {
            key: 'guidedInterview',
            value: 'PUBLISHED',
            NAME: 'Published Guided Interview'
        },
        'in progress guided interview': {
            key: 'guidedInterview',
            value: 'IN PROGRESS',
            NAME: 'In Progress Guided Interview'
        }
    },

    FORMIO_ACTIONS: {
        CANCEL: 'Cancel',
        SAVE_DRAFT: 'Save As Draft',
        PUBLISH: 'Publish'
    },

    /****** PAGE STATIC TEXTS ******/
    HEADER: "Forms Engine Dashboard",
    ALL_PUBLISHED_FORMS: "All Forms",
    All_FORMS_ID: 'all',
    FORMS_IN_PROGRESS: "Forms In Progress",
    FORMS_IN_PROGRESS_SUBTEXT: "Select to edit and complete new forms",
    NO_IN_PROGRESS_FORMS: "No In Progress Forms available",
    COMPLETED_FORMS: "Recently Completed Items",
    COMPLETED_FORMS_SUBTEXT: "Items recently published are listed below. Select an item to enter edit mode.",
    CASE_TYPE_SUCCESS_MESSAGE: " has been successfully added to ",
    DUPLICATE_FORM_SUCCESS_MESSAGE: " has been successfully duplicated",
    EMPTY_FORMS_DATA: "No Records Assigned",
    SHARED_FORM: "Shared",
    VIEW_MORE: "View More",
    SAVE_FORM_SUCCESS_MESSAGE: " is saved successfully",
    PUBLISH_FORM_SUCCESS_MESSAGE: " Guided Interview has been published successfully",
    BASIC_PDF_PUBLISH_SUCCESS_MESSAGE: " Basic PDF has been published successfully",
    FILLABLE_FORM_PUBLISH_SUCCESS_MESSAGE: " Fillable Form has been published successfully",
    CREATE_NEW_TEMPLATE: "Create New Template",
    CREATE_TEMPLATE_SUCCESS: " Template has been added successfully",
    FORMIO_DRAG_DROP: "Drag and Drop a form component to review the form",
    REVIEW_FORM: "Review Form",
    SECTION: "section",

    FORMS_TABLE: {
        PLACEHOLDER: {
            DOC_NAME: 'Document Name',
            TAGS: 'Tags (Roles)',
            STATUS: 'Status',
            DATE: 'Status Date',
            RULES: 'Rules / Statutes',
            CASE_TYPES: 'Case Type(s)'
        }
    },

    /****** TYPES ******/
    TABLE_TYPES: {
        ALL_FORMS: 'All Forms',
        CASE_TYPE_FORMS: 'Case Type Forms',
        AUDIT_HISTORY: 'Audit History',
        IN_PROGRESS_FORMS: 'In Progress Forms'
    },
    MODAL_TYPES: {
        AUDIT_HISTORY: 'Audit History',
        DUPLICATE_FORM: 'Duplicate Form',
        EDIT_TAGS: 'Edit',
        VIEW_HISTORY: 'View History',
        EDIT_CONTENT_FORM: 'Edit Form',
        EDIT_FORM_DETAILS: 'Edit Form Details'
    },

    /****** MODALS ******/
    ADD_CASE_TYPE_MODAL: {
        TITLE: "Add New Case Type",
        LABEL: "Case Type",
        CASE_TYPE_REGEX_PATTERN: /^[a-zA-Z \-()]*$/,
        CASE_EXIST_ERROR: "A Case Type with this name already exists. Please choose a unique Case Type to add.",
        CASE_TYPE_EMPTY_ERROR: "Please enter a text for Case Type.",
        CASE_TYPE_NAME_INVALID_ERROR: "Only - ( ) are allowed as special characters.  Please update the Case Type text only use these special characters.",
        CASE_TYPE_LENGTH_VALIDATION_ERROR: "Case Type should not exceed maximum length of 100",
        CREATE_BTN: "Create",
        CANCEL_BTN: "Cancel",
        PLACE_HOLDER: "Start Typing Here",
        POPULAR: "Popular"
    },
    AUDIT_HISTORY_MODAL: {
        TITLE: " Audit History",
        CLOSE_BTN: "Close",
        STATUS_ICONS: {
            'New': {
                icon: 'check',
                color: 'primary'
            },
            'In Progress': {
                icon: 'check',
                color: 'warning'
            },
            'Published': {
                icon: 'check',
                color: 'success'
            },
            'Retired': {
                icon: 'times',
                color: 'danger'
            }
        }
    },
    DUPLICATE_FORM_MODAL: {
        TITLE: " Duplicate Form Action",
        LABEL: "Enter form name",
        FORM_EXIST_ERROR: " form already exists",
        CREATE_BTN: "Duplicate Form",
        CANCEL_BTN: "Cancel"
    },
    ASSIGN_TAGS_MODAL: {
        TABS: {
            UPLOAD: {
                NAV_ID: 1,
                TEXT: "Form"
            },
            GENERAL: {
                NAV_ID: 2,
                TEXT: "General"
            },
            AEM: {
                NAV_ID: 3,
                TEXT: "AEM"
            },
            PUBLISH: {
                NAV_ID: 4,
                TEXT: "Publish"
            }
        },
        ASSIGN_TAGS_TYPE: {
            UPLOAD_FORM: 'UPLOAD_FORM',
            CONTENT_FORM: 'CONTENT_FORM',
            GUIDED_FORM: 'GUIDED_FORM'
        },
        ASSIGN_HEADER: "Upload New Form",
        CREATE_CONTENT_FORM: "Create Content Form",
        EDIT_HEADER: "Edit Tags",
        FORM: "Form",
        BASIC_PDF: "Basic PDF",
        FILLOUT: "Fillable Form",
        GUIDED_FORMS: "Guided Interview",
        EFFECTIVE_DATE: "Effective Date",
        CASE_TYPES: "Case Type(s)",
        ROLES: "Role(s)",
        STEPS: "Step(s)",
        RULES: "Rule(s)",
        STATUTES: "Statute(s)",
        COMMENTS: "Comment Log",
        DESCRIPTION: "Description",
        ASSOCIATED_URLS: "Associated URL(s)",
        ADD_ANOTHER_URL: "+ Add Another URL",
        TYPE_URL: "Type URL",
        TYPE_LABEL: "Type Label",
        REMOVE_BTN: "Remove",
        CANCEL: "Cancel",
        SAVE: "Save as Draft",
        PREV_BTN: "Previous",
        NEXT_BTN: "Next",
        SAVE_AND_PUBLISH: "Save and Publish",
        PUBLISH_BTN: "Publish",
        PUBLISH_AND_CONTINUE_BTN: "Publish & Continue",
        UPDATE_TAGS: "Update Tag(s)",
        UPDATE_AND_PUBLISH: "Update and Publish",
        UPDATE_AND_NEXT: "Update and Next",
        TAGS_CREATE_CONTENT_FORM: "Create",
        TAGS_EDIT_UPDATE_AND_PUBLISH: "Update and Publish",
        ERROR_MESSAGES: {
            FORM_TYPE_REQUIRED: "Please choose atleast one filing type",
            CASE_TYPE_REQUIRED: "Case Type is required",
            ROLE_REQUIRED: "Role is required",
            STEP_REQUIRED: "Step is required",
            MIN_PUBLISH_TYPE_REQUIRED: "Atleast one publish type needs to be selected",
            EFFECTIVE_DATE_REQUIRED: "The Effective date is required",
            EFFECTIVE_DATE_VALIDATION: "The Effective date should not be lesser than the today's date"
        },
        ERROR_CODE: {
            INVALID_FORM_NUMBER: "INVALID_FORM_NUMBER",
            INVALID_FORM_TITLE: "INVALID_FORM_TITLE",
            INVALID_FORM: "INVALID_FORM",
            INVALID_FORM_TEMPLATE: "INVALID_FORM_TEMPLATE",
            NO_FORM_FIELDS_FOUND: "NO_FORM_FIELDS_FOUND"
        },
        EDIT_TAGS_SUCCESS: 'Form details are updated successfully',
        SAVE_TAGS_SUCCESS: ' Form Tags saved successfully'
    },
    SHARED_FORMS_MODAL: {
        MODAL_CONTENT_TEXT: "This form is a shared Universal Form.",
        MODAL_CONTENT_SUBTEXT: "Modifying it will affect the form in multiple case types.",
        CANCEL_BTN: "Cancel",
        GOT_IT_BTN: "Got it"
    },
    CREATE_TEMPLATE_MODAL: {
        MODAL_CONTENT_TEXT: "Create New Template",
        MODAL_CONTENT_PLACEHOLDER: "Enter Template Name",
        CANCEL_BTN: "Cancel",
        CREATE_BTN: "Create",
        TITLE_ERROR_CODE: "INVALID_TEMPLATE",
        TITLE_EXIST_ERROR: "Template Already Exists",
    },
    CONTENT_GENERATION_MODAL: {
        TITLE: "Content Generation",
        FORM_HEADER: "Form",
        FROM_SCRATCH_BTN: "From Scratch",
        GENERATE_DRAFT: "Generate Draft",
        TYPE: {
            SCRATCH: "scratch",
            DRAFT: "draft"
        }
    },
    GUIDED_INTERVIEW_PUBLISH_MODAL: {
        TITLE: "Publish Interview",
        CANCEL_BTN: "Cancel",
        PUBLISH_BTN: "Publish",
        EFFECTIVE_DATE: "Effective Date",
        COMMENTS_LOG: "Comments Log",
        FORM_TEXT: "Form : ",
        ERROR_MESSAGES: {
            EFFECTIVE_DATE_REQUIRED: "The Effective date is required",
        },
        CONTENT_FORM_ASSOCIATION: "Content Form Association",
        CONTENT_FORM_PLACEHOLDER: "Select content form",
        CONTENT_FORM_TYPE_PLACEHOLDER: "Select content form type",
        ADD_ANOTHER_CONTENT_FORM: "+ Add another content Form"
    },
    GUIDED_INTERVIEW_CREATE_MODAL: {
        TITLE: "Create Guided Interview",
        INTERVIEW_TITLE: "Interview Title",
        DESC: "Description",
        CANCEL_BTN: "Cancel",
        CONTINUE_BTN: "Continue",
        TITLE_EXIST_ERROR: "Interview Title already exist",
        TITLE_REQUIRED: "Interview Title is required",
        TITLE_ERROR_CODE: "GI_TITLE_INVALID"
    },
    BUTTON: {
        SAVE_DRAFT: 'Save as Draft',
        PUBLISH_INTERVIEW: 'Publish Interview',
        CREATE_TEMPLATE: "Create Template",
        CANCEL: "Cancel",
        PREV_BTN: "Previous",
        NEXT_BTN: "Next",
        SUBMIT_FORM: "Submit Form",
        PUBLISH_FORM: "Publish Form",
        SUBMIT_BTN: "Submit"
    },
    /****** TABLES ******/
    CASE_TYPE_COLUMNS: {
        PROP: {
            FORM_NAME: 'formName',
            TAG: 'roles',
            STATUS: 'statusOfTypes',
            STATUS_DATE: 'statusDate',
            RULES: 'statutes',
            CASE_TYPES: 'caseTypes',
            EFFECTIVE_DATE: {
                FILLABLE: 'effectiveDateFillableForm',
                GI: 'effectiveDateGuidedForm'
            }
        },
        NAME: {
            SELECT_ONE: 'Select One',
            FORM_NAME: 'Form Name',
            TAG: 'Tags (Roles)',
            STATUS: 'Status',
            STATUS_DATE: 'Status Date',
            RULES: 'Rules / Statutes',
            CASE_TYPES: 'Case Type(s)',
            EFFECTIVE_DATE: {
                FILLABLE: 'Fillable Form Effective Date',
                GI: 'Guided Interview Effective Date'
            }
        }
    },

    AUDIT_HISTORY_COLUMNS: {
        PROP: {
            STATUS: 'status',
            USER: 'updatedBy',
            DATE: 'updatedAt',
            COMMENTS: 'comments',
        },
        NAME: {
            STATUS: 'Action',
            USER: 'User',
            DATE: 'Date',
            COMMENTS: 'Comments'
        }
    },

    UPLOAD_NEW_FORM_MODAL: {
        TITLE: 'Upload New Form',
        FORM_TITLE_LABEL: 'Form Title',
        FORM_NUMBER_LABEL: 'Form Number',
        UPLOAD_FILE: 'Upload File',
        DESCRIPTION: "Description",
        UPLOAD_FILE_PLACEHOLDER: 'Select file to upload',
        PLACEHODLER: "Start Typing Here",
        SUBMIT_BTN: 'Upload and Continue',
        SAVE_AND_CONTINUE_BTN: 'Save and Continue',
        CANCEL_BTN: "Cancel",
        FORM_TITLE_REQUIRED: "Form Title is required",
        FORM_TITLE_INVALID: "Form Title is invalid",
        FORM_NUMBER_REQUIRED: "Form Number is required",
        FORM_NUMBER_INVALID: "Form Number is invalid",
        PUBLISH_CONTINUE_BTN: "Publish & Continue",
        FILE_REQUIRED: "Upload File is required",
        FORM_UPLOAD_SUCCESS: " form has been uploaded successfully",
        ERROR_MESSAGES: {
            FORM_TITLE_REQUIRED: "Form title is required",
            FORM_NUMBER_REQUIRED: "Form Number is required",
            UPLOAD_FILE_REQUIRED: "Upload File is required",
            FILE_FORMAT_UNSUPPORTED: "File Format not supported",
            FORM_NUMBER_MAXLENGTH_VALIDATION: "Form Number cannot exceed 20 characters",
            FORM_NUMBER_PATTERN_VALIDATION: "Form Number can contains only alphanumeric, period(.) and underscore(_)",
            DESCRIPTION_MAXLENGTH_VALIDATION: "The Description cannot exceed 250 characters",
            FORM_NUMBER_EXISTS: "Form Number already exists",
            FORM_TITLE_EXISTS: "Form Title already exists",
            INVALID_FORM_TEMPLATE: "Fillable Form and/or Guided Interview exist. Please ensure the PDF is in an editable format."
        }
    },

    CANCEL_CONFIRMATION_MODAL: {
        HEADER: 'Cancel Form?',
        MESSAGE: 'Are you sure you want to cancel? Your form will not be saved.',
        RETURN_TO_FORM: "Return to Form",
        CONTINUE: "Continue"
    }
}

const LOGIN = {
    USERNAME: 'CORIS Username',
    PASSWORD: 'CORIS Password',
    HEADING: 'Utah Courts',
    SUB_HEADING: 'Forms Engine Admin Portal',
    GREET: 'Welcome to Forms Engine Admin Portal',
    COMMON_ERROR_MESSAGE: 'Invalid Username and/or Password',
    COMMON_ERROR_MESSAGE1: 'Invalid Username or/and Password',
    INVALID_CREDENTIALS: 'INVALID_CREDENTIALS',
    USERNAME_REQUIRED: 'Username is required',
    PASSWORD_REQUIRED: 'Password is required',
    USER_ROLE_NULL: 'USER_ROLE_NULL'
}

const FORMIO_CONSTANTS = {
    BUILDER: {
        BUTTON_TOOLTIP: "Button Text entered in the initial panel(page) of the wizard group will be applied to all the pages.",
        WIZARD_CUSTOM_PROPERTIES: "'Show Stepper Title': Conditional to show stepper based on the checkbox selected in the initial panel(page) of the wizard group &#013;'is Submit Page: The Submit button will be displayed in the final panel(page) of the wizard group which has this checkbox selected. &#013; 'Has Preview Edit Option:' The Edit button in Preview Form Modal will be displayed if this checkbox is selected in the initial panel(page) of the wizard group. &#013; 'Form with Payment Option': Flag to determine whether the form has payment option. &#013; 'Is Payment Page': The Submit button will be displayed in the final panel(page) of the wizard group which has this checkbox selected.",
        CANCEL_REDIRECTION: "Cancel Redirection chosen in the initial panel(page) of the wizard group will be applied to all the pages.",
        CANCEL_REDIRECTION_OPTIONS: {
            DASHBOARD: {
                LABEL: "Dashboard",
                VALUE: "dashboard"
            },
            MYCASE: {
                LABEL: "MyCase",
                VALUE: "myCase"
            }
        }
    }
}

export { LOADER, HEADER, DASHBOARD, FORMS, DATA_TABLE, GUIDED_INTERVIEW, FORM_ENGINE, LOGIN, FORMIO_CONSTANTS };
