import { environment } from 'src/environments/environment';

const API_ROUTES = {
    REFRESH_TOKEN: `${environment.apiUrl}/refresh-token`,
    LOGIN: {
        JAQ: `${environment.apiUrl}/login`,
        FORM_ENGINE: `${environment.apiUrl}/login?page=form_engine`
    },
    FORMS: `${environment.apiUrl}/fe/forms`,
    FORMS_LIST_DASHBOARD: `${environment.apiUrl}/fe`,
}

const FRONT_END_ROUTES_PATH = { 
    FORMS_DASHBOARD: 'dashboard',
    FORMS_LIST_DASHBOARD: 'forms-list',
    FORMS: 'form',
    LOGIN: 'login',
    ERROR: 'error',
    TEMPLATE: 'template'
}

const FRONT_END_ROUTES = { 
    FORMS_DASHBOARD: `/${FRONT_END_ROUTES_PATH.FORMS_DASHBOARD}`,
    FORMS_LIST_DASHBOARD: `/${FRONT_END_ROUTES_PATH.FORMS_LIST_DASHBOARD}`,
    FORMS: `/${FRONT_END_ROUTES_PATH.FORMS}`,
    LOGIN: `/${FRONT_END_ROUTES_PATH.LOGIN}`,
    ERROR: `/${FRONT_END_ROUTES_PATH.ERROR}`,
    TEMPLATE: `/${FRONT_END_ROUTES_PATH.TEMPLATE}`
}


const MYCASE_ROUTES = {
    LOGIN: `${environment.myCaseUrl}/LoginServlet`,
    CASE_INFORMATION: `${environment.myCaseUrl}/CaseInformationServlet`,
}

const SKIP_INTERCEPTORS = [
    API_ROUTES.LOGIN.FORM_ENGINE,
    API_ROUTES.FORMS,
    API_ROUTES.FORMS_LIST_DASHBOARD,
    API_ROUTES.LOGIN.JAQ
];

export { API_ROUTES, FRONT_END_ROUTES, MYCASE_ROUTES, SKIP_INTERCEPTORS, FRONT_END_ROUTES_PATH };
