import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginRequest, LoginResponse } from 'src/app/client';
import { SharedService } from 'src/app/shared';
import { API_ROUTES } from '../../constants';
import { CurrentUser } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private sharedService: SharedService) { }

  /**
   * Jaq login function
   * @param {LoginRequest} credentials
   * @returns loginResponse
   * @memberof JaqService
   */
  login(credentials: LoginRequest): Observable<LoginResponse> {
    return this.sharedService.doPost(API_ROUTES.LOGIN.FORM_ENGINE, credentials).pipe(
      tap((response: LoginResponse) => {
        if (response.success && response.data) {
          const currentUser: CurrentUser = this.sharedService.persistToken(response);
          this.sharedService.setCurrentUserValue(currentUser);
        }
        return response;
      })
    );
  }
}
