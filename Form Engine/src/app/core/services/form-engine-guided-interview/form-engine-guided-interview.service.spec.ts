import { TestBed } from '@angular/core/testing';

import { FormEngineGuidedInterviewService } from './form-engine-guided-interview.service';

describe('FormEngineGuidedInterviewService', () => {
  let service: FormEngineGuidedInterviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineGuidedInterviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
