import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { EditInterviewFormStatus, FormEngineAssociatedContentFormsPayload, FormEngineContentFormMappingTypesResponse, FormEngineCreateGuidedFormsResponse, FormEngineFormAuditHistoryResponse, FormEngineFormStatus, FormEngineGetGuidedFormResponse, FormEngineGuidedInterviewApiService } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineGuidedInterviewService {

  private auditHistoryResponseSubject: BehaviorSubject<FormEngineFormAuditHistoryResponse>;
  public auditHistoryResponse: Observable<FormEngineFormAuditHistoryResponse>;

  constructor(private formEngineGuidedInterviewService: FormEngineGuidedInterviewApiService) {
    this.auditHistoryResponseSubject = new BehaviorSubject<any>(null);
    this.auditHistoryResponse = this.auditHistoryResponseSubject.asObservable();
  }


  /************ START: GI Form Sevices  *****************/

  /**
   * Method to create the Guided Interview Form
   * @param formName 
   * @param formDescription 
   * @returns FormEngineCreateGuidedFormsResponse
   */
  createGuidedInterviewForm(formName: string, formDescription: string): Observable<FormEngineCreateGuidedFormsResponse> {
    return this.formEngineGuidedInterviewService.createGuidedInterviewForm({ formName, formDescription });
  }

  /**
   * Method to Store the FormIO properties of GI and Content Form mapping
   * @param formName 
   * @param formComponentProperties 
   * @param formStatus 
   * @param effectiveDate 
   * @param comments 
   * @param contentForms 
   * @returns {*}  {Observable<FormEngineCreateGuidedFormsResponse>}
   * @memberof DynamicFormService
   */
  saveGuidedInterviewProperties(formNumber: string, formComponentProperties: string, formStatus: FormEngineFormStatus, effectiveDate: string, comments: string, contentForms: FormEngineAssociatedContentFormsPayload[]): Observable<FormEngineCreateGuidedFormsResponse> {
    return this.formEngineGuidedInterviewService.saveGuidedInterviewProperties({ formNumber, formComponentProperties, formStatus, effectiveDate, comments, contentForms });
  }

  /**
   * Makes a GET request to retrieve a dynamic form
   *
   * @param {number} formId
   * @return {*}  {Observable<FormEngineGetDynamicFormResponse>}
   * @memberof DynamicFormService
   */
  getGuidedInterviewForm(formNumber: string, status: EditInterviewFormStatus): Observable<FormEngineGetGuidedFormResponse> {
    return this.formEngineGuidedInterviewService.getGuidedInterviewForm(formNumber, status);
  }


  /**
   * Method to fetch all content forms for the given array of casetypes id
   */
  getCFMappingTypesForGuidedInterview(canSendMappingType: boolean, formNumber: string = null): Observable<FormEngineContentFormMappingTypesResponse> {
    return this.formEngineGuidedInterviewService.getCFMappingTypesForGuidedInterview(canSendMappingType, formNumber);
  }


  /************ END: GI Form Sevices  *****************/



}
