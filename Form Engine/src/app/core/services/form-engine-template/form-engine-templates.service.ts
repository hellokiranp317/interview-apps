import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormEngineFormAuditHistoryResponse, FormEngineGetTemplateByIdResponse, FormEngineGetTemplateResponse, FormEnginePostTemplateRequest, FormEngineTemplatesApiService, SuccessResponse } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineTemplatesService {

  private auditHistoryResponseSubject: BehaviorSubject<FormEngineFormAuditHistoryResponse>;
  public auditHistoryResponse: Observable<FormEngineFormAuditHistoryResponse>;

  constructor(private formEngineTemplateService: FormEngineTemplatesApiService) {
    this.auditHistoryResponseSubject = new BehaviorSubject<any>(null);
    this.auditHistoryResponse = this.auditHistoryResponseSubject.asObservable();
  }

  /**
   * Save the template name and form component properties
   *
   * @param {FormEnginePostTemplateRequest} payload
   * @return {*}  {Observable<SuccessResponse>}
   * @memberof FormEngineService
   */
  saveTemplate(templateName: string, templateComponentProperties: string = '{}') {
    return this.formEngineTemplateService.saveTemplates({templateName,templateComponentProperties})
  }

  /**
   * Get the template component properties
   *
   * @return {*}  {Observable<FormEngineGetTemplateResponse>}
   * @memberof FormEngineService
   */
  retriveTemplate(): Observable<FormEngineGetTemplateResponse> {
    return this.formEngineTemplateService.retrieveTemplates()
  }

  getTemplateById(templateId: string): Observable<FormEngineGetTemplateByIdResponse> {
    return this.formEngineTemplateService.getTemplateById(templateId);
  }


 /**
  * Update the template name and form component properties
  * @param templateId 
  * @param templateName 
  * @param templateComponentProperties 
  * @returns {Observable<SuccessResponse>}
  */
  updateTemplate(templateId: string, templateComponentProperties: string = ''): Observable<SuccessResponse> {
    return this.formEngineTemplateService.updateTemplates(templateId, {templateComponentProperties})
  }

}
