import { TestBed } from '@angular/core/testing';

import { FormEngineTemplatesService } from './form-engine-templates.service';

describe('FormEngineTemplatesService', () => {
  let service: FormEngineTemplatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineTemplatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
