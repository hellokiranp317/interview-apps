import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormEngineContentFormEditTagsRequest, FormEngineContentFormResponse, FormEngineContentFormSaveRequest, FormEngineContentFormTagsRequest, FormEngineContentInterviewApiService, FormEngineFormAuditHistoryResponse, SuccessResponse } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineContentInterviewService {

  private auditHistoryResponseSubject: BehaviorSubject<FormEngineFormAuditHistoryResponse>;
  public auditHistoryResponse: Observable<FormEngineFormAuditHistoryResponse>;

  constructor(private formEngineFormsService: FormEngineContentInterviewApiService) {
    this.auditHistoryResponseSubject = new BehaviorSubject<any>(null);
    this.auditHistoryResponse = this.auditHistoryResponseSubject.asObservable();
  }

  /************** START: Content Form Services **************/

  /**
   * Method to fetch all content forms for the given array of casetypes id
   * @param caseTypeIds 
   * @returns FormEngineContentFormResponse
   * @memberof FormEngineService
   */
  getContentForms(caseTypeIds: number[] = []): Observable<FormEngineContentFormResponse> {
    return this.formEngineFormsService.getContentForm(caseTypeIds);
  }

  /**
   * Method to save the content form tags
   * @param payload FormEngineContentFormTagsRequest
   * @returns SuccessResponse
   */
  saveContentFormTagInformation(payload: FormEngineContentFormTagsRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.saveContentFormTagInformation(payload);
  }

  /**
   * Method to edit the content form tags
   * @param payload FormEngineContentFormTagsRequest
   * @returns SuccessResponse
   */
  editContentFormTagInformation(payload: FormEngineContentFormEditTagsRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.editContentFormTagInformation(payload);
  }

  /**
   * Method to retrieve the content form tags
   * @param formNumber string
   */
  retrieveContentFormTagInformation(formNumber: string) {
    const caseTypes = true;
    const roles = true;
    const steps = true;
    const rules = true;
    const statutes = true;
    return this.formEngineFormsService.retrieveContentFormTagInformation(caseTypes, roles, steps, rules, statutes, formNumber);
  }

  /**
   * Method to save the content form properties
   * @param payload FormEngineContentFormSaveRequest
   * @returns SuccessResponse
   */
  saveContentForm(payload: FormEngineContentFormSaveRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.saveContentForm(payload);
  }

  /**
   * Method to save the edited content form properties
   * @param payload FormEngineContentFormSaveRequest
   * @returns SuccessResponse
   */
  editContentForm(payload: FormEngineContentFormSaveRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.editContentForm(payload);
  }

  /**
   * Method to retrieve the content form by formNumber
   * @param formNumber string
   * @returns SuccessResponse
   */
  retrieveContentFormByFormNumber(formNumber: string): Observable<SuccessResponse> {
    return this.formEngineFormsService.retrieveContentFormByFormNumber(formNumber);
  }

  /************** END: Content Form Services **************/

}
