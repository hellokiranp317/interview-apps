import { TestBed } from '@angular/core/testing';

import { FormEngineContentInterviewService } from './form-engine-content-interview.service';

describe('FormEngineContentInterviewService', () => {
  let service: FormEngineContentInterviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineContentInterviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
