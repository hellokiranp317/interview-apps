import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { FormEngineFormsApiService, FormEngineCaseTypesResponse, FormEngineCreateCaseTypeRequest, FormEngineFormsByCaseTypeResponse, FormEngineFormsResponse, FormEngineFormTypes, FormEngineGetFormByCaseTypeStatus, FormEngineGetFormStatus, FormEngineGetFormTypes, RequestedSystemEnum, SortOrderEnum, SuccessResponse, FormEngineDashboardApiService } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private formEngineFormsResponseSubject: BehaviorSubject<FormEngineFormsResponse>;
  public formEngineFormsResponse: Observable<FormEngineFormsResponse>;

  constructor(private formEngineDashboardApiService: FormEngineDashboardApiService) {
    this.formEngineFormsResponseSubject = new BehaviorSubject<any>(null);
    this.formEngineFormsResponse = this.formEngineFormsResponseSubject.asObservable();
  }

  /**
   * function to get forms details
   * @returns FormEngineFormsResponse
   * @memberof DashboardService
   */
  getForms(formStatus: FormEngineGetFormStatus[] = [], formTypes: FormEngineFormTypes[] = [], count: number = 0, sortOrder: SortOrderEnum = SortOrderEnum.Desc): Observable<FormEngineFormsResponse> {
    return this.formEngineDashboardApiService.getForms(formStatus, formTypes, count, sortOrder).pipe(
      tap((response: FormEngineFormsResponse) => {
        return response;
      }
      )
    )
  }

  getFormsByCaseType(caseTypeId: number,count: number= 0, 
    formStatus:FormEngineGetFormByCaseTypeStatus[] = [FormEngineGetFormByCaseTypeStatus.Published], 
    formTypes: FormEngineGetFormTypes[] = [FormEngineGetFormTypes.Form, FormEngineGetFormTypes.ContentForm], 
    sortOrder: SortOrderEnum = SortOrderEnum.Desc): Observable<FormEngineFormsByCaseTypeResponse> {
    return this.formEngineDashboardApiService.getFormsAssociatedWithCaseType(formStatus, formTypes, caseTypeId, count, sortOrder);
  }
}
