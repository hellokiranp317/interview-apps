import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ERROR } from '../../constants';
import { ToastrOptions } from '../../models/toastrOptions';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }
  
  showSuccess(message: string = '', options?: ToastrOptions){
      this.toastr.success(message, options?.title)
  }
  
  showError(message: string = '', options?: ToastrOptions){
    if(options?.code !== ERROR.CODE.EXPIRED_JWT)
      this.toastr.error(message, options?.title)
  }
  
  showInfo(message: string = '', options?: ToastrOptions){
      this.toastr.info(message, options?.title)
  }
  
  showWarning(message: string = '', options?: ToastrOptions){
      this.toastr.warning(message, options?.title)
  }
}
