import { TestBed } from '@angular/core/testing';

import { FormEngineFormsService } from './form-engine-forms.service';

describe('FormEngineFormsService', () => {
  let service: FormEngineFormsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineFormsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
