import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {  FormEngineContentFormEditTagsRequest, FormEngineContentFormResponse, FormEngineContentFormSaveRequest, FormEngineContentFormTagsRequest, FormEngineCreateFormTagsRequest, FormEngineCreateGuidedFormsResponse, FormEngineEditFormTagsRequest, FormEngineFormAuditHistoryResponse, FormEngineFormComponentsResponse, FormEngineFormOperation, FormEngineFormsApiService, FormEngineFormTemplateResponse, FormEngineGetTemplateResponse, FormEnginePostTemplateRequest, SuccessResponse } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineFormsService {

  private auditHistoryResponseSubject: BehaviorSubject<FormEngineFormAuditHistoryResponse>;
  public auditHistoryResponse: Observable<FormEngineFormAuditHistoryResponse>;

  private builderOptionsSubject: BehaviorSubject<any>;
  public builderOptions: Observable<any>;

  private formWizardSubject: BehaviorSubject<any>;
  public formWizard: Observable<any>;

  constructor(private formEngineFormsService: FormEngineFormsApiService) {

    this.builderOptionsSubject = new BehaviorSubject<any>(null);
    this.builderOptions = this.builderOptionsSubject.asObservable();

    this.formWizardSubject = new BehaviorSubject<any>(null);
    this.formWizard = this.formWizardSubject.asObservable();

    this.auditHistoryResponseSubject = new BehaviorSubject<any>(null);
    this.auditHistoryResponse = this.auditHistoryResponseSubject.asObservable();
  }


  /**
   * Getter for builderOptions
   *
   * @readonly
   * @type {*}
   * @memberof DynamicFormService
   */
  get getBuilderOptions(): any { return this.builderOptionsSubject.getValue() }

  /**
   * Setter for builderOptions
   *
   * @param {*} builderOptions
   * @memberof DynamicFormService
   */
  setBuilderOptions(builderOptions: any) { this.builderOptionsSubject.next(builderOptions); }

  /**
   * Getter for FormWizard
   *
   * @readonly
   * @type {*}
   * @memberof DynamicFormService
   */
   get getFormWizard(): any { return this.formWizardSubject.getValue() }

  /**
  * Setter for FormWizard
  *
  * @param {*} formWizard
  * @memberof DynamicFormService
  */
  setFormWizard(formWizard: any) { this.formWizardSubject.next(formWizard); }

  /**
   * Makes a GET request to get the form components
   * 
   * @param formId number
   * @returns FormEngineFormComponentsResponse
   * @memberof JaqDashboardServiceService
   */
  getFormComponents(formId: number): Observable<FormEngineFormComponentsResponse> {
    return this.formEngineFormsService.getFormComponents(formId);
  }

  // /** Feature - can be refactored in safe as draft flow
  //  * Makes a POST request to create a dynamic form
  //  *
  //  * @param {string} formComponentProperties
  //  * @param {number} formId
  //  * @return {*}  {Observable<SuccessResponse>}
  //  * @memberof DynamicFormService
  //  */
  // createGuidedInterviewForm(formComponentProperties: string, formNumber: string, formStatus: FormEngineFormStatus, effectiveDate: string, comments: string): Observable<SuccessResponse> {
  //   return this.formEngineFormsService.postGuidedInterviewForms({ formComponentProperties, formNumber, formStatus, effectiveDate, comments });
  // }


  /**
   * Makes a GET request to get the auto-generated form template as a FormIO Widget
   *
   * @param {number} formId
   * @return {*}  {Observable<FormEngineFormTemplateResponse>}
   * @memberof DynamicFormService
   */
  getFormTemplate(formId: number): Observable<FormEngineFormTemplateResponse> {
    return this.formEngineFormsService.getFormTemplate(formId);
  }
  

  /**
   * Getter for AuditHistoryResponseSubject
   *
   * @readonly
   * @memberof FormEngineService
   */
  get getFormAuditHistoryResponse(): FormEngineFormAuditHistoryResponse { return this.auditHistoryResponseSubject.value }

  /**
   * Function to get audit history details
   * @returns FormEngineFormAuditHistoryResponse
   * @memberof FormEngineService
   */
  getFormAuditHistory(formNumber: string): Observable<FormEngineFormAuditHistoryResponse> {
    return this.formEngineFormsService.getFormAuditHistory(formNumber).pipe(
      tap((response: FormEngineFormAuditHistoryResponse) => {
        this.auditHistoryResponseSubject.next(response);
        return response;
      }
      )
    );
  }

  // Feature - Removing duplicate form as of now
  /**
   * Method to duplicate form
   *
   * @param {FormEngineDuplicateFormRequest} payload
   * @return {*}  {Observable<SuccessResponse>}
   * @memberof FormEngineService
   */
  // duplicateForm(payload: FormEngineDuplicateFormRequest): Observable<SuccessResponse> {
  //   return this.formEngineFormsService.duplicateForm(payload);
  // }

  getAllFormTags(formNumber: string) {
    const caseTypes = true;
    const roles = true;
    const steps = true;
    const rules = true;
    const statutes = true;
    return this.formEngineFormsService.getFormTagsInformation(caseTypes, roles, steps, rules, statutes, formNumber)
  }

  /**
   * Stores the form tags information in the database
   *
   * @param {FormEngineCreateFormTagsRequest} payload
   * @return {*}  {Observable<SuccessResponse>}
   * @memberof FormEngineService
   */
  storeFormTagsInformation(payload: FormEngineCreateFormTagsRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.storeFormTagsInformation(payload);
  }

  /**
   * Stores the form tags information in the database
   *
   * @param {FormEngineEditFormTagsRequest} payload
   * @return {*}  {Observable<SuccessResponse>}
   * @memberof FormEngineService
   */
  editTagsInformation(payload: FormEngineEditFormTagsRequest): Observable<SuccessResponse> {
    return this.formEngineFormsService.editTagsInformation(payload);
  }
}
