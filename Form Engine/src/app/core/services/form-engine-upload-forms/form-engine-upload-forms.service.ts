import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { FormEngineContentFormEditTagsRequest, FormEngineContentFormResponse, FormEngineContentFormSaveRequest, FormEngineContentFormTagsRequest, FormEngineCreateFormTagsRequest, FormEngineCreateGuidedFormsResponse, FormEngineEditFormTagsRequest, FormEngineFormAuditHistoryResponse, FormEngineFormOperation, FormEngineFormsApiService, FormEngineGetTemplateResponse, FormEnginePostTemplateRequest, FormEngineUploadFormsApiService, SuccessResponse } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineUploadFormsService {

  private auditHistoryResponseSubject: BehaviorSubject<FormEngineFormAuditHistoryResponse>;
  public auditHistoryResponse: Observable<FormEngineFormAuditHistoryResponse>;

  constructor(private formEngineUploadFormsService: FormEngineUploadFormsApiService) {
    this.auditHistoryResponseSubject = new BehaviorSubject<any>(null);
    this.auditHistoryResponse = this.auditHistoryResponseSubject.asObservable();
  }
  /**
   * Invokes the upload function based on form Operation
   * @param pdfFile 
   * @param formId 
   * @param formName 
   * @param formNumber 
   * @param description 
   * @param formOperation 
   * @returns {Observable<SuccessResponse>}
   */
  uploadBasedOnFormOperation(pdfFile: string = '', formId: number, formName: string, formNumber: string, description: string, formOperation = FormEngineFormOperation.UploadForm) : Observable<SuccessResponse>{
    if(formOperation === FormEngineFormOperation.UploadForm || formOperation === FormEngineFormOperation.CreateContentForm)
        return this.uploadForms(pdfFile, formName, formNumber, description, formOperation);
    else if(formOperation === FormEngineFormOperation.EditForm || formOperation === FormEngineFormOperation.EditContentForm)
        return this.updateUploadForms(pdfFile, formId, formName, formNumber, description, formOperation);
  }
  

  /**
   * Method to upload New form
   * @param formNumber 
   * @param formName 
   * @param form 
   * @returns {Observable<SuccessResponse>}
   */
  uploadForms(pdfFile: string = '', formName: string, formNumber: string, description: string, formOperation = FormEngineFormOperation.UploadForm): Observable<SuccessResponse> {
    return this.formEngineUploadFormsService.uploadForms({ pdfFile, formNumber, formName, description, formOperation });
  }

  /**
   * Method to edit form
   * @param formNumber 
   * @param formName 
   * @param form 
   * @param formId
   * @returns {Observable<SuccessResponse>}
   */
  updateUploadForms(pdfFile: string = '', formId: number, formName: string, formNumber: string, description: string, formOperation = FormEngineFormOperation.EditForm): Observable<SuccessResponse> {
    return this.formEngineUploadFormsService.updateUploadForms({ formId, pdfFile, formName, formNumber, description, formOperation });
  }

}
