import { TestBed } from '@angular/core/testing';

import { FormEngineUploadFormsService } from './form-engine-upload-forms.service';

describe('FormEngineUploadFormsService', () => {
  let service: FormEngineUploadFormsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineUploadFormsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
