import { TestBed } from '@angular/core/testing';

import { FormEngineCaseTypeService } from './form-engine-case-type.service';

describe('FormEngineCaseTypeService', () => {
  let service: FormEngineCaseTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormEngineCaseTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
