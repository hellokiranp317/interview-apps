import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { FormEngineCaseTypesApiService, FormEngineCaseTypesResponse, FormEngineCreateCaseTypeRequest, RequestedSystemEnum, SuccessResponse } from 'src/app/client';

@Injectable({
  providedIn: 'root'
})
export class FormEngineCaseTypeService {

  private caseTypesResponseSubject: BehaviorSubject<FormEngineCaseTypesResponse>;
  public caseTypesResponse: Observable<FormEngineCaseTypesResponse>;

  constructor(private formEngineCaseTypesApiService: FormEngineCaseTypesApiService) {
    this.caseTypesResponseSubject = new BehaviorSubject<any>(null);
    this.caseTypesResponse = this.caseTypesResponseSubject.asObservable();
  }

  /**
   * Getter for caseTypesResponseSubject
   *
   * @readonly
   * @memberof DashboardService
   */
  get getCaseTypesResponse(): FormEngineCaseTypesResponse { return this.caseTypesResponseSubject.value }

  /**
    * Method to get all case types
    * @returns FormEngineCaseTypesResponse
    * @memberof DashboardService
    */
  getCaseTypes(system: RequestedSystemEnum): Observable<FormEngineCaseTypesResponse> {
    return this.formEngineCaseTypesApiService.getCaseTypes(system).pipe(
      tap((response: FormEngineCaseTypesResponse) => {
        this.caseTypesResponseSubject.next(response);
        return response;
      }
      )
    );
  }

  /**
   * Method to create a new case type
   *
   * @param {string} caseType
   * @return {*}  {Observable<SuccessResponse>}
   * @memberof DynamicFormService
   */
  createCaseType(caseType: FormEngineCreateCaseTypeRequest): Observable<SuccessResponse> {
    return this.formEngineCaseTypesApiService.createCaseType(caseType);
  }

}
