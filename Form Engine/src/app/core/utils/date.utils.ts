/**
 * Returns the difference between two dates in days
 *
 * @return {Number}
 */
const calculateDateDiff = (date1, date2) => {
    let dateObj1 = new Date(date1);
    let dateObj2 = new Date(date2);
    return Math.floor((Date.UTC(dateObj2.getFullYear(), dateObj2.getMonth(), dateObj2.getDate()) - Date.UTC(dateObj1.getFullYear(), dateObj1.getMonth(), dateObj1.getDate()) ) /(1000 * 60 * 60 * 24));
}

/**
 * Function formatDate to format Date string as yyyy-mm-dd
 * @param date 
 * @returns formatted date string
 */
const formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

export { calculateDateDiff, formatDate };