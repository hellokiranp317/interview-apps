import { environment } from 'src/environments/environment';
import { COMMON, SECURITY, SKIP_INTERCEPTORS } from 'src/app/core/constants';
import { CurrentUser } from 'src/app/core/models';
import { Configuration } from 'src/app/client';

/**
 * Returns a API Configuration
 *
 * @return {*} 
 */
export const getAPIConfiguration = (): Configuration => {
    const currentUser: CurrentUser = JSON.parse(
      localStorage.getItem(COMMON.CURRENT_USER) ? localStorage.getItem(COMMON.CURRENT_USER) : null
    )
    const authorizationToken = currentUser ? `${SECURITY.BEARER}${currentUser.accessToken}` : null;
    return new Configuration({
      basePath: environment.apiUrl,
      credentials: {
        Bearer: authorizationToken
      }
    })
}

/**
 * Fetches the current user from local storage
 *
 * @return {*}  {CurrentUser}
 */
export const getCurrentUserFromLocalStorage = (): CurrentUser => {
  const stringifiedCurrentUserData: string = getLocalStorageData(COMMON.CURRENT_USER);
  return parseJSONData(stringifiedCurrentUserData);
}

/**
 * Getter for localStorage data
 * 
 * @param key 
 * @returns {string}
 */
export const getLocalStorageData = (key: string): string => { 
  return localStorage.getItem(key);
}

/**
 * Setter for localStorage data
 * 
 * @param key
 * @param value 
 * @returns {void}
 */
export const setLocalStorageData = (key: string, value: string): void => {
  return localStorage.setItem(key, value);
}

/**
 * Removes the data from localStorage
 * 
 * @param key 
 * @returns {void}
 */
export const removeLocalStorageData = (key: string): void => {
  return localStorage.removeItem(key);
}

/**
 * Parses JSON Data
 *
 * @param {string} data
 * @return {*}  {*}
 */
export const parseJSONData = (data: string): any => {
  try {
    return JSON.parse(data);
  } catch(e){
    return null;
  }
}

/**
 * Return whether the url can be skipped by interceptor
 *
 * @param {string} urlToBeSkipped
 * @return {*} 
 */
 export const canSkipInterceptor = (urlToBeSkipped: string): boolean => {
  let canSkip: boolean = false;
  SKIP_INTERCEPTORS.forEach((eachUrl) => {
    if(urlToBeSkipped.includes(eachUrl)) { canSkip = true; return; }
  })
  return canSkip;
}