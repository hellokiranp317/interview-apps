import { TemplateRef } from "@angular/core";

export interface Columns {
    prop?: string; 
    name?: string; 
    cellTemplate?: TemplateRef<any>; 
    sortable?: boolean; 
    checkboxable?: boolean; 
    cellClass?: string; 
    canAutoResize?: boolean; 
    resizeable?: boolean; 
    headerCheckboxable?: boolean; 
    headerTemplate?: TemplateRef<any>; 
    placeholder?: string; 
    dropdown?: Set<string>; 
    width?: number;
    value?: string;
    ariaLabel?: string;
}