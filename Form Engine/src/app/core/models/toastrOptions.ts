export interface ToastrOptions {
    title?: string;
    code?: string;
}