export interface FilterData {
    formName?: string;
    caseType?: string;
    status?: string;
    statusDate?: string;
    roles?: string;
    rules?: string
}