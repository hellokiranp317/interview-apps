export interface CurrentUser { 
    accessToken?: string,
    refreshToken?: string,
    username?: string,
    hasAccessTo?: string
}