import { FormEngineForm } from "src/app/client";

export interface TableModalEmitter {
    modalType: string, 
    formData: FormEngineForm
}