import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AUTHORIZED_USERS, FORMS, FRONT_END_ROUTES_PATH } from './core/constants';
import { AuthGuard } from './core/helpers';
import { FormsComponent, FormsDashboardComponent, FormsEngineDashboardComponent, LoginComponent } from './pages';
import { ErrorComponent } from './shared/pages/error/error.component';

const routes: Routes = [
  { path: '', redirectTo: FRONT_END_ROUTES_PATH.LOGIN, pathMatch: 'full' },
  { path: FRONT_END_ROUTES_PATH.LOGIN, component: LoginComponent },
  {
    path: FRONT_END_ROUTES_PATH.FORMS_DASHBOARD,
    component: FormsEngineDashboardComponent,
    data: { allowedRoles: AUTHORIZED_USERS.DASHBOARD },
    canActivate: [AuthGuard]
  },
  {
    path: FRONT_END_ROUTES_PATH.FORMS_LIST_DASHBOARD,
    component: FormsDashboardComponent,
    data: { allowedRoles: AUTHORIZED_USERS.FORMS },
    canActivate: [AuthGuard]
  },
  {
    path: FRONT_END_ROUTES_PATH.FORMS+'/:'+FORMS.TYPE+'/:'+FORMS.ID,
    component: FormsComponent,
    data: { allowedRoles: AUTHORIZED_USERS.FORMS },
    canActivate: [AuthGuard]
  },
  {
    path: FRONT_END_ROUTES_PATH.ERROR,
    component: ErrorComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy', useHash: true, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
